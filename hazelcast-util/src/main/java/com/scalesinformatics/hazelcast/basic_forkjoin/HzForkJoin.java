/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.hazelcast.basic_forkjoin;


import com.hazelcast.core.IExecutorService;
import com.hazelcast.monitor.LocalExecutorStats;
import com.scalesinformatics.util.concurrent.ThreadUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.logging.Logger;

/**
 * @author Keith Flanagan
 */
public class HzForkJoin<T> {
  private static final Logger logger = Logger.getLogger(HzForkJoin.class.getName());

  private final IExecutorService executor;
  private final List<Future<T>> futures;

  public HzForkJoin(IExecutorService executor) {
    this.executor = executor;
    this.futures = new ArrayList<>(64);
  }

  public void submit(Callable<T> job) {
    futures.add(executor.submit(job));
  }

  public void submit(List<Callable<T>> jobs) {
    for (Callable<T> job : jobs) {
      futures.add(executor.submit(job));
    }
  }

  public void waitUntilComplete() {
    waitUntilComplete(-1);
  }

  public boolean waitUntilComplete(long timeoutMs) {
    final long timeoutAt = timeoutMs > 0 ? System.currentTimeMillis() + timeoutMs : -1;
    boolean completed = false;
    long nextQueueStatsPrintAt = 0;
    do {
      try {
        for (Future<T> future : futures) {
          completed = future.isDone();
          if (!completed) {
            break;
          }
        }

        if (completed || (timeoutAt > 0 && System.currentTimeMillis() > timeoutAt)) {
          break;
        }
        ThreadUtils.sleep(50);

        if (nextQueueStatsPrintAt < System.currentTimeMillis()) {
          LocalExecutorStats stats = executor.getLocalExecutorStats();
          logger.info("Qstats: "
              + "started: " + stats.getStartedTaskCount()
              + ", pending: " + stats.getPendingTaskCount()
              + ", completed: " + stats.getCompletedTaskCount()
              + ", cancelled: " + stats.getCancelledTaskCount()
              + ", inferred running: " + (stats.getStartedTaskCount() - stats.getCancelledTaskCount() - stats.getCompletedTaskCount())
          );
          nextQueueStatsPrintAt = System.currentTimeMillis() + 5000;
        }
      } catch (Exception e) {
        logger.info("Error occurred while checking task stat, but will continue checking. " +
            "Error was: "+e.getMessage());
      }
    } while (!completed);
    return completed;
  }


  public List<Future<T>> getFutures() {
    return futures;
  }

  public IExecutorService getExecutor() {
    return executor;
  }
}
