/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.hazelcast.compute;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.scalesinformatics.util.concurrent.ThreadUtils;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.logging.Logger;

/**
 * @author Keith Flanagan
 */
public class QueueManager { //implements Executor {
  private static final Logger logger = Logger.getLogger(QueueManager.class.getName());

  private final HazelcastInstance hazelcastInstance;
  private final QueueData q;

  public QueueManager(HazelcastInstance hazelcastInstance, String queueName) {
    this.hazelcastInstance = hazelcastInstance;
    this.q = new QueueData(hazelcastInstance, queueName);
  }


  public void execute(Task command) {
//    if (!(command instanceof Task)) {
//      throw new IllegalArgumentException(
//          "This Executor implementation only supports the submission of tasks of type: "+Task.class.getName()+".");
//    }
    while (true) {
      try {
        q.getJobQueue().put(command); // put() waits if necessary for space in the queue to become available.
        break;
      } catch (InterruptedException e) {
        logger.info("Interrupted whil attempting to queue a task. Will retry the operation.");
      }
    }
  }

  public Set<Task> clearQueue() {
    Set<Task> unprocessed = new HashSet<>();
    q.getJobQueue().drainTo(unprocessed);
    return unprocessed;
  }

  public int queueSize() {
    return q.getJobQueue().size();
  }

  public int queueRemainingCapacity() {
    return q.getJobQueue().remainingCapacity();
  }

  public int runningSize() {
    return q.getRunningTasks().size();
  }

  public int countQueuedTasksInCategory(String category) {
    /*
     * As per the documentation, the queue iterator is weakly-consistent.
     * This means that the iterator will return the items as they existed at the start of iteration, but does not
     * reflect changes to the queue after this time.
     *
     * http://docs.hazelcast.org/docs/3.3/javadoc/com/hazelcast/core/IQueue.html#iterator%28%29
     */
    int count = 0;
    for (Task t : q.getJobQueue()) {
      if (t.getTaskCategory().equals(category)) {
        count++;
      }
    }
    return count;
  }

  public int countRunningTasksInCategory(String category) {
    int count = 0;
    for (ExecutionResult v : q.getRunningTasks().values()) {
      if (v.getTask().getTaskCategory().equals(category)) {
        count++;
      }
    }
    return count;
  }

  public void waitUntilAllJobsComplete() {
    waitUntilAllJobsComplete(-1);
  }

  public boolean waitUntilAllJobsComplete(long timeoutMs) {
    return waitUntilComplete(timeoutMs,
        () -> q.getJobQueue().isEmpty() && q.getRunningTasks().isEmpty());
  }

  public void waitUntilCategoryComplete(String category) {
    waitUntilCategoryComplete(category, -1);
  }

  public boolean waitUntilCategoryComplete(String category, long timeoutMs) {
    return waitUntilComplete(timeoutMs,
        () -> countQueuedTasksInCategory(category) == 0
            && countRunningTasksInCategory(category) == 0);
  }

  private boolean waitUntilComplete(long timeoutMs, Callable<Boolean> completenessFunction) {
    final long timeoutAt = timeoutMs > 0 ? System.currentTimeMillis() + timeoutMs : -1;
    boolean completed = false;
    long nextQueueStatsPrintAt = 0;
    do {
      try {
        completed = completenessFunction.call();

        if (completed || (timeoutAt > 0 && System.currentTimeMillis() > timeoutAt)) {
          break;
        }
        ThreadUtils.sleep(50);

        if (nextQueueStatsPrintAt < System.currentTimeMillis()) {
          logger.info("Qstats: "
              + "pending: " + queueSize()
              + ", running: " + q.getRunningTasks().size()
              + ", completed (cached): " + q.getTaskResults().size()
          );
          nextQueueStatsPrintAt = System.currentTimeMillis() + 5000;
        }
      } catch (Exception e) {
        logger.info("Error occurred while checking task stat, but will continue checking. " +
            "Error was: "+e.getMessage());
      }
    } while (!completed);
    return completed;
  }

  /**
   * Given a specified <code>category</code>, returns all results that are currently available.
   *
   * @param category the category of results to return
   * @param removeReturned if true, the results will be returned and then deleted from Hazelcast.
   * @return
   */
  public Map<UUID, ExecutionResult> findResultsForCategory(String category, boolean removeReturned) {
    Map<UUID, ExecutionResult> copy = new HashMap<>();
    q.getTaskResults().entrySet().stream()
        .filter(entry -> entry.getValue().getTask().getTaskCategory().equals(category))
        .forEach(entry -> copy.put(entry.getKey(), entry.getValue()));

    if (removeReturned) {
      copy.keySet().parallelStream().forEach(uuid -> q.getTaskResults().remove(uuid));
    }
    return copy;
  }

}
