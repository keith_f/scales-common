/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.hazelcast.compute;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.HazelcastInstanceAware;
import com.hazelcast.core.ILock;
import com.scalesinformatics.util.concurrent.ThreadUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * A Hazelcast-aware Runnable that can be instructed to execute on one or more cluster members.
 * This task may be extremely long-lived. It continually pulls jobs out of a specified job queue and processes
 * them until no more work exists.
 *
 * This approach allows greater control over which task queue executes on which cluster machine(s), than the
 * Hazelcast ExecutorService implementation allows on its own. For example, you can start different instances
 * of Worker (configured in different ways) on different sub-groups of machines within your Hazelcast cluster.
 *
 * Coupled with the scheduler (TODO!), you can also ensure that some machines in the cluster *don't* execute jobs.
 *
 * @author Keith Flanagan
 */
public class QueueEater implements Runnable, Serializable, HazelcastInstanceAware {
  private static final Logger logger = Logger.getLogger(QueueEater.class.getName());
  private static final long DEFAULT_EXE_EXPIRES_AFTER = 1000L * 60 * 2;

  private transient HazelcastInstance hazelcastInstance;
  private transient ScheduledThreadPoolExecutor heartbeatExe = new ScheduledThreadPoolExecutor(1);
  private String queueName;

  private boolean quit;
  private boolean terminated;

  private long taskExpiresAfter = DEFAULT_EXE_EXPIRES_AFTER;
  private long runHeartbeatEvery = taskExpiresAfter / 2;

  private class Heartbeat implements Runnable {
    private final QueueData q;
    private final UUID taskUid;

    public Heartbeat(QueueData q, UUID taskUid) {
      this.q = q;
      this.taskUid = taskUid;
    }

    @Override
    public void run() {
      ILock lock = hazelcastInstance.getLock(taskUid.toString());
      lock.lock();
      //logger.info("Locked: "+lock.getName());
      try {
        ExecutionResult result = q.getRunningTasks().get(taskUid);
        if (result == null) {
          return;
        }
        result.setProcessingExpiresAt(new Date(System.currentTimeMillis()+taskExpiresAfter));
        q.getRunningTasks().put(taskUid, result);
      } finally {
        lock.unlock();
        //logger.info("Unlocked: "+lock.getName());
      }
    }
  }

  public QueueEater() {
  }

  public QueueEater(HazelcastInstance hazelcastInstance, String queueName) {
    this.hazelcastInstance = hazelcastInstance;
    this.queueName = queueName;
  }

  @Override
  public void setHazelcastInstance(HazelcastInstance hazelcastInstance) {
    this.hazelcastInstance = hazelcastInstance;
  }

  @Override
  public void run() {
    QueueData q = new QueueData(hazelcastInstance, queueName);

    String hzHostId = hazelcastInstance.getCluster().getLocalMember().getUuid();
    String hostName = hazelcastInstance.getCluster().getLocalMember().getAddress().getHost();

    logger.info(String.format("Thread%s: Starting a QueueEater on %s / %s",
        Thread.currentThread().getId(), hzHostId, hostName));

    while (!quit)  {
      Task task;
      try {
        task = q.getJobQueue().poll(1000, TimeUnit.MILLISECONDS);
        if (task == null) {
          continue;
        }
      } catch (InterruptedException e) {
        continue;
      }

      logger.info(String.format("Thread%s: Starting to process next work item %s of type %s from queue %s",
          Thread.currentThread().getId(), task.getUid(), task.getClass().getName(), queueName));

      ExecutionResult result = new ExecutionResult();
      result.setExecutionUid(UUID.randomUUID());
      result.setTaskUid(task.getUid());
      result.setTask(task);

      result.setExecutedByHostUid(hzHostId);
      result.setExecutedByHostName(hostName);
      result.setProcessingStarted(new Date());
      result.setState(TaskState.RUNNING);

      q.getRunningTasks().put(task.getUid(), result);

      // Start the heartbeat monitor
      ScheduledFuture heartbeatFuture = heartbeatExe.scheduleAtFixedRate(
          new Heartbeat(q, task.getUid()), 0, runHeartbeatEvery, TimeUnit.MILLISECONDS);

      // Run the task
      Object taskOutput = null;
      Throwable error = null;
      try {
        taskOutput = task.call();
      } catch (Throwable e) {
        error = e;
      } finally {
        heartbeatFuture.cancel(false);
      }

      // Now update result object - remove from 'running tasks', and add to 'completed'
      ILock lock = hazelcastInstance.getLock(task.getUid().toString());
      lock.lock();
      //logger.info("Locked: "+lock.getName());
      try {
        // Re-get the object since it may have been updated by another process
        result = q.getRunningTasks().get(task.getUid());
        result.setResult(taskOutput);
        result.setProcessingCompleted(new Date());
        if (error == null) {
          result.setState(TaskState.RUN_SUCCESSFUL);
        } else {
          result.setState(TaskState.RUN_FAILED);
          result.setError(error);
        }
        // Mark this task as complete, and upload the result
        q.getTaskResults().put(task.getUid(), result);
        // Mark this task as no longer running
        q.getRunningTasks().remove(task.getUid());
      } finally {
        lock.unlock();
        //logger.info("Unlocked: "+lock.getName());
      }
    }
    terminated = true;
  }

  public void waitForTermination() {
    waitForTermination(-1);
  }

  public boolean waitForTermination(long timeoutMs) {
    final long timeoutAt = timeoutMs > 0 ? System.currentTimeMillis() + timeoutMs : -1;
    while(!terminated) {
      if (timeoutAt > 0 && System.currentTimeMillis() > timeoutAt) {
        break;
      }
      ThreadUtils.sleep(50);
    }
    return terminated;
  }

  public boolean isQuit() {
    return quit;
  }

  public void setQuit(boolean quit) {
    this.quit = quit;
  }

  public String getQueueName() {
    return queueName;
  }

  public void setQueueName(String queueName) {
    this.queueName = queueName;
  }
}
