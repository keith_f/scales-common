/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.hazelcast.compute;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

import java.util.UUID;
import java.util.concurrent.BlockingQueue;

/**
 * @author Keith Flanagan
 */
public class QueueData {
  private final String queueName;
  private final BlockingQueue<Task> jobQueue;
  // Map of job --> hazelcast member
  private final IMap<UUID, ExecutionResult> runningTasks;
  // Map of job --> result
  private final IMap<UUID, ExecutionResult> taskResults;

  public QueueData(HazelcastInstance hazelcastInstance, String queueName) {
    this.queueName = queueName;
    this.jobQueue = hazelcastInstance.getQueue(queueName);
    runningTasks = hazelcastInstance.getMap(queueName+".running");
    taskResults = hazelcastInstance.getMap(queueName+".results");
  }

  public String getQueueName() {
    return queueName;
  }

  public BlockingQueue<Task> getJobQueue() {
    return jobQueue;
  }

  public IMap<UUID, ExecutionResult> getRunningTasks() {
    return runningTasks;
  }

  public IMap<UUID, ExecutionResult> getTaskResults() {
    return taskResults;
  }
}
