package com.scalesinformatics.hazelcast.compute;

import com.hazelcast.config.Config;
import com.hazelcast.core.HazelcastInstance;
import com.scalesinformatics.hazelcast.HazelcastConfigFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.logging.Logger;

/**
 * A simple commandline Hazelcast node. You can use this program to donate
 * the host computer's RAM resources to a Hazelcast cluster. Simply start this
 * program with a Hazelcast configuration file containing the details for the
 * network that it should join.
 */
public class HazelcastComputeNode {
  private static final Logger logger = Logger.getLogger(HazelcastComputeNode.class.getName());

  private final HazelcastInstance hz;
  private final String queueName;
  private int maxThreads;
  private List<QueueEater> queueEaters;

  private boolean started;

  public HazelcastComputeNode(HazelcastInstance hz, String queueName, int maxThreads) {
    this.hz = hz;
    this.queueName = queueName;
    this.maxThreads = maxThreads;
    this.queueEaters = new ArrayList<>(maxThreads);
  }

  public synchronized void start() {
    if (started) {
      logger.info("Warning: this compute node is already running!");
      return;
    }
    logger.info(String.format("Going to start %s threads for queue %s", maxThreads, queueName));
    for (int i=0; i<maxThreads; i++) {
      logger.info(String.format("Starting thread %s", i));
      QueueEater eater = new QueueEater(hz, queueName);
      Thread t = new Thread(eater);
      t.start();
      queueEaters.add(eater);
    }
    logger.info(String.format("Total queue eater instances: %s", queueEaters.size()));
    started = true;
  }

  public synchronized void stop() {
    if (!started) {
      logger.info("Warning: this compute node is already stopped!");
      return;
    }
    // Send a shutdown signal to each of the queue processors
    for (QueueEater eater : queueEaters) {
      eater.setQuit(true);
    }

    // Wait for the queue processors to finish executing any current jobs
    for (QueueEater eater : queueEaters) {
      eater.waitForTermination();
    }
    started = false;
  }



  public static void main(String[] args) {
    if (args.length == 0) {
      System.out.println("Usage: \n"+
          "  * Cluster name to join\n" +
          "  * Cluster password\n" +
          "  * Job queue to process\n"
      );
    }
    String clusterName = args[0];
    String clusterPassword = args[1];
    String queueName = args[2];

    Config config = HazelcastConfigFactory.makeDefaultConfig(clusterName, clusterPassword);
    HazelcastInstance hz = HazelcastConfigFactory.makeInstance(config);

    int cores = Runtime.getRuntime().availableProcessors();

    HazelcastComputeNode computeNode = new HazelcastComputeNode(hz, queueName, cores);
    computeNode.start();

    System.out.println("\n\n\n\n");
    System.out.println("***************************************");
    System.out.println("Compute server started. CTRL-C to exit.");
    System.out.println("***************************************");
    System.out.println("\n\n\n\n");
  }
}
