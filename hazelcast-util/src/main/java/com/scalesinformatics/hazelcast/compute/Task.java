/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.hazelcast.compute;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.Callable;

/**
 * @author Keith Flanagan
 */
public interface Task<V> extends Callable<V>, Serializable {
  UUID getUid();
  void setUid(UUID uid);

  String getTaskCategory();
  void setTaskCategory(String taskCategory);

  String getDescription();
  void setDescription(String description);

  int getRequiredCores();
  void setRequiredCores(int requiredCores);

  Date getSubmittedAt();
  void setSubmittedAt(Date date);

  double estimateCompleteness();
}
