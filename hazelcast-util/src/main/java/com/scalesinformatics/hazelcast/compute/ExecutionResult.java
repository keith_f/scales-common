/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.hazelcast.compute;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public class ExecutionResult<V> implements Serializable {
  private UUID executionUid;
  private UUID taskUid;

  private TaskState state;
  private Throwable error;

//  private Date submitted;
  private Date processingStarted;
  private Date processingCompleted;
  private Date processingExpiresAt; // The point at which the system assumes a crash and re-queues the job

  private String executedByHostUid;
  private String executedByHostName;

  private Task<V> task;
  private V result;

  public TaskState getState() {
    return state;
  }

  public void setState(TaskState state) {
    this.state = state;
  }

  public Date getProcessingStarted() {
    return processingStarted;
  }

  public void setProcessingStarted(Date processingStarted) {
    this.processingStarted = processingStarted;
  }

  public Date getProcessingCompleted() {
    return processingCompleted;
  }

  public void setProcessingCompleted(Date processingCompleted) {
    this.processingCompleted = processingCompleted;
  }

  public V getResult() {
    return result;
  }

  public void setResult(V result) {
    this.result = result;
  }

  public String getExecutedByHostUid() {
    return executedByHostUid;
  }

  public void setExecutedByHostUid(String executedByHostUid) {
    this.executedByHostUid = executedByHostUid;
  }

  public String getExecutedByHostName() {
    return executedByHostName;
  }

  public void setExecutedByHostName(String executedByHostName) {
    this.executedByHostName = executedByHostName;
  }

  public Throwable getError() {
    return error;
  }

  public void setError(Throwable error) {
    this.error = error;
  }

  public UUID getExecutionUid() {
    return executionUid;
  }

  public void setExecutionUid(UUID executionUid) {
    this.executionUid = executionUid;
  }

  public UUID getTaskUid() {
    return taskUid;
  }

  public void setTaskUid(UUID taskUid) {
    this.taskUid = taskUid;
  }

  public Task<V> getTask() {
    return task;
  }

  public void setTask(Task<V> task) {
    this.task = task;
  }

  public Date getProcessingExpiresAt() {
    return processingExpiresAt;
  }

  public void setProcessingExpiresAt(Date processingExpiresAt) {
    this.processingExpiresAt = processingExpiresAt;
  }
}
