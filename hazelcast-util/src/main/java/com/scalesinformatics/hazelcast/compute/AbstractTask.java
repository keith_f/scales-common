/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.hazelcast.compute;

import java.util.Date;
import java.util.UUID;

/**
 * @author Keith Flanagan
 */
abstract public class AbstractTask<V> implements Task<V> {
  protected UUID uid;
  protected String taskCategory;
  protected String description;
  protected int requiredCores = 1;
  protected Date submittedAt;

  public AbstractTask() {
    uid = UUID.randomUUID();
    submittedAt = new Date();
  }

  @Override
  public double estimateCompleteness() {
    return -1;
  }

  @Override
  public UUID getUid() {
    return uid;
  }

  @Override
  public void setUid(UUID uid) {
    this.uid = uid;
  }

  @Override
  public String getTaskCategory() {
    return taskCategory;
  }

  @Override
  public void setTaskCategory(String taskCategory) {
    this.taskCategory = taskCategory;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public int getRequiredCores() {
    return requiredCores;
  }

  @Override
  public void setRequiredCores(int requiredCores) {
    this.requiredCores = requiredCores;
  }

  @Override
  public Date getSubmittedAt() {
    return submittedAt;
  }

  @Override
  public void setSubmittedAt(Date submittedAt) {
    this.submittedAt = submittedAt;
  }
}
