/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.scalesinformatics.hazelcast.concurrent;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.scalesinformatics.hazelcast.DefaultHazelcastConfig;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Tests distributed locks work correctly
 * @author Keith Flanagan
 */
public class TestLocks
{
//  private static final int CPUS = 48;
  private static final int CPUS = 100;
  private static final int NUM_THREADS_TO_RUN = 1000000;
  private static final long THREAD_SLEEP_MS = 60;

  private static final String LOCK_NAME = "someLock";
  private static int maxLocksPerIteration = 10;

  private static int commonResource = 0;


  private static ScheduledThreadPoolExecutor exe;

  public TestLocks(final HazelcastInstance hazelcast)
  {
    exe = new ScheduledThreadPoolExecutor(CPUS);

    System.out.println("Testing distributed locks with "+NUM_THREADS_TO_RUN
        +" threads, with "+CPUS+" executing in parallel");
    System.out.println("For each iteration, each thread acquires lock tp to "
        + maxLocksPerIteration+" times");

    int expectedValue = (NUM_THREADS_TO_RUN);
    System.out.println("Expecting final variable value to be: "
        + expectedValue);

    Set<Runnable> workers = new HashSet<Runnable>();
    for (int i=0; i<NUM_THREADS_TO_RUN; i++)
    {
      Runnable worker = new Runnable() {
        @Override
        public void run()
        {
//          try
//          {
////            Thread.sleep( (long) (THREAD_SLEEP_MS * Math.random()));
//          }
//          catch (InterruptedException ex)
//          {
//            Logger.getLogger(TestLocks.class.getName()).log(Level.SEVERE, null, ex);
//          }
          DistributedLock lock = new DistributedLock(hazelcast, LOCK_NAME);
          try
          {
            lock.acquireLock();
          }
          catch(Throwable e)
          {
            e.printStackTrace();
            System.exit(1);
          }
          int timesLocked = 1;
          try
          {
            //Thread.sleep(THREAD_SLEEP_MS);
            long sleep = (long) (THREAD_SLEEP_MS * Math.random());
            System.out.println("Thread: "+Thread.currentThread().getName()
                +" id: " + Thread.currentThread().getId()
                +" sleeping for: "+sleep);
            Thread.sleep(sleep);

            for (int i=0; i<maxLocksPerIteration; i++)
            {
              if (Math.random() > 0.75)
              {
                lock.acquireLock();
                timesLocked++;
              }
            }

            commonResource++;
          }
          catch (InterruptedException ex)
          {
            Logger.getLogger(TestLocks.class.getName()).log(Level.SEVERE, null, ex);
          }
          finally
          {
            for (int i=0; i<timesLocked; i++)
            {
              lock.releaseLock();
            }
          }


        }
      };
      workers.add(worker);
    }

    Set<Future> futures = new HashSet<Future>();
    for (Runnable worker : workers)
    {
      Future f = exe.submit(worker);
      futures.add(f);
    }
    System.out.println("Waiting for "+NUM_THREADS_TO_RUN+" to completed");
    for (Future f : futures)
    {
      while(true)
      {
        try
        {
          f.get();
          break;
        }
        catch (InterruptedException ex)
        {
          Logger.getLogger(TestLocks.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (ExecutionException ex)
        {
          Logger.getLogger(TestLocks.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }

    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++");
    System.out.println("Done. Value was: "+commonResource
        + ". Expected was: "+expectedValue);

    Hazelcast.shutdownAll();
  }


  public static void main(String[] args)
  {
    DefaultHazelcastConfig hzConfig = new DefaultHazelcastConfig("TestLocks-cluster", "password...").specifyNetworkInterfaces("10.*","192.168.0.*");
    HazelcastInstance hz = Hazelcast.newHazelcastInstance(hzConfig);
    TestLocks tester = new TestLocks(hz);
  }

}
