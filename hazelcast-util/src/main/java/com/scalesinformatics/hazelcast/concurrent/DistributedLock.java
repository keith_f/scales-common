/*
 * Copyright 2010 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 12-Nov-2010, 12:46:05
 */

package com.scalesinformatics.hazelcast.concurrent;

import com.hazelcast.core.HazelcastInstance;
import java.util.concurrent.locks.Lock;
import java.util.logging.Logger;

/**
 * Utility class that provides distributed locking functionality. Actual
 * locking is delegated to Hazelcast. This class provides additional utilities,
 * such as checks to ensure a distributed lock is only used by one local thread.
 *
 * @author Keith Flanagan
 */
public class DistributedLock
    implements Lockable
{
  private static final Logger logger =
      Logger.getLogger(DistributedLock.class.getName());
  /**
   * The unique name of this distributed lock
   */
  private final String lockName;
  //private final ILock lock;
  private final Lock lock;
//  private long lockedBy;
  private String lockedBy;
  private int timesLocked;

  public DistributedLock(HazelcastInstance hazelcast, String lockName)
  {
    this.lockName = lockName;
    timesLocked = 0;
    lock = hazelcast.getLock(lockName);
  }

  @Override
  public void acquireLock()
  {
    //long acquiringThreadId = Thread.currentThread().getId();
    String acquiringThreadId = Thread.currentThread().getName();
//    logger.log(Level.INFO, "Thread: {0} **Attempting** to acquire lock: {2}",
//        new Object[]{acquiringThreadId, lockName});
    lock.lock();
    synchronized(this)
    {
      if (timesLocked > 0 && (!acquiringThreadId.equals(lockedBy)))
      {
        throw new RuntimeException(
            "Lock "+lockName+" was succesfully acquired by: " + acquiringThreadId
            + " even though it was already locked by process: "+lockedBy
            + ". Lock had been acquired "+timesLocked+" times.");
      }
      lockedBy = acquiringThreadId;
      timesLocked++;
//      logger.log(Level.INFO,
//          "Lock: {0} acquired by thread: {1}. Now locked {2} times.",
//          new Object[]{lockName, lockedBy, timesLocked});
    }
  }

  @Override
  public void releaseLock()
  {
    synchronized(this)
    {
      lock.unlock();

      timesLocked--;
//      logger.log(Level.INFO,
//          "Lock: {0} released by thread: {1}. Now locked {2} times.",
//          new Object[]{lockName, lockedBy, timesLocked});
      if (timesLocked == 0)
      {
        lockedBy = null;
      } 
    }
  }
}
