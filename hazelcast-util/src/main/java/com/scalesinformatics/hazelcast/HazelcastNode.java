package com.scalesinformatics.hazelcast;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.UnknownHostException;

/**
 * A simple commandline Hazelcast node. You can use this program to donate
 * the host computer's RAM resources to a Hazelcast cluster. Simply start this
 * program with a Hazelcast configuration file containing the details for the
 * network that it should join.
 */
public class HazelcastNode 
{
  public static void main(String[] args) {
    if (args.length == 0) {
      System.out.println("Usage: \n"+
          "  * Cluster name to join\n" +
          "  * Cluster password\n");
    }
    String clusterName = args[0];
    String clusterPassword = args[1];

    Config config = HazelcastConfigFactory.makeDefaultConfig(clusterName, clusterPassword);
    HazelcastInstance hz = HazelcastConfigFactory.makeInstance(config);

//    DefaultHazelcastConfig hzConfig = new DefaultHazelcastConfig(clusterName, clusterPassword).
//        specifyNetworkInterfaces("10.*", "192.168.0.*");
//    HazelcastInstance hz = Hazelcast.newHazelcastInstance(hzConfig);
    System.out.println("\n\n\n\n");
    System.out.println("***************************************");
    System.out.println("Compute server started. CTRL-C to exit.");
    System.out.println("***************************************");
    System.out.println("\n\n\n\n");
  }
}
