/*
 * Copyright 2013 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.hazelcast;

import java.net.InetAddress;
import java.util.HashSet;
import java.util.Set;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

/**
 * A convenience utility that uses <code>DefaultHazelcastConfig</code> to create a <code>Config</code> object, and then
 * pass it to a newly constructed <code>HazelcastInstance</code>.
 *
 * @author Keith Flanagan
 */
public class ScalesHazelcastInstanceFactory {

  /**
   * Creates a HazelcastInstance by using all IPv4 'local' subnets, including 'localhost', 10.x.x.x, etc.
   *
   * @param clusterName
   * @param clusterPassword
   * @return
   */
  public static HazelcastInstance createInstanceBoundToLocalNets(String clusterName, String clusterPassword)
      throws HazelcastInstantiationException {
    Set<String> hzBindAddrs = new HashSet<>();
    hzBindAddrs.add("127.0.0.1");
    hzBindAddrs.add("127.0.1.1");
    hzBindAddrs.add("192.168.*.*");
    hzBindAddrs.add("10.*.*.*");
    hzBindAddrs.add("172.16.*.*");
    hzBindAddrs.add("128.240.*.*");

    return createInstance(clusterName, clusterPassword, hzBindAddrs);
  }

  public static HazelcastInstance createInstance(String clusterName, String clusterPassword, Set<String> hzBindAddrs)
      throws HazelcastInstantiationException {
    try {
      DefaultHazelcastConfig hzConfig = new DefaultHazelcastConfig(clusterName, clusterPassword);
      hzConfig.setProperty("hazelcast.rest.enabled", "true");
      if (hzBindAddrs.isEmpty()) {
        String hostname = InetAddress.getLocalHost().getHostAddress();
        hzConfig.specifyNetworkInterfaces(hostname);
      } else {
        hzConfig.specifyNetworkInterfaces(hzBindAddrs.toArray(new String[0]));
      }
      HazelcastInstance hzInstance = Hazelcast.newHazelcastInstance(hzConfig);
      return hzInstance;
    } catch (Exception e) {
      throw new HazelcastInstantiationException("Failed to create Hazelcast instance", e);
    }
  }

}
