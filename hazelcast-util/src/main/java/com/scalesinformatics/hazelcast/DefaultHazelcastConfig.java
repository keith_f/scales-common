/*
 * Copyright 2013 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.hazelcast;

import com.hazelcast.config.*;

/**
 * @author Keith Flanagan
 */
@Deprecated
public class DefaultHazelcastConfig extends Config {
  private static final int DEFAULT_PORT = 8000;

  public static enum AwsRegion {
    EU_WEST_1 ("eu-west-1"),
    US_EAST_1 ("us-east-1");

    private final String name;

    private AwsRegion(String s) {
      name = s;
    }

    @Override
    public String toString() {
      return name;
    }
  }

  /**
   * Creates a Hazelcast configuration object using mostly default settings, and a few guesses that should work for
   * most purposes.
   *
   * @param clusterName the name of the cluster to connect to
   * @param clusterPassword the password for the specified cluster. Note, as of Hazelcast 3-RC1, the configuration
   *                        <b>seems</b> to be totally ignoring the clusterName, and differentiating clusters ONLY
   *                        by <code>clusterPassword</code>!
   */
  public DefaultHazelcastConfig(String clusterName, String clusterPassword) {
    super();

    getGroupConfig().setName(clusterName);
    getGroupConfig().setName(clusterPassword);

    /*
     * Basic network settings
     */
    NetworkConfig network = getNetworkConfig();
    network.setPort(DEFAULT_PORT);
    network.setPortAutoIncrement(true);

    /*
     * Multicast
     */
    JoinConfig join = network.getJoin();
    MulticastConfig multicastConfig = join.getMulticastConfig();
    multicastConfig.setMulticastGroup("224.2.2.3");
    multicastConfig.setMulticastPort(54327);
    multicastConfig.setEnabled(true);

    TcpIpConfig tcpIpConfig = network.getJoin().getTcpIpConfig();
    tcpIpConfig.setEnabled(true);

    network.getInterfaces().setEnabled(true);
    network.getInterfaces().addInterface("192.168.*.*");
    network.getInterfaces().addInterface("10.*.*.*");
//    network.getJoin().getMulticastConfig().addTrustedInterface("192.168.0.*");

//    for (String address : bindAddresses) {
//      System.out.println("Adding address: "+address);
//      network.getInterfaces().addInterface(address);
//      network.getJoin().getMulticastConfig().addTrustedInterface(address);
//    }
  }


  /**
   * Configures which network interface(s) to bind to.
   * @param interfaceStrings a list of IPs corresponding to network interfaces that you need Hazelcast to listen on.
   *                         For example, "10.*", "128.240.*" would ensure that if a machine had one or more cards
   *                         within the specified IP ranges, that Hazelcast would use those ranges.
   * @return
   */
  public DefaultHazelcastConfig specifyNetworkInterfaces(String... interfaceStrings) {
    NetworkConfig network = getNetworkConfig();
    network.getInterfaces().setEnabled(true);
    for (String interfaceString : interfaceStrings) {
      network.getInterfaces().addInterface(interfaceString);
      network.getJoin().getMulticastConfig().addTrustedInterface(interfaceString);
    }

    return this;
  }



  /**
   * Configures a specified set of peers to connect to, regardless of other peer finding methods.
   *
   * @param hostnames the list of hostnames or IP addresses to connect to directly.
   * @return
   */
  public DefaultHazelcastConfig specifySpecificPeers(String... hostnames) {
    NetworkConfig network = getNetworkConfig();
    TcpIpConfig tcpIpConfig = network.getJoin().getTcpIpConfig();
    tcpIpConfig.setEnabled(true);
    for (String hostname : hostnames) {
      tcpIpConfig.addMember(hostname);
    }

    return this;
  }

  public DefaultHazelcastConfig specifyAWSConfig(String awsAccessKey, String awsSecretKey, AwsRegion region) {
    NetworkConfig network = getNetworkConfig();
    AwsConfig awsConfig = network.getJoin().getAwsConfig();
    awsConfig.setEnabled(true);
    awsConfig.setAccessKey(awsAccessKey);
    awsConfig.setSecretKey(awsSecretKey);
    awsConfig.setRegion(region.name);

    return this;
  }


}
