#!/bin/bash

# Microbase launcher script
# Author: Keith Flanagan

# The Java class to run
EXE_CLASSNAME="com.scalesinformatics.hibernate.CreateSchema"
RAM="-Xmx64M"

# Get the filename of this script
SCRIPT_NAME=$0
SCRIPT_PATH=`which $0`
PROG_HOME=`dirname $SCRIPT_PATH`
#echo "Program home directory: $PROG_HOME"

#MVN_OPTS="-o"

MVN="mvn $MVN_OPTS"

if [ ! -d $PROG_HOME/target/dependency ] ; then
    # Install dependencies into the program's target directory if necessary
    ( cd $PROG_HOME ; $MVN dependency:copy-dependencies )
fi

# Configure CLASSPATH
# Include program's compiled classes
CLASSPATH=$CLASSPATH:$PROG_HOME/target/classes

# Also include 'target/classes' in the current directory - this probably contains the data beans that we're creating the SQL schema for.
CLASSPATH=$CLASSPATH:./target/classes

# Include .jar dependencies
for LIB in `find $PROG_HOME/target/dependency -name "*.jar"` ; do
    CLASSPATH=$CLASSPATH:$LIB
done

# Add a directory to which responder classes and jars can be added
CLASSPATH=$CLASSPATH:$PROG_HOME/responder-cp

# Include responder .jar files
for LIB in `find $PROG_HOME/responder-cp -name "*.jar"` ; do
    CLASSPATH=$CLASSPATH:$LIB
done

echo $CLASSPATH

# Finally, start the application
java $RAM -cp $CLASSPATH $EXE_CLASSNAME $@



