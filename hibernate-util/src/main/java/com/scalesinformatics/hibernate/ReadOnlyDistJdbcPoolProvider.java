/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 26-Mar-2012, 01:17:54
 */

package com.scalesinformatics.hibernate;

import java.sql.Connection;
import java.sql.SQLException;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Keith Flanagan
 */
public class ReadOnlyDistJdbcPoolProvider
    extends DistJdbcPoolProvider
{

  public ReadOnlyDistJdbcPoolProvider(Configuration conf)
      throws HibernateUtilException
  {
    super(conf);
    System.out.println("+++++++++++++++++++ Constructor:  "+conf.getProperty("foo.bar"));
  }
  
  @Override
  public Connection getConnection()
      throws SQLException
  {
    System.out.println("+++++++++++++++++++ getConnection:  "+conf.getProperty("foo.bar"));
    return pool.getReadOnlyConnection();
  }

}
