/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 25-Mar-2012, 12:39:15
 */
package com.scalesinformatics.hibernate;

import java.io.File;
import java.net.URL;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.BootstrapServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.hibernate.service.internal.BootstrapServiceRegistryImpl;
import org.hibernate.service.jdbc.connections.spi.ConnectionProvider;

/**
 * Provides a bridge between Hibernate and the DistributedJdbcPool
 * connection pool implementation. This class can be used to obtain 
 * Hibernate Session objects for either read-only or read/write databases.
 * 
 * @author Keith Flanagan
 */
public class SessionProvider
{
  private static final Logger logger = 
      Logger.getLogger(SessionProvider.class.getSimpleName());
  
  private SessionFactory readFactory;
  private SessionFactory writeFactory;
  
  /**
   * Creates a new SessionProvider using the default Hibernate 
   * config file. This file is usually <code>hibernate.cfg.xml</code> and
   * located somewhere on the classpath. Only use this constructor for simple
   * projects that connect to a single database, and therefore have a single
   * Hibernate configuration file.
   * 
   * @throws HibernateUtilException 
   */
  public SessionProvider()
      throws HibernateUtilException
  {
    Configuration conf = new Configuration().configure();
    readFactory = buildReadOnlyFactory(conf);
    
    conf = new Configuration().configure();
    writeFactory = buildWritableFactory(conf);
  }
  
  /**
   * Creates a new SessionProvider using the a Hibernate 
   * configuration file located somewhere on the classpath. 
   * This file is specified by <code>configResourcePath</code>. Use this 
   * constructor where you need multiple SessionProviders to connect to
   * different databases and need different Hibernate configuration files.
   * 
   * @param configResourcePath a path to a resource on the classpath that
   * contains Hibernate configuration for the required database.
   * @throws HibernateUtilException 
   */
  public SessionProvider(String configResourcePath) 
      throws HibernateUtilException
  {
    Configuration conf = new Configuration().configure(configResourcePath);
    readFactory = buildReadOnlyFactory(conf);
    
    conf = new Configuration().configure(configResourcePath);
    writeFactory = buildWritableFactory(conf);
  }
  
  /**
   * Creates a new SessionProvider using the a Hibernate 
   * configuration file located somewhere on the filesystem. 
   * This file is specified by <code>configFile</code>. Use this 
   * constructor where you need multiple SessionProviders to connect to
   * different databases and need different Hibernate configuration files.
   * 
   * @param configFile a path to a file on the local filesystem containing a
   * Hibernate configuration for the required database.
   * @throws HibernateUtilException 
   */
  public SessionProvider(File configFile) 
      throws HibernateUtilException
  {
    Configuration conf = new Configuration().configure(configFile);
    readFactory = buildReadOnlyFactory(conf);
    
    conf = new Configuration().configure(configFile);
    writeFactory = buildWritableFactory(conf);
  }
  
  /**
   * Creates a new SessionProvider using the a Hibernate 
   * configuration file located somewhere on a server. 
   * This file is specified by <code>configFileUrl</code>. Use this 
   * constructor where you need multiple SessionProviders to connect to
   * different databases and need different Hibernate configuration files.
   * 
   * @param configFileUrl a URL that points to a remote Hibernate 
   * configuration file.
   * @throws HibernateUtilException 
   */
  public SessionProvider(URL configFileUrl) 
      throws HibernateUtilException
  {
    Configuration conf = new Configuration().configure(configFileUrl);
    readFactory = buildReadOnlyFactory(conf);
    
    conf = new Configuration().configure(configFileUrl);
    writeFactory = buildWritableFactory(conf);
  }
  
  
  private SessionFactory buildReadOnlyFactory(Configuration conf) 
      throws HibernateUtilException
  {
    try
    {
      //See: http://www.javalobby.org/java/forums/t18406.html
      //Also: https://hibernate.onjira.com/browse/HHH-6609 (comments section)
      
      BootstrapServiceRegistry bsr = new BootstrapServiceRegistryImpl();
      ServiceRegistryBuilder builder = new ServiceRegistryBuilder(bsr);
      
      /*
       * Read only session factory
       */
      // Create the SessionFactory from hibernate.cfg.xml
//      Configuration conf = new Configuration().configure();
//      System.out.println("+++++++++++++++++++ "+conf.getProperty("foo.bar"));
      
      
      //Then override the connection provider with our own pool implementation
      builder.addService( ConnectionProvider.class, new ReadOnlyDistJdbcPoolProvider(conf) );
      SessionFactory sessionFactory = conf.buildSessionFactory(builder.buildServiceRegistry());
      return sessionFactory;
    }
    catch (Exception ex)
    {
      System.err.println("Initial SessionFactory creation failed." + ex);
      throw new HibernateUtilException("Initial SessionFactory creation failed.", ex);
    }
  }
  
  private SessionFactory buildWritableFactory(Configuration conf) 
      throws HibernateUtilException
  {
    try
    {
      //See: http://www.javalobby.org/java/forums/t18406.html
      //Also: https://hibernate.onjira.com/browse/HHH-6609 (comments section)
      
      BootstrapServiceRegistry bsr = new BootstrapServiceRegistryImpl();
      ServiceRegistryBuilder builder = new ServiceRegistryBuilder(bsr);
      
      /*
       * Writable session factory
       */
      // Create the SessionFactory from hibernate.cfg.xml
//      Configuration conf = new Configuration().configure();
      builder.addService( ConnectionProvider.class, new WritableDistJdbcPoolProvider(conf) );
      SessionFactory sessionFactory = conf.buildSessionFactory(builder.buildServiceRegistry());
      
      return sessionFactory;
    }
    catch (Throwable ex)
    {
      // Make sure you log the exception, as it might be swallowed
      System.err.println("Initial SessionFactory creation failed." + ex);
      throw new HibernateUtilException("Initial SessionFactory creation failed.", ex);
    }
  }
  

//  private void buildFactories(final String configResourcePath) 
//      throws HibernateUtilException
//  {
//    try
//    {
//      //See: http://www.javalobby.org/java/forums/t18406.html
//      
//      /*
//       * Read only session factory
//       */
//      //Create a provider that is configured to use app-specific config resource
//      ReadOnlyDistJdbcPoolProvider readProvider = 
//          new ReadOnlyDistJdbcPoolProvider() {
//        @Override
//        protected String getConfigurationResource()
//        {
//          return configResourcePath;
//        }
//      };
//      // Create the SessionFactory from hibernate.cfg.xml
//      Configuration conff = new Configuration().c
//      Configuration conf = new Configuration().configure();
//      System.out.println("+++++++++++++++++++ "+conf.getProperty("foo.bar"));
//      //Then override the connection provider with our own pool implementation
//      conf.setProperty(Environment.CONNECTION_PROVIDER, 
//          readProvider.getClass().getName());
//      readFactory = conf.buildSessionFactory();
//      
//      
//      
//      
//      /*
//       * Writable session factory
//       */
//      //Create a provider that is configured to use app-specific config resource
//      WritableDistJdbcPoolProvider writableProvider = 
//          new WritableDistJdbcPoolProvider() {
//        @Override
//        protected String getConfigurationResource()
//        {
//          return configResourcePath;
//        }
//      };
//      // Create the SessionFactory from hibernate.cfg.xml
//      conf = new Configuration().configure();
//      //Then override the connection provider with our own pool implementation
////      conf.setProperty(Environment.CONNECTION_PROVIDER, 
////          WritableDistJdbcPoolProvider.class.getName());
//      conf.setProperty(Environment.CONNECTION_PROVIDER, 
//          writableProvider.getClass().getName());
//      writeFactory = conf.buildSessionFactory();
//    }
//    catch (Throwable ex)
//    {
//      // Make sure you log the exception, as it might be swallowed
//      System.err.println("Initial SessionFactory creation failed." + ex);
//      throw new HibernateUtilException("Initial SessionFactory creation failed.", ex);
//    }
//  }
//  

  public SessionFactory getReadSessionFactory() 
      throws HibernateUtilException
  {
    return readFactory;
  }
  
  public SessionFactory getWriteSessionFactory() 
      throws HibernateUtilException
  {
    return writeFactory;
  }  
  
  public Session getReadOnlySession()
      throws HibernateUtilException
  {
    try
    {
      logger.info("******************** Obtaining READ ONLY DB Session");
//      Session session = getSessionFactory().getCurrentSession();
//      Session session = getReadSessionFactory().getCurrentSession();
      Session session = getReadSessionFactory().openSession();
      return session;
    }
    catch(Exception e)
    {
      throw new HibernateUtilException("Failed to obtain read-only connection", e);
    }
  }
  
  public Session getWriteableSession()
      throws HibernateUtilException
  {
    try
    {
      logger.info("******************** Obtaining WRITABLE DB Session");
//      Session session = getSessionFactory().getCurrentSession();
//      Session session = getWriteSessionFactory().getCurrentSession();
      Session session = getWriteSessionFactory().openSession();
      return session;
    }
    catch(Exception e)
    {
      throw new HibernateUtilException("Failed to obtain writable connection", e);
    }
  }
  
  public static void silentRollback(Session session)
  {
    try
    {
      if (session == null)
      {
        return;
      }
      session.getTransaction().rollback();
    }
    catch(Exception e)
    {
      System.err.println(
          "WARNING: Caught exception while silently rolling back a session: "
          + e.getMessage());
//      e.printStackTrace();
    }
  }
  
  public static void silentClose(Session session)
  {
    try
    {
      if (session == null)
      {
        return;
      }
//      System.out.println("Silently closing a connection. isConnected: "
//          + session.isConnected() + ", isOpen: " + session.isOpen());
      session.close();
    }
    catch(Exception e)
    {
      System.err.println(
          "WARNING: Caught exception while silently closing a session: "
          + e.getMessage());
//      e.printStackTrace();
    }
  }
  
}
