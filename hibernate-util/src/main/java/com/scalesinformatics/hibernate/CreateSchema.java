/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 26-Mar-2012, 02:01:31
 */
package com.scalesinformatics.hibernate;

import java.io.File;
import java.io.FileWriter;
import java.net.URL;
import java.util.Arrays;
import org.apache.commons.cli.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.dialect.MySQLDialect;


/**
 *
 * @author Keith Flanagan
 */
public class CreateSchema
{

  private static void printHelpExit(Options options)
  {
    printHelpExit(options, null);
  }

  private static void printHelpExit(Options options, String additionalFooter)
  {
    HelpFormatter formatter = new HelpFormatter();
    String cmdSyntax = "create_schema.sh";
    String header = "";
    StringBuilder footer = new StringBuilder();
    footer.append("This program takes a Hibernate configuration file and "
        + "prints the SQL required to generate the schema it defines. This "
        + "file can be obtained from the filesystem, from the classpath, or "
        + "from a remote server. Schema output is sent either to STDOUT, or "
        + "to a file (if specified)\n\n");
    if (additionalFooter != null)
    {
      footer.append("\n").append(additionalFooter);
    }
    int width = 80;
    //formatter.printHelp( "notification.sh", options );
    formatter.printHelp(width, cmdSyntax, header, options, footer.toString());
    System.exit(0);
  }
  
  public static void main(String[] args)
  {
    CommandLineParser parser = new PosixParser();

    Options options = new Options();
    
    options.addOption(OptionBuilder
        .withLongOpt("hibernate-config-file")
        .hasArgs(1)
        .withDescription(
        "The Hibernate configuration file containing the schema to print "
        + "(file path).")
        .create( "h" ));
    
    options.addOption(OptionBuilder
        .withLongOpt("hibernate-config-resource")
        .hasArgs(1)
        .withDescription(
        "The Hibernate configuration file containing the schema to print "
        + "(classpath resource).")
        .create( "r" ));
    
    options.addOption(OptionBuilder
        .withLongOpt("hibernate-config-url")
        .hasArgs(1)
        .withDescription(
        "The Hibernate configuration file containing the schema to print "
        + "(HTTP/FTP URL).")
        .create( "u" ));
    
    options.addOption(OptionBuilder
        .withLongOpt("output")
        .hasArgs(1)
        .withDescription(
        "A file to which the schema should be printed.")
        .create( "o" ));
    
    if (args.length == 0)
    {
      printHelpExit(options);
    }

    File configFile = null;
    String configResource = null;
    URL configUrl = null;
    File outputFile = null;
    try
    {
      CommandLine line = parser.parse(options, args);

      System.out.println(Arrays.asList(line.getArgs()));
      System.out.println(Arrays.asList(line.getOptions()));

      if (line.hasOption("hibernate-config-file"))
      {
        configFile = new File(line.getOptionValue("hibernate-config-file"));
      }
      if (line.hasOption("hibernate-config-resource"))
      {
        configResource = line.getOptionValue("hibernate-config-resource");
      }
      if (line.hasOption("hibernate-config-url"))
      {
        configUrl = new URL(line.getOptionValue("hibernate-config-url"));
      }
      if (line.hasOption("output"))
      {
        outputFile = new File(line.getOptionValue("output"));
      }
    }
    catch(Exception e)
    {
      e.printStackTrace();
      printHelpExit(options);
    }
    
    Configuration cfg = null;
    if (configFile != null)
    {
      System.out.println("Reading Hibernate configuration from file: "
          + configFile.getAbsolutePath());
      cfg = new Configuration().configure(configFile);
    } 
    else if (configResource != null)
    {
      System.out.println("Reading Hibernate configuration from resource: "
          + configResource);
      cfg = new Configuration().configure(configResource);
    } 
    else if (configUrl != null)
    {
      System.out.println("Reading Hibernate configuration from URL: "
          + configUrl.toString());
      cfg = new Configuration().configure(configUrl);
    }
    
    if (cfg == null)
    {
      printHelpExit(options, 
          "No Hibernate configuration file location was specified!");
    }
    
//    String[] lines = cfg.generateSchemaCreationScript(new PostgreSQLDialect());
    String[] lines = cfg.generateSchemaCreationScript(new MySQLDialect());    
    StringBuilder sql = new StringBuilder();
    for (int i = 0; i < lines.length; i++)
    {
      sql.append(lines[i]).append(";\n");
    }
    
    System.out.println("\n\nGenerated SQL:\n");
    System.out.println(sql.toString());
    
    if (outputFile != null)
    {
      try
      {
        FileWriter fw = new FileWriter(outputFile);
        fw.write(sql.toString());
        fw.close();
      }
      catch(Exception e)
      {
        e.printStackTrace();
        printHelpExit(options, 
            "Failed to write output file: "+outputFile.getAbsolutePath());
      }
    }
  }
}
