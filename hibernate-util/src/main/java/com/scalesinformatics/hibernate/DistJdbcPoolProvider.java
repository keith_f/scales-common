package com.scalesinformatics.hibernate;

/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 26-Mar-2012, 01:17:54
 */



import com.scalesinformatics.jdbcutils.DatabaseConfiguration;
import com.scalesinformatics.jdbcutils.DistributedJdbcPool;
import com.scalesinformatics.jdbcutils.DistributedJdbcPoolFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.jdbc.connections.spi.ConnectionProvider;

/**
 *
 * @author Keith Flanagan
 */
abstract public class DistJdbcPoolProvider
    implements ConnectionProvider
{
  private static final String HIB_PROP_USERNAME = "dist.jdbc.pool.username";
  private static final String HIB_PROP_PASSWORD = "dist.jdbc.pool.password";
  private static final String HIB_PROP_JDBC_WRITE_PREFIX = "dist.jdbc.pool.url.writable";
  private static final String HIB_PROP_JDBC_READ_PREFIX = "dist.jdbc.pool.url.readonly";
  
  
  protected final Configuration conf;
  protected final DistributedJdbcPool pool;

  public DistJdbcPoolProvider(Configuration conf)
      throws HibernateUtilException
  {
    this.conf = conf;
    pool = createPool();
  }
  
  private DistributedJdbcPool createPool() 
      throws HibernateUtilException
  {
    try
    {
      DatabaseConfiguration config = new DatabaseConfiguration();
      config.setUsername(parseUsername(conf));
      config.setPassword(parsePassword(conf));
      config.setReadUrls(parseJdbcUrls(conf, HIB_PROP_JDBC_READ_PREFIX));
      config.setWriteUrls(parseJdbcUrls(conf, HIB_PROP_JDBC_WRITE_PREFIX));
      
//      DatabaseConfiguration config = DistributedJdbcPoolFactory.
//          readConfigurationFromPropertResource(resourcePath);
      
      DistributedJdbcPool pool = 
          DistributedJdbcPoolFactory.
          createSingleWriteMultipleReplicaPool(config);
      return pool;
    }
    catch(Exception e)
    {
      throw new HibernateUtilException(
          "Failed to configure a distributed ConnectionProvider for Hibernate", e);
    }
//    catch(DatabaseConfigurationException e)
//    {
//      throw new HibernateUtilException(
//          "Failed to configure a distributed ConnectionProvider for Hibernate", e);
//    }
  }

  private String parseUsername(Configuration conf)
      throws HibernateUtilException
  {
    String val = conf.getProperty(HIB_PROP_USERNAME);
    if (val == null)
    {
      throw new HibernateUtilException(
          "No username set in the Hibernate configuration file "
          + "(property: "+HIB_PROP_USERNAME+")");
    }
    return val;
  }
  private String parsePassword(Configuration conf)
      throws HibernateUtilException
  {
    String val = conf.getProperty(HIB_PROP_PASSWORD);
    if (val == null)
    {
      throw new HibernateUtilException(
          "No password set in the Hibernate configuration file "
          + "(property: "+HIB_PROP_PASSWORD+")");
    }
    return val;
  }
  
  private Set<String> parseJdbcUrls(Configuration conf, String prefix)
      throws HibernateUtilException
  {
    Set<String> jdbcUrls = new HashSet<String>();
    for (int i=0; i<Integer.MAX_VALUE; i++)
    {
      StringBuilder propName = new StringBuilder(prefix);
      propName.append(".").append(i);
      
      String val = conf.getProperty(propName.toString());
      if (val == null)
      {
        break;
      }
      jdbcUrls.add(val);
    }
    
    if (jdbcUrls.isEmpty())
    {
      throw new HibernateUtilException(
          "No JDBC URLs were specified in the Hibernate configuration "
          + "file (property prefix: "+prefix+".XX)");
    }
    System.out.println("Parsed "+jdbcUrls.size()+" JDBC URLs for: "+prefix);
    return jdbcUrls;
  }
  
  @Override
  public void closeConnection(Connection cnctn)
      throws SQLException
  {
    // Nothing to do here
  }

  @Override
  public boolean supportsAggressiveRelease()
  {
    return false;
  }

  @Override
  public boolean isUnwrappableAs(Class type)
  {
    return false;
  }

  @Override
  public <T> T unwrap(Class<T> type)
  {
    return null;
  }

}
