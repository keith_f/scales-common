/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.util;

import java.security.SecureRandom;
import java.util.Random;
import java.util.logging.Logger;

/**
 * @author Keith Flanagan
 */
public class RandomUtils {
  private static final Logger logger = Logger.getLogger(RandomUtils.class.getName());

  public static Random rnd;

  private static final int WARM_UP = 100000;
  private static final double MAX_DOUBLE_RANGE = 100000;

  private static synchronized Random makeRandom() {
    logger.info("Warming up random number generator ...");
    //Random random = new Random();
    Random random = new SecureRandom();
    for (int i=0; i<WARM_UP; i++) {
      random.nextDouble();
    }
    logger.info("Random number generator warm-up complete!");
    return random;
  }

  public static Random getRandom() {
    if (rnd == null) {
      rnd = makeRandom();
    }
    return rnd;
  }

  public static double randomDoubleInRange(double min, double max) {
    Random random = getRandom();
    double range = max - min;
    //System.out.println("Min: "+min+"; Max: "+max+"; Range: "+range);
    return max - (random.nextDouble() * range);
  }

  /**
   * Returns a random long such that the result is:
   * <code>minIncl <= x < maxExcl</code>
   *
   * @param minIncl the minimum (inclusive) value that can be returned
   * @param maxExcl the maximum (exclusive) value that can be returned
   * @return
   */
  public static long randomLongInRange(long minIncl, long maxExcl) {
    return (long) randomDoubleInRange(minIncl, maxExcl);
  }

  public static int randomIntInRange(int minIncl, int maxExcl) {
    return (int) randomDoubleInRange(minIncl, maxExcl);
  }

  public static void main(String[] args) {
    System.out.println(randomDoubleInRange(0, 10));
    System.out.println(randomDoubleInRange(2, 10));
    System.out.println(randomDoubleInRange(-1000, 1000));


    System.out.println(randomDoubleInRange(-20, -10));


    for (int i=0; i<1000; i++) {
      System.out.print(randomLongInRange(-100, 100)+", ");
    }
  }
}
