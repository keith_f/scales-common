/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * @author Keith Flanagan
 */
public class BeanIO {
  public enum Encoding {
    JSON, XML;
  }

  private final ObjectMapper mapper;

  public BeanIO() {
    this(Encoding.JSON);
  }

  private BeanIO(Encoding encoding) {
    if (encoding == Encoding.JSON) {
      mapper = new ObjectMapper();
    } else if (encoding == Encoding.XML) {
      mapper = new XmlMapper();
    } else {
      throw new IllegalArgumentException("Unsupported encoding type");
    }

    /*
     * This stores object type information in the serialized JSON, and allows polymorphic
     * types to be deserialized. See:
     * http://wiki.fasterxml.com/JacksonPolymorphicDeserialization
     *
     * <pre>
     *  {
     *    "first" : [ "com.scalesinformatics.siliconbricks4.functions.Min", {
     *      "first" : [ "com.scalesinformatics.siliconbricks4.functions.DoubleLiteral", {
     *        "value" : 0.18525988791575243
     *      } ],
     *      "second" : [ "com.scalesinformatics.siliconbricks4.functions.Divide", {
     *        "first" : [ "com.scalesinformatics.siliconbricks4.functions.IfDouble", {
     *          "boolFunc" : [ "com.scalesinformatics.siliconbricks4.functions.IfBoolean", {
     *
     *     ....
     *
     * </pre>
     */
    mapper.enableDefaultTyping();

    // Don't throw exceptions if we try to serialize an empty bean
    mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
  }

  public void save(File saveFile, Object toSave) throws IOException {
    mapper.writerWithDefaultPrettyPrinter().writeValue(saveFile, toSave);
  }

  public byte[] save(Object toSave) throws IOException {
    return mapper.writerWithDefaultPrettyPrinter().writeValueAsBytes(toSave);
  }

  public String saveToString(Object toSave) throws IOException {
    return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(toSave);
  }

  public <T> T load(File loadFile, Class<T> type) throws IOException {
    return mapper.readValue(loadFile, type);
  }

  public <T> T load(File loadFile, TypeReference<T> type) throws IOException {
    return mapper.readValue(loadFile, type);
  }

  public <T> T load(byte[] bytes, Class<T> type) throws IOException {
    return mapper.readValue(bytes, type);
  }

  public <T> T load(byte[] bytes, TypeReference<T> type) throws IOException {
    return mapper.readValue(bytes, type);
  }

  public <T> T load(String serializedString, TypeReference<T> type) throws IOException {
    return mapper.readValue(serializedString, type);
  }

  public <T> T load(String serializedString, Class<T> type) throws IOException {
    return mapper.readValue(serializedString, type);
  }



  public void saveGz(File saveFile, Object toSave) throws IOException {
    byte[] gzBytes = saveGz(toSave);
    FileUtils.writeByteArrayToFile(saveFile, gzBytes);
  }


  public byte[] saveGz(Object toSave) throws IOException {
    try (ByteArrayOutputStream baos = new ByteArrayOutputStream(8192);
         GZIPOutputStream os = new GZIPOutputStream(baos)) {
      mapper.writerWithDefaultPrettyPrinter().writeValue(os, toSave);

      return baos.toByteArray();
    }
  }


  public <T> T loadGz(File loadFile, TypeReference<T> type) throws IOException {
    return loadGz(FileUtils.readFileToByteArray(loadFile), type);
  }

  public <T> T loadGz(byte[] gzBytes, TypeReference<T> type) throws IOException {
    try (InputStream is = new GZIPInputStream(new ByteArrayInputStream(gzBytes))) {
      return mapper.readValue(is, type);
    }
  }

  public <T> T loadGz(File loadFile, Class<T> type) throws IOException {
    return loadGz(FileUtils.readFileToByteArray(loadFile), type);
  }

  public <T> T loadGz(byte[] gzBytes, Class<T> type) throws IOException {
    try (InputStream is = new GZIPInputStream(new ByteArrayInputStream(gzBytes))) {
      return mapper.readValue(is, type);
    }
  }


}
