/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.scalesinformatics.util;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Keith Flanagan
 */
public class NumDoubleFactory implements NumFactory<Double> {
  private final NumDouble ZERO;
  private final NumDouble HALF;
  private final NumDouble ONE;
  private final NumDouble TWO;
  private final NumDouble MAX_INT;

  private final MathContext context;

  private final Map<String, Num<Double>> cache;

  public NumDoubleFactory() {
    this(DEFAULT_CONTEXT);
  }

  public NumDoubleFactory(MathContext context) {
    this.context = context;
    this.cache = new HashMap<>();

    ZERO = new NumDouble(this, 0d, context);
    HALF = new NumDouble(this, 0.5d, context);
    ONE = new NumDouble(this, 1d, context);
    TWO = new NumDouble(this, 2d, context);
    MAX_INT = new NumDouble(this, (double) Integer.MAX_VALUE, context);
  }

  @Override
  public Num<Double> make(String value) {
    return new NumDouble(this, new BigDecimal(value, context).doubleValue(), context);
  }

  @Override
  public Num<Double> make(String value, MathContext context) {
    return new NumDouble(this, new BigDecimal(value, context).doubleValue(), context);
  }

  @Override
  public Num<Double> make(BigDecimal value) {
    return new NumDouble(this, value.doubleValue(), context);
//    return new NumBigDecimal(this, value, context);
  }

  @Override
  public Num<Double> make(BigDecimal value, MathContext context) {
    return new NumDouble(this, value.doubleValue(), context);
//    return new NumBigDecimal(this, value, context);
  }

  @Override
  public Num<Double> make(Double value) {
    return new NumDouble(this, value, context);
  }

  @Override
  public Num<Double> make(Double value, MathContext context) {
    return new NumDouble(this, value, context);
  }

  @Override
  public Num<Double> makeRandomBetween(BigDecimal lowerLimit, BigDecimal upperLimit, MathContext context) {
    BigDecimal rnd = new BigDecimal(Math.random(), context);
    BigDecimal range = upperLimit.subtract(lowerLimit, context);
    BigDecimal value = rnd.multiply(range, context).add(lowerLimit, context);

    return new NumDouble(this, value.doubleValue(), context);
  }

  @Override
  public Num<Double> getWellKnownNumber(WellKnownNumber number) {
    switch (number) {
      case ZERO:
        return ZERO;
      case HALF:
        return HALF;
      case ONE:
        return ONE;
      case TWO:
        return TWO;
      case MAX_INT:
        return MAX_INT;
      default:
        throw new IllegalArgumentException("Number not yet supported: "+number.name());
    }
  }

  @Override
  public void putNumber(String name, Num<Double> number) {
    cache.put(name, number);
  }

  @Override
  public Num<Double> getNumber(String name) {
    return cache.get(name);
  }

}
