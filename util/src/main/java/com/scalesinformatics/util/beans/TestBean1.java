/*
 * Copyright (c) 2009 jointly held by the authors listed at the top of 
 * each source file and/or their respective employers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.scalesinformatics.util.beans;

import java.util.List;

/**
 * @author Keith Flanagan
 */
public class TestBean1
{
  private int id;
  private String uid;
  private String uniqueToBean1;
  private int differentTypeDataItem;
  private List<String> aCollection;

  public TestBean1()
  {
  }

  public int getId()
  {
    return id;
  }

  public void setId(int id)
  {
    this.id = id;
  }

  public String getUid()
  {
    return uid;
  }

  public void setUid(String uid)
  {
    this.uid = uid;
  }

  public String getUniqueToBean1()
  {
    return uniqueToBean1;
  }

  public void setUniqueToBean1(String uniqueToBean1)
  {
    this.uniqueToBean1 = uniqueToBean1;
  }

  public int getDifferentTypeDataItem()
  {
    return differentTypeDataItem;
  }

  public void setDifferentTypeDataItem(int differentTypeDataItem)
  {
    this.differentTypeDataItem = differentTypeDataItem;
  }

  public List<String> getACollection()
  {
    return aCollection;
  }

  public void setACollection(List<String> aCollection)
  {
    this.aCollection = aCollection;
  }
}
