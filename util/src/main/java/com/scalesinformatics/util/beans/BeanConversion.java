/*
 * Copyright (c) 2009 jointly held by the authors listed at the top of 
 * each source file and/or their respective employers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.scalesinformatics.util.beans;

import java.lang.reflect.Method;
import java.beans.*;
import java.util.*;
import java.util.logging.Logger;

/**
 * Quite often  we need to be able to convert between one kind of data bean and
 * another that store similar, but not identical, kinds of information.
 * For instance, we might have a data bean used for persisting data in a
 * database, and another data bean used for Web service transport. The two beans
 * will often have:
 * <ul>
 * <li>a large number of common fields (e.g., integer coordinates
 * on a sequence).</li>
 * <li>some fields on one bean that aren't relevant to the other bean - for
 * example, internal database ids that we don't want to expose to the world</li>
 * <li>some fields that are present on both beans, but are different data types
 * or require some other kind of modification - for example, in a database
 * we might represent repetitive data as an foreign-key integer for performance
 * reasons, but when exposing data to the world, the foreign key should be
 * translated back into a String</li>
 * <ul>
 * <p/>
 * Inheritance isn't really appropriate in these cases, meaning manual
 * conversion is often required. This is tedious. Therefore, this class attempts
 * to automate conversion between two beans where the property names and types
 * match. Appropriate hooks are provided for you to implement specific
 * conversion where necessary.
 *
 * @author Keith Flanagan
 */
public class BeanConversion
{
  private static final Logger logger =
      Logger.getLogger(BeanConversion.class.getName());
  public BeanConversion()
  {
  }

  public void convert(Object source, Object target)
      throws ConversionException
  {
    try
    {
      BeanInfo sourceBeanInfo = Introspector.getBeanInfo(source.getClass(), Object.class);
      BeanInfo targetBeanInfo = Introspector.getBeanInfo(target.getClass(), Object.class);

      PropertyDescriptor[] getterProps = sourceBeanInfo.getPropertyDescriptors();
      PropertyDescriptor[] setterPropsArr = targetBeanInfo.getPropertyDescriptors();
      Map<String, PropertyDescriptor> setterProps = new HashMap<String, PropertyDescriptor>();
      for (PropertyDescriptor setter : setterPropsArr)
      {
        setterProps.put(setter.getName(), setter);
      }


      for (PropertyDescriptor prop : getterProps)
      {
        Method getter = prop.getReadMethod();
        //logger.info("Getter method: " + getter);

        PropertyDescriptor setterProp = setterProps.get(prop.getName());
        if (setterProp == null)
        {
          //logger.info("No matching property in target bean for: " + prop.getName());
          continue;
        }

        Method setter = setterProp.getWriteMethod();
        if (setter.getParameterTypes()[0].equals(getter.getReturnType()))
        {
          //logger.info("Matched: " + getter + " ----> " + setter);
          Object value = getter.invoke(source);
          setter.invoke(target, value);
        } else
        {
          //logger.info("Matching property name, but mismatching types: " +
          //   prop.getName());
          continue;
        }
      }
    }
    catch (Exception e)
    {
      throw new ConversionException("Failed to convert data bean from: " +
          source.getClass().getName() + " to: " +
          target.getClass().getName(), e);
    }
  }

  public static void main(String[] args)
      throws Throwable
  {
    TestBean1 source = new TestBean1();
    List<String> testList = new ArrayList<String>();
    testList.add("string 1");
    testList.add("string 2");
    source.setACollection(testList);
    source.setDifferentTypeDataItem(5);
    source.setId(7);
    source.setUid("some uid");
    source.setUniqueToBean1("unique to bean 1");

    TestBean2 target = new TestBean2();
    BeanConversion convertor = new BeanConversion();
    convertor.convert(source, target);

    logger.info("\n\n");
    logger.info("Target bean data:");
    logger.info("  collection: " + target.getACollection());
    logger.info("  different type test: " + target.getDifferentTypeDataItem());
    logger.info("  UID: " + target.getUid());
    logger.info("  Unique to bean2: " + target.getUniqueToBean2());

  }
}
