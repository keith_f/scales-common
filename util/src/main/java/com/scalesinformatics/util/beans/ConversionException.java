/*
 * Copyright (c) 2009 jointly held by the authors listed at the top of 
 * each source file and/or their respective employers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.scalesinformatics.util.beans;

/**
 * @author Keith Flanagan
 */
public class ConversionException
  extends Exception
{
  public ConversionException()
  {
  }

  public ConversionException(String s)
  {
    super(s);
  }

  public ConversionException(String s, Throwable throwable)
  {
    super(s, throwable);
  }

  public ConversionException(Throwable throwable)
  {
    super(throwable);
  }
}
