/*
 * Copyright (c) 2009 jointly held by the authors listed at the top of 
 * each source file and/or their respective employers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.scalesinformatics.util.beans;

import java.util.List;

/**
 * @author Keith Flanagan
 */
public class TestBean2
{
  private String uid;
  private String uniqueToBean2;
  private String differentTypeDataItem;
  private List<String> aCollection;

  public TestBean2()
  {
  }

  public String getUid()
  {
    return uid;
  }

  public void setUid(String uid)
  {
    this.uid = uid;
  }

  public String getUniqueToBean2()
  {
    return uniqueToBean2;
  }

  public void setUniqueToBean2(String uniqueToBean2)
  {
    this.uniqueToBean2 = uniqueToBean2;
  }

  public String getDifferentTypeDataItem()
  {
    return differentTypeDataItem;
  }

  public void setDifferentTypeDataItem(String differentTypeDataItem)
  {
    this.differentTypeDataItem = differentTypeDataItem;
  }

  public List<String> getACollection()
  {
    return aCollection;
  }

  public void setACollection(List<String> aCollection)
  {
    this.aCollection = aCollection;
  }
}
