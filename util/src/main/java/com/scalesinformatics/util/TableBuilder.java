
package com.scalesinformatics.util;

import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * An ASCII-art table formatter utility.
 *
 * Based on code posted here:
 * http://www.ksmpartners.com/2013/08/nicely-formatted-tabular-output-in-java/
 *
 * Adapted to suit the needs of various projects.
 *
 *
 * @author Michael Schaeffer
 * @author Keith Flanagan
 */
public class TableBuilder {
  public interface CellRenderer {
    default String getPrefixCtrlText() {
      return "";
    }

    String getMainText();

    default String getSuffixCtrlText() {
      return "";
    }

  }

  public static class StringCellRenderer implements CellRenderer {
    private final String prefixCtrlText;
    private final String suffixCtrlText;
    private final String mainText;

    public StringCellRenderer(String mainText) {
      this("", "", mainText);
    }

    public StringCellRenderer(String prefixCtrlText, String suffixCtrlText, String mainText) {
      this.prefixCtrlText = prefixCtrlText;
      this.suffixCtrlText = suffixCtrlText;
      this.mainText = mainText;
    }

    @Override
    public String getMainText() {
      return mainText;
    }

    @Override
    public String getPrefixCtrlText() {
      return prefixCtrlText;
    }

    @Override
    public String getSuffixCtrlText() {
      return suffixCtrlText;
    }
  }

  private static class HorizontalLineCellRenderer extends StringCellRenderer {
    public HorizontalLineCellRenderer() {
      super("");
    }
  }


  private static final String DEFAULT_DELIMITER = " | ";
  private static final String HORIZONTAL_ROW_MARKER = "\u0007--";
  private List<CellRenderer[]> rows = new LinkedList<>();

  private String delimiter = DEFAULT_DELIMITER;

  private final Map<Integer, Integer> colWidthOverrides;

  public TableBuilder() {
    this.colWidthOverrides = new HashMap<>();
  }

  public void addRow(String... cols) {
    StringCellRenderer[] displayableCols = new StringCellRenderer[cols.length];
    for (int i=0; i<cols.length; i++) {
      displayableCols[i] = new StringCellRenderer(cols[i]);
    }
    rows.add(displayableCols);
  }

  public void addRow(CellRenderer... cols) {
    rows.add(cols);
  }

  public void addHorizontalLine() {
    rows.add(new CellRenderer[] {new HorizontalLineCellRenderer()});
  }

  private int countCols() {
    int cols = -1;

    for (Object[] row : rows) {
      cols = Math.max(cols, row.length);
    }
    return cols;
  }

  private int[] colWidths(int numCols) {
    int[] widths = new int[numCols];

    for (CellRenderer[] row : rows) {
      if (row[0] instanceof HorizontalLineCellRenderer) {
        continue;
      }
      for (int colNum = 0; colNum < row.length; colNum++) {
        int mainTextLen = row[colNum].getMainText() != null ? row[colNum].getMainText().length() : 0;

        widths[colNum] =
            Math.max(widths[colNum], mainTextLen);
      }
    }

    for (Map.Entry<Integer, Integer> override : colWidthOverrides.entrySet()) {
      if (override.getKey() >= widths.length) {
        throw new IllegalArgumentException("Overridden column index was >= total number of columns: "
            + override.getKey() + " >= " + widths.length);
      }
      widths[override.getKey()] = override.getValue();
    }

    return widths;
  }

  @Override
  public String toString() {
    StringBuilder buf = new StringBuilder(1024);

    int numCols = countCols();
    int[] colWidths = colWidths(numCols);


    for (CellRenderer[] row : rows) {
      final boolean horizLine = row.length == 1 && row[0] instanceof HorizontalLineCellRenderer;

      for (int colNum = 0; colNum < numCols; colNum++) {
        if (horizLine) {
          buf.append(StringUtils.rightPad("", colWidths[colNum], '-'));
        } else if (colNum < row.length) {
          String mainText = row[colNum].getMainText();
          if (mainText == null) {
            mainText = "";
          }
          if (mainText.length() > colWidths[colNum]) {
            mainText = mainText.substring(0, colWidths[colNum]);
          }
          buf.append(row[colNum].getPrefixCtrlText());
          buf.append(StringUtils.rightPad(mainText, colWidths[colNum], ' '));
          buf.append(row[colNum].getSuffixCtrlText());
        } else {
          buf.append(StringUtils.rightPad("", colWidths[colNum], ' '));
        }

        buf.append(delimiter);
      }

      buf.append('\n');
    }

    return buf.toString();
  }

  public void addColumnWidthOverride(int colIdx, int forcedWidth) {
    if (colIdx < 0) {
      throw new IllegalArgumentException("Column index must be at >= 0");
    }
    if (forcedWidth < 1) {
      throw new IllegalArgumentException("Column width must be at >= 1");
    }
    colWidthOverrides.put(colIdx, forcedWidth);
  }

  public String getDelimiter() {
    return delimiter;
  }

  public void setDelimiter(String delimiter) {
    this.delimiter = delimiter;
  }
}
