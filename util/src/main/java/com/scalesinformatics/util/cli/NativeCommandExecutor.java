package com.scalesinformatics.util.cli;

import com.scalesinformatics.util.file.FileUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utility class to execute a native command from java.
 *
 * @author Keith Flanagan
 */
public class NativeCommandExecutor
{
  private static Logger logger = 
      Logger.getLogger(NativeCommandExecutor.class.getName());
  static 
  {
    logger.setLevel(Level.OFF);
  }

  public static int executeNativeCommand(Appendable stdOut, Appendable stdErr, String... commandLine)
      throws NativeCommandException
  {
    return executeNativeCommand(new File("."), new HashMap<>(), stdOut, stdErr, commandLine);
  }
  
  /**
   * Runs a native command. This method executes a native command and captures
   * the stdout and stderr streams that result. The results of execution are
   * returned in a convienient <code>NativeCommandOutput</code> bean.
   *
   * @param workingDirectory
   *            the working directory to execute in
   * @param commandLine
   *            an array containing the command to execute, followed by any
   *            command line arguments to be passed
   * @param stdOut
   *            STDOUT will be appended to this object
   * @param stdErr
   *            STDERR will be appended to this object
   * @return the exit code of the application. This is application specific,
   *         but generally, a 0 means everything went ok.
   * @throws NativeCommandException
   *             if something went wrong during execution... the causes are
   *             many...
   */
  public static int executeNativeCommand(File workingDirectory, Appendable stdOut, Appendable stdErr,
                                         String... commandLine)
      throws NativeCommandException
  {
    return executeNativeCommand(workingDirectory, new HashMap<>(), stdOut, stdErr, commandLine);
  }
  
  public static int executeNativeCommand(File workingDirectory, Map<String, String> userEnv,
                                         Appendable stdOut, Appendable stdErr,
                                         String... commandLine)
      throws NativeCommandException
  {
    if (commandLine.length == 0)
    {
      throw new NativeCommandException(
          "No native command specified for execution!");
    }
    BufferedInputStream stdOutStr = null;
    BufferedInputStream stdErrStr = null;
    try
    {
      // Execute process
      logger.fine("Executing native process: "+Arrays.asList(commandLine)
          + "\nWorking directory: "+workingDirectory.getAbsolutePath());
      boolean canExecute = checkCanExecute(commandLine[0]);
      if (!canExecute)
      {
        throw new NativeCommandException(
            "The command: "+commandLine[0] + " did not have 'executable' "
            + "permissions, and an attempt to make it 'executable' failed");
      }
      //java 1.4
//      final Process ps = Runtime.getRuntime()
//          .exec(commandArgs, null, workingDirectory);
      
      //Java 1.5+
      ProcessBuilder pb = new ProcessBuilder(commandLine);
      Map<String, String> env = pb.environment();
      for (String key : userEnv.keySet())
      {
        env.put(key, userEnv.get(key));
      }
      pb.directory(workingDirectory);
      final Process ps = pb.start();

      stdOutStr = new BufferedInputStream(ps.getInputStream());
      stdErrStr = new BufferedInputStream(ps.getErrorStream());
      ProcessOutputMonitor outputMonitor = 
          new ProcessOutputMonitor(stdOutStr, stdOut, stdErrStr, stdErr);

      Thread streamMonitorThread = new Thread(outputMonitor);
      streamMonitorThread.setDaemon(true);
      streamMonitorThread.start();

      /*
       * We must wait here until outputMonitor signals that it is ready
       * (ie, that it has acquired it's necessary locks)
       */
      logger.fine("Waiting for process stream monitor to become \"ready\"");
      while (!outputMonitor.isReady())
      {
        // ok, hack, but it'll do... Shouldn't spend long here anyway
        try
        {
          Thread.sleep(40);
        }
        catch (InterruptedException e)
        {
          // Don't care about this
        }
      }

      // Halt this thread until the process has exited
      logger.fine("Waiting for process to complete...");
      Integer exitCode = null;
      while (exitCode == null)
      {
        try
        {
          exitCode = ps.waitFor();
        }
        catch (InterruptedException e)
        {
          // interrupted... so lets continue until the process exits
        }
      }

      /*
       * Indicate to the output monitor that the native process has
       * finished so we no longer need to retrieve it's output.
       */
      outputMonitor.setDie();

      /*
       * Wait for the output monitor thead to confirm that it's now dead.
       * Otherwise we could lose output(?)
       */
      while (!outputMonitor.getDead())
      {
        try
        {
          Thread.sleep(20);
        }
        catch (InterruptedException e)
        {
          // Don't care about this
        }
      }
      logger.fine("Finished. Exit code was: "+exitCode);
      return exitCode;
    }
    catch (IOException e)
    {
      throw new NativeCommandException(
          "Exception running native command: " + commandLine[0], e);
    }
    finally
    {
      IOException failure = FileUtils.closeInputStreams(stdOutStr, stdErrStr);
      if (failure != null)
      {
        throw new NativeCommandException(
            "Failed to close stream(s) STDOUT, STDERR", failure);
      }
    }
  }
  
  private static boolean checkCanExecute(String pathToExe)
      throws NativeCommandException
  {
    File exe = new File(pathToExe);
    if (!exe.isAbsolute())
    {
      //Can't test executability, since we're not sure where the actual 
      //file is. Assume that it is (since it'll be on the PATH)
      return true;
    }
    else if (!exe.exists())
    {
      throw new NativeCommandException("No such file: "+exe.getAbsolutePath());
    }
    else if (!exe.isFile())
    {
      throw new NativeCommandException("Path: "+exe.getAbsolutePath() +
          " exists, but it is not a file, and therefore not executable.");
    }
    

    try
    {
      if (exe.isFile() && !exe.canExecute())
      {
        boolean ownerOnly = true;
        exe.setExecutable(true, ownerOnly);
      }
    }
    catch(SecurityException e)
    {
      logger.severe("Failed to set executable permission on: "+pathToExe);
    }
    finally
    {
      return exe.canExecute();
    }
  }
}

class ProcessOutputMonitor
    implements Runnable
{
  private final StreamMonitor stdOutMon;

  private final StreamMonitor stdErrMon;

  private int pollTime = 50;

  public ProcessOutputMonitor(InputStream stdOut, Appendable stdOutOutput,
      InputStream stdErr, Appendable stdErrOutput)
  {
    stdOutMon = new StreamMonitor(stdOut, stdOutOutput);
    stdErrMon = new StreamMonitor(stdErr, stdErrOutput);
  }

  //Instruction FROM this thread that we're ready
  private boolean ready = false;

  //Instruction TO this thread that we should finish
  private boolean die = false;

  //Instruction FROM this thread that we're finished successfully
  private boolean dead = false;

  public void run()
  {
    try
    {
      /*
       * Synchronised to ensure we don't read the recored stream output
       * before we've finished recording the final block(s)
       */
      synchronized (stdOutMon)
      {
        synchronized (stdErrMon)
        {
          // Notify anyone who was waiting for us to "get ready"
          // that we've now aquired necessary locks
          setReady(true);
          do
          {
            try
            {
              Thread.sleep(pollTime);
            }
            catch (InterruptedException e)
            {
              // This doesn't matter
            }
            checkStreams();
          } while (!getDie());
          // Process should have ended by now, but check anyway
          checkStreams();
        }
      }
      setDead();
    }
    catch (IOException e)
    {
      throw new RuntimeException(
          "Error checking the streams of a native process");
    }
  }

  private void checkStreams() throws IOException
  {
    stdOutMon.checkStream();
    stdErrMon.checkStream();
  }

  /**
   * Indicates that the process has died, so there's no need to continue
   * polling the output streams. Synchronised to ensure we don't modify this
   * concurrently with run() reading it
   */
  public synchronized void setDie()
  {
    die = true;
  }

  public synchronized boolean getDie()
  {
    return die;
  }

  public synchronized void setDead()
  {
    dead = true;
  }

  public synchronized boolean getDead()
  {
    return dead;
  }

  public synchronized boolean isReady()
  {
    return ready;
  }

  public synchronized void setReady(boolean ready)
  {
    this.ready = ready;
  }
}
