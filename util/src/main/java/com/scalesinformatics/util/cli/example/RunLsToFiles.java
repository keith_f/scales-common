/*
 * This program may only be used, distributed and/or modified under the terms 
 * of the license found in LICENSE.TXT in the project's root directory.
 * 
 * Copyright 2007 jointly held by the authors listed at the top of each
 * source file and/or their respective employers.
 */

package com.scalesinformatics.util.cli.example;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import com.scalesinformatics.util.cli.NativeCommandException;
import com.scalesinformatics.util.cli.NativeCommandExecutor;

/**
 * A test / tutorial showing how to run a simple native Unix command from
 * within Java. 
 * Although this is a simple example, running most commands isn't any more 
 * complex because there are only 2 output streams to deal with: STDOUT and 
 * STDERR. In this example, these two streams are written to files.
 * 
 * @author Keith Flanagan
 */
public class RunLsToFiles 
{
  public static void main(String args[])
      throws NativeCommandException, IOException
  {
    /*
     * The first task is to decide where the output of the program is going to
     * go. This can be any implementation of the 
     * <code>java.lang.Appendable</code> interface. In this case, we use 
     * <code>StringBuilder</code> because the output of the "ls" command is
     * small. You could use <code>java.io.FileWriter</code> instead for 
     * larger output - that would allow STDOUT and STDERR to be streamed to
     * disk.
     */
    File stdOutFile = new File("stdout.txt");
    File stdErrFile = new File("stderr.txt");
    FileWriter stdOut = new FileWriter(stdOutFile);
    FileWriter stdErr = new FileWriter(stdErrFile);
    
    //The directory that the command will be executed in
    File workingDirectory = new File(".");
    
    /*
     * The command line we want to execute. The first entry in the array is
     * required. It is the program name to run - this must either be located 
     * in a directory listed in the environment variable PATH, or you can 
     * specify an absolute pathname, e.g.: "ls" vs "/bin/ls".
     * 
     * Other array elements are optional - these are command line arguments
     * that will be passed to the native program.
     */
    String[] commandLine = new String[] { "ls", "-lh" };
    
    /* Finally, run the specified program.
     * The content of exitStatus will depend on the program, but generally
     * a value of 0 means "everything went ok".
     * The current thread will wait here until "ls" has finished executing.
     */
    int exitStatus = NativeCommandExecutor.executeNativeCommand(
        workingDirectory, stdOut, stdErr, commandLine);
    
    /*
     * At this point the native command has terminated.
     * However, we still have two open streams that need to be closed.
     */
    stdOut.flush();
    stdOut.close();
    
    stdErr.flush();
    stdErr.close();
    
    // All done.
    
    System.out.println("Finished executing command: "+commandLine[0]);
    System.out.println("Exit status was: "+exitStatus);
    System.out.println("STDOUT was written to: "+stdOutFile.getAbsolutePath());
    System.out.println("STDERR was written to: "+stdErrFile.getAbsolutePath());
  }
}
