package com.scalesinformatics.util.cli;

/**
 * 
 * @author Keith Flanagan
 */
public class OSUtils
{

  /**
   * A hacky way to determine which OS/arch we're running.
   * 
   * @return an element of the enum "OperatingSystem". If the current OS is
   *         "unsupported", we return UNSUPPORTED here.
   */
  public static OperatingSystem determineOperatingSystem()
  {
    String osName = System.getProperty("os.name");
    String version = System.getProperty("os.version");
    String arch = System.getProperty("os.arch");
    System.out.println("OS Name: " + osName);
    System.out.println("OS Version: " + version);
    System.out.println("OS Architecture: " + arch);
    if ((osName.contains("Linux")) && (arch.contains("i386")))
    {
      return OperatingSystem.LINUX_X86;
    }
    else if ((osName.contains("Linux")) && (arch.contains("amd64")))
    {
      return OperatingSystem.LINUX_AMD64;
    }
    else if ((osName.contains("Windows")) && (arch.contains("x86")))
    {
      return OperatingSystem.WINDOWS_X86;
    }
    else
    {
      return OperatingSystem.UNSUPPORTED;
    }
  }

  public enum OperatingSystem
  {
    LINUX_X86, LINUX_AMD64, WINDOWS_X86, UNSUPPORTED;
  }
}
