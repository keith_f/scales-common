package com.scalesinformatics.util.cli;

import java.io.IOException;
import java.io.InputStream;

/**
 * This class connects an input stream to an output stream. On calling
 * <code>checkStream</code>, the input stream is checked for any available
 * input. If there is, it's transferred to the output stream.
 *
* @author Keith Flanagan
 */
public class StreamMonitor
{
  private static final int BLOCK_SIZE = 8192;
  private final InputStream inStream;

  private final Appendable output;

  /**
   *
   * @param is
   *            the stream of incoming bytes
   * @param output
   *            the stream/storage of outgoing bytes
   */
  public StreamMonitor(InputStream is, Appendable output)
  {
    this.inStream = is;
    this.output = output;
  }

  /**
   * Checks the monitored InputStream for new content. If there are bytes
   * available, then they are transferred to the output Appendable.
   * 
   * @throws java.io.IOException
   */
  public void checkStream() 
      throws IOException
  {
    byte[] tmpBuffer = new byte[BLOCK_SIZE];
    int available;
    //Read from the stream, and write to the output stream
    while ((available = inStream.available()) > 0)
    {
      //System.out.println(available + " bytes in the buffer ready to copy");
      int toRead = available < BLOCK_SIZE ? available : BLOCK_SIZE;
      //System.out.println("Going to read: "+toRead);
      inStream.read(tmpBuffer, 0, toRead);
      output.append(new String(tmpBuffer, 0, toRead));
    }
  }

  public InputStream getInStream()
  {
    return inStream;
  }

  public Appendable getOutput()
  {
    return output;
  }
}
