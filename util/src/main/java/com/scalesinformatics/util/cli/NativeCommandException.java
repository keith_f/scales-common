package com.scalesinformatics.util.cli;

/**
 * @author Keith Flanagan
 */
public class NativeCommandException extends Exception
{

  public NativeCommandException()
  {
  }

  public NativeCommandException(String message)
  {
    super(message);
  }

  public NativeCommandException(Throwable cause)
  {
    super(cause);
  }

  public NativeCommandException(String message, Throwable cause)
  {
    super(message, cause);
  }

}
