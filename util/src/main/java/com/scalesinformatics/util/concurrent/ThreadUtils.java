/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.scalesinformatics.util.concurrent;

/**
 *
 * @author Keith Flanagan
 */
public class ThreadUtils
{
  public static void sleep(long millis)
  {
    long start = System.currentTimeMillis();
    long duration = 0;
    do
    {
      try
      {
        Thread.sleep(5);
      }
      catch(InterruptedException e)
      {
      }
      duration = System.currentTimeMillis() - start;
    } while (duration < millis);
  }

  public static void main(String[] args) {
    for (int i=0; i<100000; i++) {
      ThreadUtils.sleep(1000);
    }
  }
}
