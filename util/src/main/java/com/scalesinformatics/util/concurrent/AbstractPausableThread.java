/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Jan 11, 2012, 12:15:32 PM
 */

package com.scalesinformatics.util.concurrent;

/**
 * An extension of Thread that can be paused, resumed, killed.
 * If the thread is not currently paused or killed, then it will run at a 
 * specified frame rate (limited by going to sleep for a specified time).
 * However, certain events can wake the thread if necessary, allowing 
 * implementations to be flexible enough to cope with dynamically changing 
 * situations when necessary. For example, we might want a thread implementation
 * to update its state / redraw something every second, but then rapidly update
 * (e.g., every 20ms) when something important is happening. This abstract
 * class allows such implementations to be created with minimal effort.
 * 
 * 
 * @author Keith Flanagan
 */
abstract public class AbstractPausableThread
    extends Thread
{
  private static final long DEFAULT_WAKE_AT_LEAST_EVERY_MS = 1000;
  
  private boolean paused;
  private boolean killed;
  private long wakeAtLeastEveryMs;

  private long    previousTime;
  private long    currentTime;

  public AbstractPausableThread()
  {
    paused = true;
    wakeAtLeastEveryMs = DEFAULT_WAKE_AT_LEAST_EVERY_MS;
  }

  public AbstractPausableThread(String threadName)
  {
    super(threadName);
    paused = true;
    wakeAtLeastEveryMs = DEFAULT_WAKE_AT_LEAST_EVERY_MS;
  }
  
  
  /**
   * Pauses or resumes a Thread implementation. While paused, the Thread will
   * not perform any work, either through an external interrupt 
   * (<code>runNow</code>), or through the periodic wakeup events.
   * @param paused if <code>true</code>, then the thread will not perform any
   * more work until resumed by passing <code>false</code> to a future call to
   * this method.
   */
  public void setPaused(boolean paused)
  {
    this.paused = paused;
    synchronized (this)
    {
      this.notify();
    }
  }

  /**
   * Returns whether this Thread is currently paused.
   * 
   * @return <code>true</code> if the implementation is currently paused, 
   * otherwise returns <code>false</code>.
   */
  public boolean isPaused()
  {
    return paused;
  }

  /**
   * Stops the thread after the current execution has completed (if any).
   * After this method returns, the Thread will have ended and will be no
   * longer usable.
   */
  public void kill()
  {
    this.killed = true;
    synchronized (this)
    {
      this.notify();
    }
  }
  
  

  public long getWakeAtLeastEveryMs()
  {
    return wakeAtLeastEveryMs;
  }

  /**
   * Sets the rate at which this thread wakes itself to perform periodic
   * updates. The default is once every 1000ms. You can use this method to set
   * the rate dynamically.
   * Once called, this method is immediately woken. It then returns to the 
   * steady state of updates at a rate of <code>wakeAtLeastEveryMs</code>. 
   *  
   * @param wakeAtLeastEveryMs
   */
  public void setWakeAtLeastEveryMs(long wakeAtLeastEveryMs)
  {
    this.wakeAtLeastEveryMs = wakeAtLeastEveryMs;
    synchronized (this)
    {
      this.notify();
    }
  }
  
  /**
   * Forces the thread to wake immediately, if it is currently sleeping.
   * Call this method if you wish to force an update of the work performed
   * by <code>run()</code>.
   */
  public void runNow()
  {
    synchronized (this)
    {
      this.notify();
    }
  }

  @Override
  public void run()
  {
    this.killed = false;
    //Initial sleep time
    long threadWaitTimeMs = wakeAtLeastEveryMs; 
    while (!killed)
    {
      /*
       * If paused, go back to sleep, else do whatever work needs to be done.
       */
      if (!isPaused())
      {
        currentTime = System.currentTimeMillis();
        _doWork(currentTime, previousTime);
        previousTime = currentTime;
      } 

      // sleep if no need to run or to enforce a frame limit if necessary
      synchronized (this)
      {
        if (!killed)
        {
          try
          {
            wait(threadWaitTimeMs);
          } catch (Exception e)
          {
            e.printStackTrace();
          }
        }
      }
    } //end while(!killed)
  }
  
  abstract protected void _doWork(long currentRunTimeMs, long previousRunTimeMs);
}
