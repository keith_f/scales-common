/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.scalesinformatics.util;

import java.math.BigDecimal;
import java.math.MathContext;

/**
 * @author Keith Flanagan
 */
public interface Num<T extends Number> extends Comparable<Num<T>> {

  MathContext getContext();
  T getValue();

  NumFactory<T> getFactory();

  double toDouble();

  BigDecimal toBigDecimal();

  BigDecimal toBigDecimalScaled(int decimalPlaces);

  Num<T> toScale(int decimalPlaces);

  Num<T> add(Num<T> other);

  Num<T> subtract(Num<T> other);

  Num<T> multiply(Num<T> other);

  Num<T> divide(Num<T> other);

  Num<T> negate();



  boolean isLessThan(Num<T> other);

  boolean isGreaterThan(Num<T> other);

  boolean isGreaterThanOrEqual(Num<T> other);

  boolean isLessThanOrEqual(Num<T> other);

  boolean isEqualTo(Num<T> other);

  boolean isEqualTo(Num<T> other, int scale);

  Num min(Num<T> other);

  Num max(Num<T> other);


  Num<T> absRange(Num<T> other);

  Num<T> absMidPoint(Num<T> other);

  Num<T> addPercent(Num<T> percentDiff);


  /**
   * Calculates a percentage change going from <code>this</code> number to an<code>other</code> number.
   * If <code>a > b</code> then we calculate the percentage decrease.
   * If <code>b > a</code> then we calculate the percentage increase.
   *
   * See: http://www.calculatorsoup.com/calculators/algebra/percent-change-calculator.php
   *
   * Comparing Old to New: You had 5 books, but now have 7.
   * https://www.mathsisfun.com/numbers/percentage-change.html
   *
   * @param other the new value
   * @return the percentage increase or decrease from <codee>this</codee> --> <code>other</code>.
   * Notes:
   *   1) Returned percentage decreases are negative.
   *   2) Increases or decreases from zero are, of course, not supported.
   *   3) Values are returned without multiplying by 100. e.g: 0.1 == 10%
   */
  Num<T> calcPercentageChange(Num<T> other);


  /**
   * The difference between two values divided by the average of the two values. Shown as a percentage.
   * Percentage Difference is used when both values mean the same kind of thing (for example the heights of two people).
   *
   * Example: Alex sold 15 tickets, and Sam sold 25
   *
   * https://www.mathsisfun.com/percentage-difference.html
   *
   * The percentage difference is always positive.
   *
   * @param other
   * @return
   */
  Num<T> calcPercentageDifference(Num<T> other);

  Num<T> calcFoldChange(Num<T> other);
}
