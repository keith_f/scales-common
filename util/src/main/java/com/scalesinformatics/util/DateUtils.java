/*
 * Copyright 2013 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author Keith Flanagan
 */
public class DateUtils {
  private static final String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";
  private static final String DATEFORMAT_YMD = "yyyy-MM-dd";
  private static final String DATEFORMAT_YMDHMS = "yyyy-MM-dd HH:mm:ss";


  /**
   *
   * @param year
   * @param month (starts from 0)
   * @param day
   * @return
   */
  public static Date makeDate(int year, int month, int day) {
    Calendar cal = Calendar.getInstance();
    cal.clear();
    cal.set(Calendar.YEAR, year);
    cal.set(Calendar.MONTH, month);
    cal.set(Calendar.DAY_OF_MONTH, day);
    return cal.getTime();
  }

  /**
   *
   * @param year
   * @param month (starts from 0)
   * @param day
   * @param hour
   * @return
   */
  public static Date makeDate(int year, int month, int day, int hour) {
    Calendar cal = Calendar.getInstance();
    cal.clear();
    cal.set(Calendar.YEAR, year);
    cal.set(Calendar.MONTH, month);
    cal.set(Calendar.DAY_OF_MONTH, day);
    cal.set(Calendar.HOUR_OF_DAY, hour);
    return cal.getTime();
  }

  /**
   *
   * @param year
   * @param month (starts from 0)
   * @param day
   * @param hour
   * @param minute
   * @return
   */
  public static Date makeDate(int year, int month, int day, int hour, int minute) {
    Calendar cal = Calendar.getInstance();
    cal.clear();
    cal.set(Calendar.YEAR, year);
    cal.set(Calendar.MONTH, month);
    cal.set(Calendar.DAY_OF_MONTH, day);
    cal.set(Calendar.HOUR_OF_DAY, hour);
    cal.set(Calendar.MINUTE, minute);
    return cal.getTime();
  }

  /**
   * The way we used to format dates. Here for reference only.
   * @deprecated use <code>dateToUtcFormat</code> or <code>dateToUtcString</code>.
   * @param date
   * @return
   */
  public static String formatDateOld(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    return String.format("%d-%d-%d %d:%d:%d (%s)",
        cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DATE),
        cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND),
        date.toString()
    );
  }

  /**
   * Takes a <code>Date</code> and returns a <code>SimpleDateFormat</code> configured for the UTC timezone.
   *
   * @param date
   * @return
   */
  public static SimpleDateFormat dateToUtcFormat(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
    sdf.setCalendar(cal);
    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
    return sdf;
  }
  /**
   * Takes a <code>Date</code> and returns a <code>String</code> representation in UTC.
   *
   * @param date
   * @return
   */
  public static String dateToUtcString(Date date) {
    SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
    String utcString = sdf.format(date);
    return utcString;
  }

  /**
   * Prints a String of the specified Date in yyyy-mm-dd format (UTC timezone)
   * @param date
   * @return
   */
  public static String dateToUtcYearMonthDateString(Date date) {

    SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT_YMD);
    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
    String utcString = sdf.format(date);
    return utcString;
  }

  /**
   * Prints a String of the specified Date in yyyy-mm-dd hh:mm:ss format (UTC timezone)
   * @param date
   * @return
   */
  public static String dateToUtcYearMonthDateHourMinSecString(Date date) {

    SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT_YMDHMS);
    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
    String utcString = sdf.format(date);
    return utcString;
  }

  /**
   * Takes a <code>Date</code> and returns a <code>String</code> representation in Moscow time (UTC+4 (always?)).
   *
   * @param date
   * @return
   */
  public static String dateToMoscowTimeString(Date date) {
    SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
    sdf.setTimeZone(TimeZone.getTimeZone("Europe/Moscow"));
    String utcString = sdf.format(date);
    return utcString;
  }

  /**
   * Takes a <code>String</code> in the format specified by <code>DATEFORMAT</code> and parses it into
   * a <code>Date</code> object.
   *
   * @param dateString
   * @return
   */
  public static Date stringToDate(String dateString) throws ParseException {
    SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT);
    return dateFormat.parse(dateString);
  }

  public static Date stringYMDToDate(String dateString) throws ParseException {
    SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT_YMD);
    return dateFormat.parse(dateString);
  }

  public static String dateToStringInTimezone(Date date, String timeZone, boolean printTz) {
    SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
    TimeZone tz = TimeZone.getTimeZone(timeZone);
    sdf.setTimeZone(tz);
    if (printTz) {
      int hours = tz.getOffset(date.getTime()) / 1000 / 60 / 60;
      return sdf.format(date) + ((hours < 0) ? "" : "+") + hours;
    } else {
      return sdf.format(date);
    }
  }
}
