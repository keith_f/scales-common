/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.scalesinformatics.util;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * @author Keith Flanagan
 */
public interface NumFactory<T extends Number> {

  RoundingMode DEFAULT_ROUNDING_MODE = RoundingMode.HALF_EVEN;

  // Same as 'long'
  MathContext DEFAULT_CONTEXT = new MathContext(19, DEFAULT_ROUNDING_MODE);

  enum WellKnownNumber {
    ZERO, HALF, ONE, TWO, MAX_INT;
  }

  Num<T> make(String value);
  Num<T> make(String value, MathContext context);
//  Num<T> make(long value);
//  Num<T> make(long value, MathContext context);
  Num<T> make(BigDecimal value);
  Num<T> make(BigDecimal value, MathContext context);

  Num<T> make(Double value);
  Num<T> make(Double value, MathContext context);

  Num<T> makeRandomBetween(BigDecimal lowerLimit, BigDecimal upperLimit, MathContext context);

  Num<T> getWellKnownNumber(WellKnownNumber number);

  void putNumber(String name, Num<T> number);
  Num<T> getNumber(String name);
}

