/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.util;

import java.util.HashSet;
import java.util.ServiceLoader;
import java.util.Set;

/**
 * A utility for loading and finding SPI implementations.
 * For an example of use see the exchange SPIs, or trader SPI.
 * 
 * @author Keith Flanagan
 */
public class GenericServiceLoader<T>
{
  private final ClassLoader classLoader;
  private final Class<T> serviceType;
  
  public GenericServiceLoader(ClassLoader classLoader, Class<T> serviceType)
  {
    this.classLoader = classLoader;
    this.serviceType = serviceType;
  }
  
//  public GenericServiceLoader(Class<T> serviceType)
//  {
//    this.classLoader = GenericServiceLoader.class.getClassLoader();
//    this.serviceType = serviceType;
//  }
  
  public ServiceLoader<T> load()
  {
    ServiceLoader<T> loader = ServiceLoader.load(serviceType, classLoader);
    return loader;
  }
  
  
  public Set<T> loadAll()
  {
    ServiceLoader<T> loader = ServiceLoader.load(serviceType, classLoader);
    Set<T> impls = new HashSet<>();
    for (T impl : loader) {
      impls.add(impl);
    }
    return impls;
  }
  
  public T findByClassname(String classname)
  {
    ServiceLoader<T> loader = ServiceLoader.load(serviceType, classLoader);
    for (T impl : loader) {
      if (impl.getClass().getName().equals(classname)) {
        return impl;
      }
    }
    return null;
  }
  
  public T findBySimpleName(String simpleName)
  {
    ServiceLoader<T> loader = ServiceLoader.load(serviceType, classLoader);
    for (T impl : loader) {
      if (impl.getClass().getSimpleName().equals(simpleName)) {
        return impl;
      }
    }
    return null;
  }
  
  public T findByClassNameOrSimpleName(String simpleOrClassName)
  {
    T impl = findBySimpleName(simpleOrClassName);
    if (impl == null) {
      impl = findByClassname(simpleOrClassName);
    }
    return impl;
  }
}
