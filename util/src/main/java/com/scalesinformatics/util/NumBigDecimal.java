/*
 * Copyright (c) Scales Informatics Ltd 2013. All rights reserved. It is strictly prohibited to copy, redistribute,
 * republish or modify with work without the prior written consent of Scales Informatics Ltd.
 */

package com.scalesinformatics.util;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;

/**
 *
 * @author Keith Flanagan
 */
public class NumBigDecimal implements Num<BigDecimal>, Serializable {

  private final NumFactory<BigDecimal> factory;
  private final MathContext context;

  private final BigDecimal value;

  NumBigDecimal(NumFactory<BigDecimal> factory, BigDecimal value, MathContext context) {
    this.factory = factory;
    this.value = value;
    this.context = context;
  }

  @Override
  public String toString() {
    return value.toString();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    NumBigDecimal that = (NumBigDecimal) o;

    return value.equals(that.value);
  }

  @Override
  public int hashCode() {
    return value.hashCode();
  }

  @Override
  public MathContext getContext() {
    return context;
  }

  @Override
  public BigDecimal toBigDecimal() {
    return value;
  }

  @Override
  public BigDecimal toBigDecimalScaled(int decimalPlaces) {
    return value.setScale(decimalPlaces, context.getRoundingMode());
  }

  @Override
  public Num<BigDecimal> toScale(int decimalPlaces) {
    return factory.make(value.setScale(decimalPlaces, context.getRoundingMode()), context);
  }

  @Override
  public Num<BigDecimal> add(Num<BigDecimal> other) {
    return factory.make(
        value.add(other.getValue(), context),
        context);
  }

  @Override
  public Num<BigDecimal> subtract(Num<BigDecimal> other) {
    return factory.make(subtract(other.getValue()), context);
  }

  @Override
  public Num<BigDecimal> multiply(Num<BigDecimal> other) {
    return factory.make(multiply(other.getValue()), context);
  }

  @Override
  public Num<BigDecimal> divide(Num<BigDecimal> other) {
    return factory.make(divide(other.getValue()), context);
  }

  @Override
  public Num<BigDecimal> negate() {
    return factory.make(value.negate(), context);
  }



  private BigDecimal add(BigDecimal other) {
    return value.add(other, context);
  }

  private BigDecimal subtract(BigDecimal other) {
    return value.subtract(other, context);
  }

  private BigDecimal multiply(BigDecimal other) {
    return value.multiply(other, context);
  }

  private BigDecimal divide(BigDecimal other) {
    return value.divide(other, context);
  }



  @Override
  public boolean isLessThan(Num<BigDecimal> other) {
    return value.compareTo(other.getValue()) == -1;
  }

  @Override
  public boolean isGreaterThan(Num<BigDecimal> other) {
    return value.compareTo(other.getValue()) == 1;
  }

  @Override
  public boolean isGreaterThanOrEqual(Num<BigDecimal> other) {
    int result = value.compareTo(other.getValue());
    return result == 0 || result == 1;
  }

  @Override
  public boolean isLessThanOrEqual(Num<BigDecimal> other) {
    int result = value.compareTo(other.getValue());
    return result == 0 || result == -1;
  }

  @Override
  public boolean isEqualTo(Num<BigDecimal> other) {
    return value.compareTo(other.getValue()) == 0;
  }

  @Override
  public boolean isEqualTo(Num<BigDecimal> other, int scale) {
    return value.setScale(scale, context.getRoundingMode()).compareTo(other.getValue().setScale(scale, context.getRoundingMode())) == 0;
  }

  @Override
  public Num min(Num<BigDecimal> other) {
    return isLessThanOrEqual(other) ? this : other;
  }

  @Override
  public Num max(Num<BigDecimal> other) {
    return isGreaterThanOrEqual(other) ? this : other;
  }

  @Override
  public Num<BigDecimal> absRange(Num<BigDecimal> other) {
    BigDecimal max = value.max(other.getValue());
    BigDecimal min = value.min(other.getValue());

    BigDecimal range = max.subtract(min, context);
    return factory.make(range, context);
  }

  @Override
  public Num<BigDecimal> absMidPoint(Num<BigDecimal> other) {
    BigDecimal max = value.max(other.getValue());
    BigDecimal min = value.min(other.getValue());
    BigDecimal range = max.subtract(min, context);

    BigDecimal midPoint = range.multiply(factory.getWellKnownNumber(NumFactory.WellKnownNumber.HALF).getValue(), context).add(min, context);
    return factory.make(midPoint, context);
  }

  @Override
  public Num<BigDecimal> addPercent(Num<BigDecimal> percentDiff) {
    BigDecimal diff = value.multiply(percentDiff.getValue(), context);
    return factory.make(value.add(diff, context), context);
  }

  /**
   * Calculates a percentage change going from <code>this</code> number to an<code>other</code> number.
   * If <code>a > b</code> then we calculate the percentage decrease.
   * If <code>b > a</code> then we calculate the percentage increase.
   *
   * See: http://www.calculatorsoup.com/calculators/algebra/percent-change-calculator.php
   *
   * @param other the new value
   * @return the percentage increase or decrease from <codee>this</codee> --> <code>other</code>.
   * Notes:
   *   1) Returned percentage decreases are negative.
   *   2) Increases or decreases from zero are, of course, not supported.
   *   3) Values are returned without multiplying by 100. e.g: 0.1 == 10%
   */
  @Override
  public Num<BigDecimal> calcPercentageChange(Num<BigDecimal> other) {
    //c = (a > b) ? (a-b)/a*-100 : (b-a)/a*100;
    BigDecimal from = this.value;
    BigDecimal to = other.getValue();
    return (from.compareTo(to) > 0)
        ? factory.make(from.subtract(to).divide(from, context).negate(), context)
        : factory.make(to.subtract(from).divide(from, context), context);
//    return (from.compareTo(to) > 0)
//        ? from.subtract(to).divide(from, context).multiply(Num.ONE_HUNDRED.negate())
//        : to.subtract(from).divide(from, context).multiply(Num.ONE_HUNDRED);
  }


  @Override
  public Num<BigDecimal> calcPercentageDifference(Num<BigDecimal> other) {
    BigDecimal v1 = this.value;
    BigDecimal v2 = other.getValue();
    BigDecimal absDiff = v1.subtract(v2, context).abs();
    BigDecimal halfSum = v1.add(v2, context).multiply(factory.getWellKnownNumber(NumFactory.WellKnownNumber.HALF).getValue(), context);
    BigDecimal result = absDiff.divide(halfSum, context);

    return factory.make(result, context);                           // Return 0.1 = 10%
    //return new Num(result.multiply(ONE_HUNDRED, context));   // Return 0.1 = 0.1%
  }

  @Override
  public BigDecimal getValue() {
    return value;
  }

  @Override
  public NumFactory<BigDecimal> getFactory() {
    return factory;
  }

  @Override
  public double toDouble() {
    return value.doubleValue();
  }

  public static double calcFoldChange(double a, double b) {
    // TODO move this elsewhere as a convenience method
    a = a + 1e-200;
    b = b + 1e-200;
    return (b - a) / a;
  }

  @Override
  public Num calcFoldChange(Num other) {
    // TODO move this elsewhere as a convenience method
    Num verySmall = factory.make("1e-200");
    Num a = add(verySmall);
    Num b = other.add(verySmall);
    return b.subtract(a).divide(a);
  }



  /**
   * TODO move this elsewhere as a convenience method (with or without the *100)
   * See: http://www.calculatorsoup.com/calculators/algebra/percent-change-calculator.php
   * @param from
   * @param to
   * @return
   */
  public static double calcPercentageChange(double from, double to) {
    double percentChange = Math.abs(((from - to) / from) * 100d);
    if (to < from) {
      percentChange = -percentChange;
    }
    return percentChange;
  }


  /**
   * TODO move this elsewhere as a convenience method (with or without the *100)
   * Calculates the percentage difference between v1 and v2.
   *
   * http://www.calculatorsoup.com/calculators/algebra/percent-difference-calculator.php
   *
   * @param v1
   * @param v2
   * @return
   */
  public static double calcPercentageDifference(double v1, double v2) {
    double absDiff = Math.abs(v1 - v2);
    double halfSum = (v1 + v2) / 2d;
    return (absDiff / halfSum) * 100d;
  }

  @Override
  public int compareTo(Num<BigDecimal> o) {
    return value.compareTo(o.getValue());
  }


//  public static void main(String[] args) {
//    System.out.println(calcPercentageChange(5, 10));
//    System.out.println(calcPercentageChange(10, 5));
//
//    System.out.println(calcPercentageChange(create(5), create(10)));
//    System.out.println(calcPercentageChange(create(10), create(5)));
//
//    System.out.println(calcPercentageChange(11, 13));
//    System.out.println(calcPercentageChange(13, 11));
//
//    System.out.println(calcPercentageChange(-10, -5));
//    System.out.println(calcPercentageChange(-5, 10));
//
//    System.out.println(calcPercentageChange(-5, 5));
//
//    System.out.println(calcPercentageChange(5, -5));
//
//
//    System.out.println("\n\n----\n\n");
//
//    System.out.println(TradeCalculatorDbl.addPercent(10, 0.4));
//    System.out.println(TradeCalculatorDbl.addPercent(10, -0.4));
//    System.out.println(TradeCalculatorDbl.calculateNetProfitBetweenPrices(10, TradeCalculatorDbl.addPercent(10, -0.4), 0.002));
//    System.out.println(TradeCalculatorDbl.calculateNetProfitBetweenPrices(10, TradeCalculatorDbl.addPercent(10, -0.8), 0.002));
//
//    System.out.println(calcPercentageDifference(10, TradeCalculatorDbl.addPercent(10, -0.4)));
//
//
//    System.out.println("\n\n----\n\n");
//
//    System.out.println(calcPercentageDifference(5, 10));
//    System.out.println(calcPercentageDifference(10, 5));
//
//    System.out.println(calcPercentageDifference(create(5), create(10)));
//    System.out.println(calcPercentageDifference(create(10), create(5)));
//  }
}
