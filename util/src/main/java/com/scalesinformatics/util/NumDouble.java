/*
 * Copyright (c) Scales Informatics Ltd 2013. All rights reserved. It is strictly prohibited to copy, redistribute,
 * republish or modify with work without the prior written consent of Scales Informatics Ltd.
 */

package com.scalesinformatics.util;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;

/**
 *
 * @author Keith Flanagan
 */
public class NumDouble implements Num<Double>, Serializable {

  private final NumFactory<Double> factory;
  private final MathContext context;

  private final Double value;

//  NumDouble(NumFactory<Double> factory, BigDecimal value, MathContext context) {
//    this.factory = factory;
//    this.value = value;
//    this.context = context;
//  }

  NumDouble(NumFactory<Double> factory, Double value, MathContext context) {
    this.factory = factory;
    this.value = value;
    this.context = context;
  }


  @Override
  public String toString() {
    return value.toString();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    NumDouble that = (NumDouble) o;

    return value.equals(that.value);
  }

  @Override
  public int hashCode() {
    return value.hashCode();
  }

  @Override
  public MathContext getContext() {
    return context;
  }

  @Override
  public BigDecimal toBigDecimal() {
    return new BigDecimal(value, context);
  }

  @Override
  public BigDecimal toBigDecimalScaled(int decimalPlaces) {
    return toBigDecimal().setScale(decimalPlaces, context.getRoundingMode());
  }

  @Override
  public Num<Double> toScale(int decimalPlaces) {
    return factory.make(new BigDecimal(value, context).setScale(decimalPlaces, context.getRoundingMode()));
  }

  @Override
  public Num<Double> add(Num<Double> other) {
    return factory.make(value + other.getValue(), context);
  }

  @Override
  public Num<Double> subtract(Num<Double> other) {
    return factory.make(value - other.getValue(), context);
  }

  @Override
  public Num<Double> multiply(Num<Double> other) {
    return factory.make(value * other.getValue(), context);
  }

  @Override
  public Num<Double> divide(Num<Double> other) {
    return factory.make(value / other.getValue(), context);
  }

  @Override
  public Num<Double> negate() {
    return factory.make(-value, context);
  }

  @Override
  public boolean isLessThan(Num<Double> other) {
    return value.compareTo(other.getValue()) == -1;
  }

  @Override
  public boolean isGreaterThan(Num<Double> other) {
    return value.compareTo(other.getValue()) == 1;
  }

  @Override
  public boolean isGreaterThanOrEqual(Num<Double> other) {
    int result = value.compareTo(other.getValue());
    return result == 0 || result == 1;
  }

  @Override
  public boolean isLessThanOrEqual(Num<Double> other) {
    int result = value.compareTo(other.getValue());
    return result == 0 || result == -1;
  }

  @Override
  public boolean isEqualTo(Num<Double> other) {
    return value.compareTo(other.getValue()) == 0;
  }

  @Override
  public boolean isEqualTo(Num<Double> other, int scale) {
    return toBigDecimalScaled(scale).compareTo(other.toBigDecimalScaled(scale)) == 0;
  }

  @Override
  public Num min(Num<Double> other) {
    return isLessThanOrEqual(other) ? this : other;
  }

  @Override
  public Num max(Num<Double> other) {
    return isGreaterThanOrEqual(other) ? this : other;
  }

  @Override
  public Num<Double> absRange(Num<Double> other) {
    Double max = Double.max(value, other.getValue());
    Double min = Double.min(value, other.getValue());

    Double range = max - min;
    return factory.make(range, context);
  }

  @Override
  public Num<Double> absMidPoint(Num<Double> other) {
    Double max = Double.max(value, other.getValue());
    Double min = Double.min(value, other.getValue());
    Double range = max - min;

    Double midPoint = (range * 0.5d) + min;
    return factory.make(midPoint, context);
  }

  @Override
  public Num<Double> addPercent(Num<Double> percentDiff) {
    Double diff = value * percentDiff.getValue();
    return factory.make(value + diff, context);
  }

  /**
   * Calculates a percentage change going from <code>this</code> number to an<code>other</code> number.
   * If <code>a > b</code> then we calculate the percentage decrease.
   * If <code>b > a</code> then we calculate the percentage increase.
   *
   * See: http://www.calculatorsoup.com/calculators/algebra/percent-change-calculator.php
   *
   * @param other the new value
   * @return the percentage increase or decrease from <codee>this</codee> --> <code>other</code>.
   * Notes:
   *   1) Returned percentage decreases are negative.
   *   2) Increases or decreases from zero are, of course, not supported.
   *   3) Values are returned without multiplying by 100. e.g: 0.1 == 10%
   */
  @Override
  public Num<Double> calcPercentageChange(Num<Double> other) {
    //c = (a > b) ? (a-b)/a*-100 : (b-a)/a*100;
    Double from = this.value;
    Double to = other.getValue();
    return (from.compareTo(to) > 0)
        ? factory.make( (from - to) / -from, context) //from.subtract(to).divide(from, context).negate(), context)
        : factory.make( (to - from) / from, context); //to.subtract(from).divide(from, context), context);
  }


  @Override
  public Num<Double> calcPercentageDifference(Num<Double> other) {
    Double v1 = this.value;
    Double v2 = other.getValue();
    Double absDiff = Math.abs(v1 - v2); // v1.subtract(v2, context).abs();
    Double halfSum = (v1 + v2) * 0.5; // v1.add(v2, context).multiply(factory.getWellKnownNumber(NumFactory.WellKnownNumber.HALF).getValue(), context);
    Double result = absDiff / halfSum; //absDiff.divide(halfSum, context);

    return factory.make(result, context);                           // Return 0.1 = 10%
    //return new Num(result.multiply(ONE_HUNDRED, context));   // Return 0.1 = 0.1%
  }

  @Override
  public Double getValue() {
    return value;
  }

  @Override
  public NumFactory<Double> getFactory() {
    return factory;
  }

  @Override
  public double toDouble() {
    return value;
  }

  public static double calcFoldChange(double a, double b) {
    // TODO move this elsewhere as a convenience method
    a = a + 1e-200;
    b = b + 1e-200;
    return (b - a) / a;
  }

  @Override
  public Num calcFoldChange(Num other) {
    // TODO move this elsewhere as a convenience method
    Num verySmall = factory.make("1e-200");
    Num a = add(verySmall);
    Num b = other.add(verySmall);
    return b.subtract(a).divide(a);
  }



  /**
   * TODO move this elsewhere as a convenience method (with or without the *100)
   * See: http://www.calculatorsoup.com/calculators/algebra/percent-change-calculator.php
   * @param from
   * @param to
   * @return
   */
  public static double calcPercentageChange(double from, double to) {
    double percentChange = Math.abs(((from - to) / from) * 100d);
    if (to < from) {
      percentChange = -percentChange;
    }
    return percentChange;
  }


  /**
   * TODO move this elsewhere as a convenience method (with or without the *100)
   * Calculates the percentage difference between v1 and v2.
   *
   * http://www.calculatorsoup.com/calculators/algebra/percent-difference-calculator.php
   *
   * @param v1
   * @param v2
   * @return
   */
  public static double calcPercentageDifference(double v1, double v2) {
    double absDiff = Math.abs(v1 - v2);
    double halfSum = (v1 + v2) / 2d;
    return (absDiff / halfSum) * 100d;
  }

  @Override
  public int compareTo(Num<Double> o) {
    return value.compareTo(o.getValue());
  }


//  public static void main(String[] args) {
//    System.out.println(calcPercentageChange(5, 10));
//    System.out.println(calcPercentageChange(10, 5));
//
//    System.out.println(calcPercentageChange(create(5), create(10)));
//    System.out.println(calcPercentageChange(create(10), create(5)));
//
//    System.out.println(calcPercentageChange(11, 13));
//    System.out.println(calcPercentageChange(13, 11));
//
//    System.out.println(calcPercentageChange(-10, -5));
//    System.out.println(calcPercentageChange(-5, 10));
//
//    System.out.println(calcPercentageChange(-5, 5));
//
//    System.out.println(calcPercentageChange(5, -5));
//
//
//    System.out.println("\n\n----\n\n");
//
//    System.out.println(TradeCalculatorDbl.addPercent(10, 0.4));
//    System.out.println(TradeCalculatorDbl.addPercent(10, -0.4));
//    System.out.println(TradeCalculatorDbl.calculateNetProfitBetweenPrices(10, TradeCalculatorDbl.addPercent(10, -0.4), 0.002));
//    System.out.println(TradeCalculatorDbl.calculateNetProfitBetweenPrices(10, TradeCalculatorDbl.addPercent(10, -0.8), 0.002));
//
//    System.out.println(calcPercentageDifference(10, TradeCalculatorDbl.addPercent(10, -0.4)));
//
//
//    System.out.println("\n\n----\n\n");
//
//    System.out.println(calcPercentageDifference(5, 10));
//    System.out.println(calcPercentageDifference(10, 5));
//
//    System.out.println(calcPercentageDifference(create(5), create(10)));
//    System.out.println(calcPercentageDifference(create(10), create(5)));
//  }
}
