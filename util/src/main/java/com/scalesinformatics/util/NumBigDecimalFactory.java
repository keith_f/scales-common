/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.scalesinformatics.util;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Keith Flanagan
 */
public class NumBigDecimalFactory implements NumFactory<BigDecimal> {
  private final NumBigDecimal ZERO;
  private final NumBigDecimal HALF;
  private final NumBigDecimal ONE;
  private final NumBigDecimal TWO;
  private final NumBigDecimal MAX_INT;

  private final MathContext context;

  private final Map<String, Num<BigDecimal>> cache;

  public NumBigDecimalFactory() {
    this(DEFAULT_CONTEXT);
  }

  public NumBigDecimalFactory(MathContext context) {
    this.context = context;
    this.cache = new HashMap<>();

    ZERO = new NumBigDecimal(this, new BigDecimal("0", context), context);
    HALF = new NumBigDecimal(this, new BigDecimal("0.5", context), context);
    ONE = new NumBigDecimal(this, new BigDecimal("1", context), context);
    TWO = new NumBigDecimal(this, new BigDecimal("2", context), context);
    MAX_INT = new NumBigDecimal(this, new BigDecimal(Integer.MAX_VALUE, context), context);
  }

  @Override
  public Num<BigDecimal> make(String value) {
    return new NumBigDecimal(this, new BigDecimal(value, context), context);
  }

  @Override
  public Num<BigDecimal> make(String value, MathContext context) {
    return new NumBigDecimal(this, new BigDecimal(value, context), context);
  }

  @Override
  public Num<BigDecimal> make(BigDecimal value) {
    return new NumBigDecimal(this, value, context);
//    return new NumBigDecimal(this, value, context);
  }

  @Override
  public Num<BigDecimal> make(BigDecimal value, MathContext context) {
    return new NumBigDecimal(this, value, context);
//    return new NumBigDecimal(this, value, context);
  }

  @Override
  public Num<BigDecimal> make(Double value) {
    return new NumBigDecimal(this, new BigDecimal(value, context), context);
  }

  @Override
  public Num<BigDecimal> make(Double value, MathContext context) {
    return new NumBigDecimal(this, new BigDecimal(value, context), context);
  }

  @Override
  public Num<BigDecimal> makeRandomBetween(BigDecimal lowerLimit, BigDecimal upperLimit, MathContext context) {
    BigDecimal rnd = new BigDecimal(Math.random(), context);
    BigDecimal range = upperLimit.subtract(lowerLimit, context);
    BigDecimal value = rnd.multiply(range, context).add(lowerLimit, context);

    return new NumBigDecimal(this, value, context);
  }

  @Override
  public Num<BigDecimal> getWellKnownNumber(WellKnownNumber number) {
    switch (number) {
      case ZERO:
        return ZERO;
      case HALF:
        return HALF;
      case ONE:
        return ONE;
      case TWO:
        return TWO;
      case MAX_INT:
        return MAX_INT;
      default:
        throw new IllegalArgumentException("Number not yet supported: "+number.name());
    }
  }

  @Override
  public void putNumber(String name, Num<BigDecimal> number) {
    cache.put(name, number);
  }

  @Override
  public Num<BigDecimal> getNumber(String name) {
    return cache.get(name);
  }

}
