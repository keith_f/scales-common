/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation. The full license may be found in
 * COPYING.LESSER in this project's root directory.
 * 
 * Copyright 2007 jointly held by the authors listed at the top of each
 * source file and/or their respective employers.
 */
package com.scalesinformatics.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.server.UID;
import java.util.UUID;

public class UidGenerator
{

  public static String generateSimpleUid()
  {
    return String.valueOf(System.currentTimeMillis()) + Math.random();
  }

  /**
   * Generates an id that is globally unique
   * @deprecated use generateUid instead
   * @return unique id
   */
  public static String generateUidOld()
  {
    UID uniqueId = new UID();
    InetAddress host = null;
    try
    {
      host = InetAddress.getLocalHost();
    }
    catch (UnknownHostException e)
    {
      e.printStackTrace();
    }
    String id;
    if (host == null)
    {
      id = uniqueId.toString() + Math.random();
      System.out.println("Generated id: " + id + " len: " + id.length());
      return id;
    }
    else
    {
      id = host.getHostAddress() + ":" + uniqueId.toString() + Math.random();
      System.out.println("Generated id: " + id + " len: " + id.length());
      return id;
    }
  }

  public static String generateUid()
  {
    UUID uuid = UUID.randomUUID();
    String idStr = uuid.toString().replace("-", "");
    //    	System.out.println(uuid.toString());
    //    	System.out.println(idStr);
    return idStr;

  }
  
  public static byte[] generateUidAsBytes()
  {
    return javax.xml.bind.DatatypeConverter.parseHexBinary(generateUid());
  }

  /**
   * Base64 hex encoder pasted from http://www.javapractices.com/Topic56.cjp
   * 
   * @param aInput
   * @return
   */
  public static String hexEncode(byte[] aInput)
  {
    StringBuffer result = new StringBuffer();
    char[] digits =
    { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd',
        'e', 'f' };
    for (int idx = 0; idx < aInput.length; ++idx)
    {
      byte b = aInput[idx];
      result.append(digits[(b & 0xf0) >> 4]);
      result.append(digits[b & 0x0f]);
    }
    return result.toString();
  }

  public static void main(String args[])
  {
    generateUid();
  }
}
