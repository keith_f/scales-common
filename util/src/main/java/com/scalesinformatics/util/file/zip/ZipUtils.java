/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation. The full license may be found in
 * COPYING.LESSER in this project's root directory.
 * 
 * Copyright 2007 jointly held by the authors listed at the top of each
 * source file and/or their respective employers.
 */
package com.scalesinformatics.util.file.zip;

import com.scalesinformatics.util.file.FileUtils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.jar.JarOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * 
 * @author Keith Flanagan
 */
public class ZipUtils
{
  private static final Logger logger = 
      Logger.getLogger(ZipUtils.class.getName());
  
  private static final int BUFFER_SIZE = 4096;

  /**
   * Unzips a zip stream to a specified directory location. Recursive
   * extraction (nested files/directories) is supported. The entire filesystem
   * contained within the Zip archive will be created using
   * <code>targetDir</code> as the root.
   * 
   * @param is
   *            the InputStream containing zipped content
   * @param targetDir
   *            the root directory to unzip to. Multiple files/directories may
   *            be extracted here.
   */
  public static void extractZip(InputStream is, File targetDir)
      throws IOException
  {
    logger.log(Level.INFO, "Unzipping from InputStream to: {0}", targetDir.getAbsolutePath());
    ZipInputStream zis = null;
    try
    {
      zis = new ZipInputStream(new BufferedInputStream(is));
      ZipEntry entry;
      while ((entry = zis.getNextEntry()) != null)
      {
        if (entry.isDirectory())
        {
          File dir = new File(targetDir, entry.getName());
          logger.log(Level.INFO, "Creating directory: {0}", dir.getAbsolutePath());
          dir.mkdirs();
          continue;
        }

        int read;
        byte data[] = new byte[BUFFER_SIZE];
        File targetFile = new File(targetDir, entry.getName());
        logger.log(Level.INFO, "Unzipping: {0} to {1}", 
            new Object[]{entry.getName(), targetFile.getAbsolutePath()});
        FileOutputStream fos = new FileOutputStream(targetFile);
        BufferedOutputStream dest = null;
        try
        {
          dest = new BufferedOutputStream(fos, BUFFER_SIZE);
          while ((read = zis.read(data, 0, BUFFER_SIZE)) != -1)
          {
            dest.write(data, 0, read);
          }
          dest.flush();
        }
        finally
        {
          dest.close();
        }
//        targetFile.setExecutable(true); //FIXME Ugly hack required for microbase job enactment!
      }
    }
    catch(Throwable e)
    {
      throw new IOException("failed to extract zip file: "+e.getMessage(), e);
    }
    finally
    {
      //IOException failureOs = FileUtils.closeOutputStreams(dest);
      IOException failure = FileUtils.closeInputStreams(zis);
      if (failure != null)
      {
        throw new IOException("Failed to close stream", failure);
      }
    }
  }
  
  
//  public static void extractZip(InputStream is, File targetDir)
//      throws IOException
//  {
//    ZipInputStream zis = null;
//    BufferedOutputStream dest = null;
//    try
//    {
//      zis = new ZipInputStream(new BufferedInputStream(is));
//      ZipEntry entry;
//      while ((entry = zis.getNextEntry()) != null)
//      {
//        if (entry.isDirectory())
//        {
//          File dir = new File(targetDir, entry.getName());
//          dir.mkdirs();
//          continue;
//        }
//
//        int read;
//        byte data[] = new byte[BUFFER_SIZE];
//        File targetFile = new File(targetDir, entry.getName());
//        FileOutputStream fos = new FileOutputStream(targetFile);
//        dest = new BufferedOutputStream(fos, BUFFER_SIZE);
//        while ((read = zis.read(data, 0, BUFFER_SIZE)) != -1)
//        {
//          dest.write(data, 0, read);
//        }
//        dest.flush();
//      }
//    }
//    finally
//    {
//      IOException failureOs = FileUtils.closeOutputStreams(dest);
//      IOException failureIs = FileUtils.closeInputStreams(zis);
//      if (failureIs != null)
//      {
//        throw new IOException("Failed to close stream", failureIs);
//      }
//      if (failureOs != null)
//      {
//        throw new IOException("Failed to close stream", failureOs);
//      }
//    }
//  }

  /**
   * Extracts a Gzipped stream to a file. GZip only supports a single "file"
   * (although this may in fact be an archive such as a Tape ARchive,
   * depending on whomever created it)
   * 
   * @param is
   *            the gzipped stream
   * @param targetFile
   *            the target file to uncompress to. If this file already exists,
   *            it will be overwritten
   * @throws IOException
   */
  public static void extractGzip(InputStream is, File targetFile)
      throws IOException
  {
    GZIPInputStream gzis = new GZIPInputStream(new BufferedInputStream(is));
    BufferedOutputStream dest = null;
    try
    {
      int read;
      byte data[] = new byte[BUFFER_SIZE];
      FileOutputStream fos = new FileOutputStream(targetFile);
      dest = new BufferedOutputStream(fos, BUFFER_SIZE);
      while ((read = gzis.read(data, 0, BUFFER_SIZE)) != -1)
      {
        dest.write(data, 0, read);
      }
    }
    finally
    {
      IOException failureOs = FileUtils.closeOutputStreams(dest);
      IOException failureIs = FileUtils.closeInputStreams(gzis);
      if (failureIs != null)
      {
        throw new IOException("Failed to close stream", failureIs);
      }
      if (failureOs != null)
      {
        throw new IOException("Failed to close stream", failureOs);
      }
    }
  }

  /**
   * Creates a zip archive from a set of input streams.
   * 
   * @param data
   *            a map of target filename -> input data
   * @return a byte array containing the zip archive
   * @throws IOException
   */
  public static byte[] createZipArchiveInRam(Map<String, InputStream> data)
      throws IOException
  {
    ByteArrayOutputStream bos = null;
    ZipOutputStream zos = null;
    try
    {
      bos = new ByteArrayOutputStream();
      zos = new ZipOutputStream(bos);
      byte[] buffer = new byte[BUFFER_SIZE];
      for (String entryName : data.keySet())
      {
        InputStream in = data.get(entryName);
        if (in == null)
        {
          throw new IOException("Input stream for entry: " 
              + entryName + " was null!");
        }

        zos.putNextEntry(new ZipEntry(entryName));

        int read;
        while ((read = in.read(buffer)) != -1)
        {
          zos.write(buffer, 0, read);
        }
        zos.closeEntry();
        in.close();
      }
      zos.flush();
      byte[] bytes = bos.toByteArray();
      return bytes;
    }
    finally
    {
      IOException failureOs = FileUtils.closeOutputStreams(zos, bos);
      if (failureOs != null)
      {
        throw new IOException("Failed to close stream", failureOs);
      }
    }
  }
  
  public static void gzip(File inFile, File outFile) 
      throws IOException
  {
    BufferedInputStream bis = null;
    BufferedOutputStream bos = null;
    try
    {
      bis = new BufferedInputStream(new FileInputStream(inFile));
      bos = new BufferedOutputStream(
          new GZIPOutputStream(new FileOutputStream(outFile)));
      byte[] buffer = new byte[BUFFER_SIZE];

      int read;
      while ((read = bis.read(buffer)) != -1)
      {
        bos.write(buffer, 0, read);
      }
    }
    finally
    {
      IOException failureOs = FileUtils.closeOutputStreams(bos);
      IOException failureIs = FileUtils.closeInputStreams(bis);
      if (failureIs != null)
      {
        throw new IOException("Failed to close stream", failureIs);
      }
      if (failureOs != null)
      {
        throw new IOException("Failed to close stream", failureOs);
      }
    }
  }

  /**
   * Creates a flat zip archive (directories not supported (?))
   * 
   * @param data
   *            a map of filename -> data
   * @param outZipFile
   *            a target file for the zip archive. If this exists already, it
   *            will be overwritten
   * @throws IOException
   */
  public static void createZipFile(Map<String, InputStream> data,
      File outZipFile) throws IOException
  {
    ZipOutputStream zos = null;
    try
    {
      zos = new ZipOutputStream(
        new BufferedOutputStream(new FileOutputStream(outZipFile)));
      byte[] buffer = new byte[BUFFER_SIZE];

      for (String entryName : data.keySet())
      {
        InputStream in = data.get(entryName);
        if (in == null)
        {
          throw new IOException("Input stream for entry: " 
              + entryName + " was null!");
        }

        zos.putNextEntry(new ZipEntry(entryName));

        int read;
        while ((read = in.read(buffer)) != -1)
        {
          zos.write(buffer, 0, read);
        }
        zos.closeEntry();
        in.close();
      }
    }
    finally
    {
      IOException failureOs = FileUtils.closeOutputStreams(zos);
      if (failureOs != null)
      {
        throw new IOException("Failed to close stream", failureOs);
      }
    }
  }

  /**
   * Creates a jar file from a series of input streams, each stream
   * representing a file.
   * 
   * @param data
   *            a map of filename -> stream containing its data.
   * @return
   * @throws IOException
   */
  public static byte[] createJar(Map<String, InputStream> data)
      throws IOException
  {
    ByteArrayOutputStream bos = null;
    JarOutputStream jos = null;
    try
    {
      bos = new ByteArrayOutputStream();
      jos = new JarOutputStream(bos);
      byte[] buffer = new byte[BUFFER_SIZE];
      for (String entryName : data.keySet())
      {
        InputStream in = data.get(entryName);
        if (in == null)
        {
          throw new IOException("Input stream for entry: " + entryName +
              " was null!");
        }

        jos.putNextEntry(new ZipEntry(entryName));

        int read;
        while ((read = in.read(buffer)) != -1)
        {
          jos.write(buffer, 0, read);
        }
        jos.closeEntry();
        in.close();
      }

      jos.flush();
      byte[] bytes = bos.toByteArray();
      return bytes;
    }
    finally
    {
      IOException failureOs = FileUtils.closeOutputStreams(jos, bos);
      if (failureOs != null)
      {
        throw new IOException("Failed to close stream", failureOs);
      }
    }
  }
}
