/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation. The full license may be found in
 * COPYING.LESSER in this project's root directory.
 * 
 * Copyright 2007 jointly held by the authors listed at the top of each
 * source file and/or their respective employers.
 */
package com.scalesinformatics.util.file;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class DirectoryUtils
{
  /**
   * For debug/test
   * 
   * @param args
   */
  public static void main(String[] args)
      throws Throwable
  {
    deleteDir(new File("/tmp/testdel"));
  }
  
  public static List<File> findDirectoriesWithName(File root, String name)
  {
    System.out.println("Searching from: "+root.getAbsolutePath());
    List<File> found = new ArrayList<File>();
    if (!root.isDirectory())
    {
      throw new RuntimeException("Expecting a directory");
    }
    
    Stack<File> dirStack = new Stack<File>();
    dirStack.push(root);

    while (!dirStack.isEmpty())
    {
      File curDir = dirStack.pop();

      File[] fileArray = curDir.listFiles();
      for (File file : fileArray)
      {
        if (file.isDirectory())
        {
          if (file.getName().equals(name))
          {
            found.add(file);
          }
          else
          {
            dirStack.push(file);  
          }
        }
      }
    }
    return found;
  }
  
  public static File findDirectoryWithName(File root, String name)
  {
    System.out.println("Searching from: "+root.getAbsolutePath());
    if (!root.isDirectory())
    {
      throw new RuntimeException("Expecting a directory");
    }
    
    Stack<File> dirStack = new Stack<File>();
    dirStack.push(root);

    while (!dirStack.isEmpty())
    {
      File curDir = dirStack.pop();

      File[] fileArray = curDir.listFiles();
      for (File file : fileArray)
      {
        if (file.isDirectory())
        {
          if (file.getName().equals(name))
          {
            return file;
          }
          else
          {
            dirStack.push(file);  
          }
        }
      }
    }
    return null;
  }
  
  
  
  public static File findDirWithMostFreeSpace()
  {
    long freeSpace = Long.MIN_VALUE;
    File best = null;
    for (File root : File.listRoots())
    {
      FreeSpaceFinder finder = new FreeSpaceFinder();
      finder.scanBiggestFreeSpace(root);
      File bestInRoot = finder.getBestDirectory();
      if (bestInRoot != null && bestInRoot.getUsableSpace() > freeSpace)
      {
        best = bestInRoot;
      }
    }
    return best;
  }

  /**
   * Deletes a file or directory, and all of its child files/directories (if any)
   * Source adapted from:
   * http://forum.java.sun.com/thread.jspa?threadID=563148&messageID=3415560
   * 
   * @param dir the directory to delete
   */
  public static void deleteDir(File fileOrDir)
      throws IOException
  {
    if (fileOrDir.isFile())
    {
      fileOrDir.delete();
      return;
    }
    
    Stack<File> dirStack = new Stack<File>();
    dirStack.push(fileOrDir);

    boolean containsSubFolder;
    while (!dirStack.isEmpty())
    {
      File currFileOrDir = dirStack.peek();
      containsSubFolder = false;

      File[] fileArray = currFileOrDir.listFiles();
      if (fileArray != null)
      {
        for (File file : fileArray)
        {
          if (file.isDirectory())
          {
            dirStack.push(file);
            containsSubFolder = true;
          } else
          {
            file.delete(); //delete file
          }
        }
      }


      if (!containsSubFolder)
      {
        dirStack.pop(); //remove curr dir from stack
        currFileOrDir.delete(); //delete curr dir
      }
    }
  }

  
  /**
   * Deletes a file/directory and all its files/subdirectories
   * 
   * @param dir
   * @return
   */
  public static boolean deleteDirsOld(File dir)
  {
    for (File file : dir.listFiles())
    {
      if (file.isDirectory())
      {
        deleteDirsOld(file);
      }
      else
      {
        file.delete();
      }
    }
    return (dir.delete());
  }
}



class FreeSpaceFinder
{
  private File biggestDirSoFar;
  private long biggestSpaceSoFar = Long.MIN_VALUE;

  public void scanBiggestFreeSpace(File root)
  {
    long freeSpace = root.getUsableSpace();
    reportSpace(root, freeSpace);

    File[] children = root.listFiles();
    if (children == null)
    {
      return;
    }

    for (File child : children)
    {
      if (child.isDirectory())
      {
        scanBiggestFreeSpace(child);
      }
    }
  }

  private void reportSpace(File dir, long freeSpace)
  {
    if (freeSpace > biggestSpaceSoFar)
    {
      biggestDirSoFar = dir;
      biggestSpaceSoFar = freeSpace;
    }
  }

  public File getBestDirectory()
  {
    return biggestDirSoFar;
  }
}
