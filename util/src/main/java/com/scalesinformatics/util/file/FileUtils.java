/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation. The full license may be found in
 * COPYING.LESSER in this project's root directory.
 * 
 * Copyright 2007 jointly held by the authors listed at the top of each
 * source file and/or their respective employers.
 */
package com.scalesinformatics.util.file;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileUtils
{
  private static final Logger logger = 
      Logger.getLogger(FileUtils.class.getName());
  static 
  {
    logger.setLevel(Level.FINEST);
  }
  
  /**
   * Copies a File.
   * @param source must be a File, not a directory
   * @param dest must be a File, not a directory
   * @throws java.io.IOException
   */
  public static void copyFile(File source, File dest) 
      throws IOException
  {
    BufferedInputStream is = null;
    BufferedOutputStream os = null;
    try 
    {
      is = new BufferedInputStream(new FileInputStream(source));
      os = new BufferedOutputStream(new FileOutputStream(dest));

      //byte[] buf = new byte[4096];
      byte[] buf = new byte[262144]; //256kb
      int read;
      while ((read = is.read(buf)) != -1)
      {
        os.write(buf, 0, read);
      }
      os.flush();
    }
    catch (IOException e)
    {
      throw new IOException("Failed to copy file: "
              + source.getAbsolutePath() + " to "
              + dest.getAbsolutePath() + ", message: "
              + e.getMessage(), e);
    }
    finally
    {
      IOException failureOs = closeOutputStreams(os);
      IOException failureIs = closeInputStreams(is);

      if (failureIs != null)
      {
        throw new IOException("Failed to close stream", failureIs);
      }
      if (failureOs != null)
      {
        throw new IOException("Failed to close stream", failureOs);
      }
    }
  }

  public static void copyStream(InputStream source, OutputStream dest)
      throws IOException
  {
    BufferedInputStream is = null;
    BufferedOutputStream os = null;
    try
    {
      is = new BufferedInputStream(source);
      os = new BufferedOutputStream(dest);

      //byte[] buf = new byte[4096];
      byte[] buf = new byte[262144]; //256kb
      int read;
      while ((read = is.read(buf)) != -1)
      {
        os.write(buf, 0, read);
      }
      os.flush();
    }
    catch (IOException e)
    {
      throw new IOException("Failed to copy stream: "+e.getMessage(), e);
    }
    finally
    {
      IOException failureOs = closeOutputStreams(os);
      IOException failureIs = closeInputStreams(is);

      if (failureIs != null)
      {
        throw new IOException("Failed to close stream", failureIs);
      }
      if (failureOs != null)
      {
        throw new IOException("Failed to close stream", failureOs);
      }
    }
  }
  
  /**
   * Loads a file entirely into memory
   * @param source
   * @param dest
   * @return
   * @throws java.io.IOException
   */
  public static byte[] loadFile(File source) 
      throws IOException
  {
    BufferedInputStream is = null;
    ByteArrayOutputStream os = null;
    try 
    {
      is = new BufferedInputStream(new FileInputStream(source));
      os = new ByteArrayOutputStream();

      byte[] buf = new byte[4096];
      int read;
      while ((read = is.read(buf)) != -1)
      {
        os.write(buf, 0, read);
      }
      os.flush(); //needed?
      return os.toByteArray();
    }
    catch (IOException e)
    {
      throw new IOException("Failed to copy file: "+e.getMessage(), e);
    }
    finally
    {
      IOException failureOs = closeOutputStreams(os);
      IOException failureIs = closeInputStreams(is);

      if (failureIs != null)
      {
        throw new IOException("Failed to close stream", failureIs);
      }
      if (failureOs != null)
      {
        throw new IOException("Failed to close stream", failureOs);
      }
    }
  }
  
  public static List<File> findFilesWithExtension(File root, String ext)
  {
    System.out.println("Searching from: "+root.getAbsolutePath());
    List<File> found = new ArrayList<File>();
    if (!root.isDirectory())
    {
      throw new RuntimeException("Expecting a directory");
    }
    
    Stack<File> dirStack = new Stack<File>();
    dirStack.push(root);

    while (!dirStack.isEmpty())
    {
      File curDir = dirStack.pop();

      File[] fileArray = curDir.listFiles();
      for (File file : fileArray)
      {
        if (file.isDirectory())
        {
          dirStack.push(file);
        } else
        {
          if (file.getName().endsWith(ext))
          {
            found.add(file);
          }
        }
      }
    }
    return found;
  }
  
  /**
   * Convenience method that attempts to close one or more streams, without
   * throwing an exception if the operation(s) fail. 
   * @param streams the list of streams that need to be closed
   * @return null if everything went ok, otherwise an IOException - the last
   * exception that occured during stream closing.
   */
  public static IOException closeInputStreams(InputStream... streams)
  {
    IOException lastException = null;
    for(InputStream is : streams)
    {
      try
      {
        if (is != null)
        {
          logger.fine("Attempting to close stream: "+is.toString());
          is.close();
        }
      }
      catch(IOException e)
      {
        logger.severe("Failed to close InputStream: "+e.getMessage());
        lastException = e;
      }
    }
    return lastException;
  }
  
  /**
   * Convenience method that attempts to close one or more streams, without
   * throwing an exception if the operation(s) fail. 
   * @param streams the list of streams that need to be closed
   * @return null if everything went ok, otherwise an IOException - the last
   * exception that occured during stream closing.
   */
  public static IOException closeOutputStreams(OutputStream... streams)
  {
    IOException lastException = null;
    for(OutputStream os : streams)
    {
      try
      {
        if (os != null)
        {
          try
          {
            logger.fine("Attempting to close stream: "+os.toString());
            os.flush();
          }
          catch(IOException e)
          {
            logger.severe("Failed to flush OutputStream: "+e.getMessage());
          }
          os.close();
        }
      }
      catch(IOException e)
      {
        logger.severe("Failed to close OutputStream: "+e.getMessage());
        lastException = e;
      }
    }
    return lastException;
  }
  
}
