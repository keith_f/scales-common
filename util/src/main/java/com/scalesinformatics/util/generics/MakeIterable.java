/*
 * Copyright 2013 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.util.generics;

import java.util.Iterator;

/**
 * We can't go from a <code>DeserialisingIterable</code> which returns an EntityKeys over some parameterised type
 * without compile errors. Eg the following will fail:
 * <pre>
 * return DeserialisingIterable<EntityKeys<Message>> (new DeserialisingIterable<>(cursor, marshaller, EntityKeys.class);
 * </pre>
 *
 * The solution is to create another Iterable that casts each object as it is returned, as below.
 *
 *
 * This incredibly useful class comes from "The Klingonian cast" here, and is a more generic solution to the we
 * previously implemented in MongoUtils (Entanglement project):
 * http://schneide.wordpress.com/2013/02/04/java-generics-the-klingonian-cast/
 *
 * This class gets around the 'type erasure' problem when dealing with parameterised types within Iterables. The example
 * given on the blog is the following:
 *
 * <pre>
 *   List<Integer> integers = new ArrayList<Integer>();
 *   Iterable<Integer> iterable = integers;
 *   Iterable<Number> numbers = integers; // Damn!
 * </pre>
 *
 * Which can be fixed with this class with:
 * <pre>
 *   List<Integer> integers = new ArrayList<Integer>();
 *   Iterable<Number> numbers = MakeIterable.<Number>outOf(integers);
 * </pre>
 *
 * My requirement for this solution came when implementing the Notification system of Microbase 3.0, although this class
 * will be useful in many other cases. Here, we have an Iterable over elements of type EntityKeys that we need to be
 * cast to an Iterable of EntityKeys that represent Messages.
 * <pre>
 *   Iterable<EntityKeys> origItr ...
 *   Iterable<EntityKeys<Message> msgKeys = MakeIterable.<EntityKeys<Message>>outOf(origItr);
 * </pre>
 */
public class MakeIterable {
  public static <T> Iterable<T> byUpcasting(final Iterable<? extends T> iterable) {
    return new Iterable<T>() {
      @Override
      public Iterator<T> iterator() {
        return iteratorByUpcasting(iterable.iterator());
      }
    };
  }

  protected static <T> Iterator<T> iteratorByUpcasting(final Iterator<? extends T> iterator) {
    return new Iterator<T>() {
      @Override
      public boolean hasNext() {
        return iterator.hasNext();
      }
      @Override
      public T next() {
        return iterator.next();
      }
      @Override
      public void remove() {
        iterator.remove();
      }
    };
  }


  public static <T> Iterable<T> byCastingElements(final Iterable iterable) {
    return new Iterable<T>() {
      @Override
      public Iterator<T> iterator() {
        return iteratorByCastingElements(iterable.iterator());
      }
    };
  }

  protected static <T> Iterator<T> iteratorByCastingElements(final Iterator iterator) {
    return new Iterator<T>() {
      @Override
      public boolean hasNext() {
        return iterator.hasNext();
      }
      @Override
      public T next() {
        return (T) iterator.next();
      }
      @Override
      public void remove() {
        iterator.remove();
      }
    };
  }
}
