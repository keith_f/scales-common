/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation. The full license may be found in
 * COPYING.LESSER in this project's root directory.
 * 
 * Copyright 2007 jointly held by the authors listed at the top of each
 * source file and/or their respective employers.
 */

package com.scalesinformatics.util;

import java.util.Collection;
import java.util.List;

/**
 * A set of utilities for manipulating arrays.
 * 
 * @author Keith Flanagan
 */
public class ArrayUtils
{
  /**
   * Searches an array for an element.
   * 
   * @param array
   *            the array to search
   * @param element
   *            the element to search for
   * @return returns true if <code>array<code> contains <code>element</code>
   */
  public static <T> boolean arrayContains(T[] array, T element)
  {
    for (T curEl : array)
    {
      if (curEl.equals(element))
      {
        return true;
      }
    }
    return false;
  }

  /**
   * Searches an array for an element - special comparator for strings.
   * 
   * @param array
   *            the array to search
   * @param element
   *            the string to search for
   * @param ignoreCase
   *            true to ignore case, false for exact match
   * @return returns true if <code>array<code> contains <code>element</code>
   */
  public static boolean arrayContains(String[] array, String element,
      boolean ignoreCase)
  {
    if (ignoreCase)
    {
      for (String curEl : array)
      {
        if (curEl.equalsIgnoreCase(element))
        {
          return true;
        }
      }
    }
    else
    {
      for (String curEl : array)
      {
        if (curEl.equals(element))
        {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Given some Collection and an array of the same type, adds each element of
   * the array to the collection.
   * 
   * @param collection
   *            the Collection to add elements to
   * @param array
   *            the array whose elements are to be added to
   *            <code>collection</code>
   */
  public static <T> void addArrayToCollection(T[] array,
      Collection<T> collection)
  {
    for (T element : array)
    {
      collection.add(element);
    }
  }

  /**
   * Adds each element of <code>collection</code> to the <code>array</code>
   * 
   * @param <T>
   * @param collection
   *            collection of objects to add to the array
   * @param array
   *            the recipient of the objects in <code>collection</code>
   */
  public static <T> void collectionToArray(Collection<T> collection, T[] array)
  {
    addCollectionToArray(collection, array, 0);
  }

  /**
   * Adds each element of <code>collection</code> to the <code>array</code>,
   * starting at array element <code>startIndex</code>
   * 
   * @param <T>
   * @param collection
   *            collection of objects to add to the array
   * @param array
   *            the recipient of the objects in <code>collection</code>
   * @param the
   *            element of the array to start adding collection elements at
   */
  public static <T> void addCollectionToArray(Collection<T> collection,
      T[] array, int index)
  {
    for (T element : collection)
    {
      array[index++] = element;
    }
  }

  public static char[] byteArrayToCharArray(byte[] byteArray)
  {
    char[] charArray = new char[byteArray.length];
    int index = 0;
    for (byte b : byteArray)
    {
      charArray[index++] = (char) b;
    }
    return charArray;
  }

  public static byte[] charArrayToByteArray(char[] charArray)
  {
    byte[] byteArray = new byte[charArray.length];
    int index = 0;
    for (char c : charArray)
    {
      byteArray[index++] = (byte) c;
    }
    return byteArray;
  }

  /**
   * List<List<T>> -> T[y][x]<br>
   * List - y<br>
   * List<T> - x<br>
   * 
   * @param <T>
   * @param listOfLists
   * @param array
   */
  public static <T> void listOfListsTo2dArray(List<List<T>> listOfLists,
      T[][] array)
  {
    int row = 0;
    for (List<T> innerList : listOfLists)
    {
      int col = 0;
      for (T item : innerList)
      {
        array[row][col++] = item;
      }
      row++;
    }
  }

  public static char[] charWrapperToChar(Character[] chars)
  {
    char[] newArray = new char[chars.length];
    for (int i = 0; i < chars.length; i++)
    {
      newArray[i] = chars[i];
    }
    return newArray;
  }

  public static Character[] charToCharWrapper(char[] chars)
  {
    Character[] newArray = new Character[chars.length];
    for (int i = 0; i < chars.length; i++)
    {
      newArray[i] = chars[i];
    }
    return newArray;
  }

  public static byte[] byteWrapperToByte(Byte[] bytes)
  {
    byte[] newArray = new byte[bytes.length];
    for (int i = 0; i < bytes.length; i++)
    {
      newArray[i] = bytes[i];
    }
    return newArray;
  }

  public static Byte[] byteToByteWrapper(byte[] bytes)
  {
    Byte[] newArray = new Byte[bytes.length];
    for (int i = 0; i < bytes.length; i++)
    {
      newArray[i] = bytes[i];
    }
    return newArray;
  }
}
