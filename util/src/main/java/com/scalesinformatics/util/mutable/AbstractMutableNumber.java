/*
 * Copyright 2016 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.util.mutable;

import java.io.Serializable;
import java.util.UUID;

/**
 * @author Keith Flanagan
 */
abstract public class AbstractMutableNumber<T> implements MutableNumber<T>, Serializable {
  private String name;
  private T minValue;
  private T maxValue;
  private T value;

  public AbstractMutableNumber() {
    name = UUID.randomUUID().toString();
  }

  public AbstractMutableNumber(T minValue, T maxValue) {
    this (UUID.randomUUID().toString(), minValue, maxValue);
  }

  public AbstractMutableNumber(String name, T minValue, T maxValue) {
    this.name = name;
    this.minValue = minValue;
    this.maxValue = maxValue;
    mutate(); // Set an initial value
  }

  public AbstractMutableNumber(T minValue, T maxValue, T value) {
    this (UUID.randomUUID().toString(), minValue, maxValue, value);
  }

  public AbstractMutableNumber(String name, T minValue, T maxValue, T value) {
    this.name = name;
    this.minValue = minValue;
    this.maxValue = maxValue;
    this.value = value;
  }

  @Override
  public String toString() {
    return "AbstractMutableNumber{" +
        "name='" + name + '\'' +
        ", minValue=" + minValue +
        ", maxValue=" + maxValue +
        ", value=" + value +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    AbstractMutableNumber<?> that = (AbstractMutableNumber<?>) o;

    return name.equals(that.name);
  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void setName(String name) {
    this.name = name;
  }

  public T getMinValue() {
    return minValue;
  }

  public void setMinValue(T minValue) {
    this.minValue = minValue;
  }

  public T getMaxValue() {
    return maxValue;
  }

  public void setMaxValue(T maxValue) {
    this.maxValue = maxValue;
  }

  public T getValue() {
    return value;
  }

  public void setValue(T value) {
    this.value = value;
  }
}
