/*
 * Copyright 2016 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.util.mutable;

import com.scalesinformatics.util.RandomUtils;

import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public class MutableBool implements MutableVar<Boolean> {

  private String name;
  private Boolean value;

  public MutableBool() {
    this.name = UUID.randomUUID().toString();
  }

  public MutableBool(Boolean value) {
    this(UUID.randomUUID().toString(), value);
  }

  public MutableBool(String name, Boolean value) {
    this.name = name;
    this.value = value;
  }

  @Override
  public void mutate() {
    setValue(RandomUtils.getRandom().nextBoolean());
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void setName(String name) {
    this.name = name;
  }

  @Override
  public Boolean getValue() {
    return value;
  }

  @Override
  public void setValue(Boolean value) {
    this.value = value;
  }
}
