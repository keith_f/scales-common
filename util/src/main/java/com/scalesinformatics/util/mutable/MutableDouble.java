/*
 * Copyright 2016 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.util.mutable;

import com.scalesinformatics.util.RandomUtils;

/**
 * @author Keith Flanagan
 */
public class MutableDouble extends AbstractMutableNumber<Double> {

  public MutableDouble() {
  }

  public MutableDouble(Double minValue, Double maxValue) {
    super(minValue, maxValue);
  }

  public MutableDouble(String name, Double minValue, Double maxValue) {
    super(name, minValue, maxValue);
  }

  public MutableDouble(Double minValue, Double maxValue, Double value) {
    super(minValue, maxValue, value);
  }

  public MutableDouble(String name, Double minValue, Double maxValue, Double value) {
    super(name, minValue, maxValue, value);
  }

  @Override
  public void mutate() {
    setValue(RandomUtils.randomDoubleInRange(getMinValue(), getMaxValue()));
  }
}
