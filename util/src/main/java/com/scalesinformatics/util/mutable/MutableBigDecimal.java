/*
 * Copyright 2016 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.util.mutable;

import com.scalesinformatics.util.RandomUtils;

import java.math.BigDecimal;

/**
 * @author Keith Flanagan
 */
public class MutableBigDecimal extends AbstractMutableNumber<BigDecimal> {

  public MutableBigDecimal() {
  }

  public MutableBigDecimal(BigDecimal minValue, BigDecimal maxValue) {
    super(minValue, maxValue);
  }

  public MutableBigDecimal(String name, BigDecimal minValue, BigDecimal maxValue) {
    super(name, minValue, maxValue);
  }

  public MutableBigDecimal(BigDecimal minValue, BigDecimal maxValue, BigDecimal value) {
    super(minValue, maxValue, value);
  }

  public MutableBigDecimal(String name, BigDecimal minValue, BigDecimal maxValue, BigDecimal value) {
    super(name, minValue, maxValue, value);
  }

  @Override
  public void mutate() {
    setValue(new BigDecimal(
        RandomUtils.randomDoubleInRange(getMinValue().doubleValue(), getMaxValue().doubleValue())));
  }
}
