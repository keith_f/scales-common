/*
 * Copyright 2016 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.util.mutable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.scalesinformatics.util.RandomUtils;

/**
 * A composition of one or more MutableVars.
 * Provides a method to programatically obtain a list of the MutableVars in order to
 * perform automated mutations.
 *
 * @author Keith Flanagan
 */
public interface MutatableThing {
  @JsonIgnore
  MutableVar[] getMutableProperties();

  /**
   * Performs one or more mutations on this composition.
   * @param maxMutations
   */
  default void mutate(int maxMutations) {
    MutableVar[] mutableVars = getMutableProperties();
    if (mutableVars.length == 0) {
      // Nothing to mutate (no variables)
      return;
    }
    //System.out.println(mutableVars.length);

    preMutate(maxMutations);
    for (int i=0; i<maxMutations; i++) {
      int idx = RandomUtils.getRandom().nextInt(mutableVars.length);
      mutableVars[idx].mutate();
    }
    postMutate(maxMutations);
  }

  default void preMutate(int maxMutations) {
    // Default so a no-op
  }

  default void postMutate(int maxMutations) {
    // Default so a no-op
  }
}
