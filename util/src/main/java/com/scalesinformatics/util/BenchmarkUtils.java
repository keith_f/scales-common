package com.scalesinformatics.util;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is designed to aid with benchmarks/performance testing of code. Quite often, you need to print out
 * periodic performance/progress information - for example, every X iterations of a loop. Instead of hand-crafting
 * this code each time, this class enables you to easily measure and print performance information per X iterations,
 * and also
 */
public class BenchmarkUtils<T> {
  public static class Iteration<T> {
    private long timestamp;
    private T iterationItems;

    public long getTimestamp() {
      return timestamp;
    }

    public void setTimestamp(long timestamp) {
      this.timestamp = timestamp;
    }

    public T getIterationItems() {
      return iterationItems;
    }

    public void setIterationItems(T iterationItems) {
      this.iterationItems = iterationItems;
    }
  }


  private long startTime;
  private List<Iteration<T>> iterations;

  public BenchmarkUtils() {
    startTime = System.currentTimeMillis();
    iterations = new ArrayList<>();
  }

  public void beginNextIteration() {
    nextIteration(null);
  }

  public void nextIteration(T items) {
    Iteration itr = new Iteration();
    itr.setTimestamp(System.currentTimeMillis());
    itr.setIterationItems(items);
    iterations.add(itr);
  }

  public int countIterations() {
    return iterations.size();
  }

  public double averageIterationTimeInSeconds() {
    double now = System.currentTimeMillis();
    double duration = now - startTime;
    return duration / iterations.size() / 1000;
  }

  public double lastIterationTimeInSeconds() {
    double now = System.currentTimeMillis();
    double start = startTime;
    if (!iterations.isEmpty()) {
      Iteration<T> last = iterations.get(iterations.size()-1);
      start = last.getTimestamp();
    }

    double durationMs = now - start;
    return durationMs / 1000;
  }

  public double totalTimeInSeconds() {
    double now = System.currentTimeMillis();
    double durationMs = now - startTime;
    return durationMs / 1000;
  }


}
