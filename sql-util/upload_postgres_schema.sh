#!/bin/sh

# This script performs runs something from the OGRE tree.
# Things are run with a classpath built from all the jars in ./lib

# Args:
# 1 class to run
# 2, 3 ..., n arguments passed on to java class

PROG_HOME="."
CLASSPATH=$PROG_HOME/target/classes
MVN="mvn $MVN_OPTS"

if [ ! -d $PROG_HOME/target/dependency ] ; then
  $MVN dependency:copy-dependencies
fi


for LIB in `find $PROG_HOME/target/dependency -name "*.jar"` ; do
    CLASSPATH=$CLASSPATH:$LIB
done


#echo $CLASSPATH

java -Xmx768M -cp $CLASSPATH uk.ac.ncl.keithf.database.jdbc.pool.PostgresUtils $@

