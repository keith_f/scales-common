/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Feb 9, 2012, 12:34:31 PM
 */

package com.scalesinformatics.jdbcutils;

import java.sql.*;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

/**
 * An implementation of a Connection that can be re-used by a
 * connection pool. Almost every method call is simply forwarded onto the 
 * delegate, with the exception of 'close()', that informs the connection
 * pool that the Connection is no longer required.
 * 
 * @author Keith Flanagan
 */
public class ReusableConnection
  implements Connection
{
  private final DistributedJdbcPool pool;
  private final Connection delegate;
  
  public ReusableConnection(DistributedJdbcPool pool, Connection delegate)
  {
    this.pool = pool;
    this.delegate = delegate;
  }
  
  /**
   * Used by the connection pool manager to close the underlying JDBC
   * connection.
   * @throws SQLException 
   */
  public void closeDelegate()
      throws SQLException
  {
    delegate.close();
  }

  @Override
  public void setTypeMap(Map<String, Class<?>> map)
      throws SQLException
  {
    delegate.setTypeMap(map);
  }

  @Override
  public void setTransactionIsolation(int i)
      throws SQLException
  {
    delegate.setTransactionIsolation(i);
  }

  @Override
  public Savepoint setSavepoint(String string)
      throws SQLException
  {
    return delegate.setSavepoint(string);
  }

  @Override
  public Savepoint setSavepoint()
      throws SQLException
  {
    return delegate.setSavepoint();
  }

  @Override
  public void setReadOnly(boolean bln)
      throws SQLException
  {
    delegate.setReadOnly(bln);
  }

  @Override
  public void setHoldability(int i)
      throws SQLException
  {
    delegate.setHoldability(i);
  }

  @Override
  public void setClientInfo(Properties prprts)
      throws SQLClientInfoException
  {
    delegate.setClientInfo(prprts);
  }

  @Override
  public void setClientInfo(String string, String string1)
      throws SQLClientInfoException
  {
    delegate.setClientInfo(string, string1);
  }

  @Override
  public void setCatalog(String string)
      throws SQLException
  {
    delegate.setCatalog(string);
  }

  @Override
  public void setAutoCommit(boolean bln)
      throws SQLException
  {
    delegate.setAutoCommit(bln);
  }

  @Override
  public void rollback(Savepoint svpnt)
      throws SQLException
  {
    delegate.rollback(svpnt);
  }

  @Override
  public void rollback()
      throws SQLException
  {
    delegate.rollback();
  }

  @Override
  public void releaseSavepoint(Savepoint svpnt)
      throws SQLException
  {
    delegate.releaseSavepoint(svpnt);
  }

  @Override
  public PreparedStatement prepareStatement(String string, String[] strings)
      throws SQLException
  {
    return delegate.prepareStatement(string, strings);
  }

  @Override
  public PreparedStatement prepareStatement(String string, int[] ints)
      throws SQLException
  {
    return delegate.prepareStatement(string, ints);
  }

  @Override
  public PreparedStatement prepareStatement(String string, int i)
      throws SQLException
  {
    return delegate.prepareStatement(string, i);
  }

  @Override
  public PreparedStatement prepareStatement(String string, int i, int i1, int i2)
      throws SQLException
  {
    return delegate.prepareStatement(string, i, i1, i2);
  }

  @Override
  public PreparedStatement prepareStatement(String string, int i, int i1)
      throws SQLException
  {
    return delegate.prepareStatement(string, i, i1);
  }

  @Override
  public PreparedStatement prepareStatement(String string)
      throws SQLException
  {
    return delegate.prepareStatement(string);
  }

  @Override
  public CallableStatement prepareCall(String string, int i, int i1, int i2)
      throws SQLException
  {
    return delegate.prepareCall(string, i, i1, i2);
  }

  @Override
  public CallableStatement prepareCall(String string, int i, int i1)
      throws SQLException
  {
    return delegate.prepareCall(string, i, i1);
  }

  @Override
  public CallableStatement prepareCall(String string)
      throws SQLException
  {
    return delegate.prepareCall(string);
  }

  @Override
  public String nativeSQL(String string)
      throws SQLException
  {
    return delegate.nativeSQL(string);
  }

  @Override
  public boolean isValid(int i)
      throws SQLException
  {
    return delegate.isValid(i);
  }

  @Override
  public boolean isReadOnly()
      throws SQLException
  {
    return delegate.isReadOnly();
  }

  @Override
  public boolean isClosed()
      throws SQLException
  {
    return delegate.isClosed();
  }

  @Override
  public SQLWarning getWarnings()
      throws SQLException
  {
    return delegate.getWarnings();
  }

  @Override
  public Map<String, Class<?>> getTypeMap()
      throws SQLException
  {
    return delegate.getTypeMap();
  }

  @Override
  public int getTransactionIsolation()
      throws SQLException
  {
    return delegate.getTransactionIsolation();
  }

  @Override
  public DatabaseMetaData getMetaData()
      throws SQLException
  {
    return delegate.getMetaData();
  }

  @Override
  public int getHoldability()
      throws SQLException
  {
    return delegate.getHoldability();
  }

  @Override
  public Properties getClientInfo()
      throws SQLException
  {
    return delegate.getClientInfo();
  }

  @Override
  public String getClientInfo(String string)
      throws SQLException
  {
    return delegate.getClientInfo(string);
  }

  @Override
  public String getCatalog()
      throws SQLException
  {
    return delegate.getCatalog();
  }

  @Override
  public boolean getAutoCommit()
      throws SQLException
  {
    return delegate.getAutoCommit();
  }

  @Override
  public Struct createStruct(String string, Object[] os)
      throws SQLException
  {
    return delegate.createStruct(string, os);
  }

  @Override
  public Statement createStatement(int i, int i1, int i2)
      throws SQLException
  {
    return delegate.createStatement(i, i1, i2);
  }

  @Override
  public Statement createStatement(int i, int i1)
      throws SQLException
  {
    return delegate.createStatement(i, i1);
  }

  @Override
  public Statement createStatement()
      throws SQLException
  {
    return delegate.createStatement();
  }

  @Override
  public SQLXML createSQLXML()
      throws SQLException
  {
    return delegate.createSQLXML();
  }

  @Override
  public NClob createNClob()
      throws SQLException
  {
    return delegate.createNClob();
  }

  @Override
  public Clob createClob()
      throws SQLException
  {
    return delegate.createClob();
  }

  @Override
  public Blob createBlob()
      throws SQLException
  {
    return delegate.createBlob();
  }

  @Override
  public Array createArrayOf(String string, Object[] os)
      throws SQLException
  {
    return delegate.createArrayOf(string, os);
  }

  @Override
  public void commit()
      throws SQLException
  {
    delegate.commit();
  }

  @Override
  public void close()
      throws SQLException
  {
    pool.releaseConnection(this);
  }

  @Override
  public void clearWarnings()
      throws SQLException
  {
    delegate.clearWarnings();
  }

  @Override
  public <T> T unwrap(Class<T> type)
      throws SQLException
  {
    return delegate.unwrap(type);
  }

  @Override
  public boolean isWrapperFor(Class<?> type)
      throws SQLException
  {
    return delegate.isWrapperFor(type);
  }

  @Override
  public void setSchema(String string)
      throws SQLException
  {
    delegate.setSchema(string);
  }

  @Override
  public String getSchema()
      throws SQLException
  {
    return delegate.getSchema();
  }

  @Override
  public void abort(Executor exctr)
      throws SQLException
  {
    delegate.abort(exctr);
  }

  @Override
  public void setNetworkTimeout(Executor exctr, int i)
      throws SQLException
  {
    delegate.setNetworkTimeout(exctr, i);
  }

  @Override
  public int getNetworkTimeout()
      throws SQLException
  {
    return delegate.getNetworkTimeout();
  }

  
}
