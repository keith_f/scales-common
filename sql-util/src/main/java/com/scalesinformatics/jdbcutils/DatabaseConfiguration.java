/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * File created: 18-Nov-2011, 14:02:51
 */
package com.scalesinformatics.jdbcutils;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Keith Flanagan
 */
public class DatabaseConfiguration
    implements Serializable
{
  private String username;
  private String password;
  private Set<String> writeUrls;
  private Set<String> readUrls;
  
  public DatabaseConfiguration()
  {
    username = null;
    password = null;
    writeUrls = new HashSet<String>();
    readUrls = new HashSet<String>();
  }

  public String getPassword()
  {
    return password;
  }

  public void setPassword(String password)
  {
    this.password = password;
  }

  public Set<String> getReadUrls()
  {
    return readUrls;
  }

  public void setReadUrls(Set<String> readUrls)
  {
    this.readUrls = readUrls;
  }

  public String getUsername()
  {
    return username;
  }

  public void setUsername(String username)
  {
    this.username = username;
  }

  public Set<String> getWriteUrls()
  {
    return writeUrls;
  }

  public void setWriteUrls(Set<String> writeUrls)
  {
    this.writeUrls = writeUrls;
  }
}
