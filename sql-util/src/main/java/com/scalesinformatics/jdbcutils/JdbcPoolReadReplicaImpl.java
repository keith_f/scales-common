/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * File created: 16-Nov-2011, 15:31:14
 */
package com.scalesinformatics.jdbcutils;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Logger;

/**
 * A JDBC connection pool that is intended to support multiple database servers
 * arranged in a master -> { set of read replica } fashion. Examples include
 * Amazon's RDS, or PostgreSQL master -> slave configuration.
 * <p/>
 * This implementation supports a single 'master' database supporting write
 * operations. Many slaves are supported. Therefore, this implementation is
 * suitable for use in situations where reads outnumber write operations.
 *
 * @author Keith Flanagan
 */
public class JdbcPoolReadReplicaImpl
    implements DistributedJdbcPool {
  private static final Logger l =
      Logger.getLogger(JdbcPoolReadReplicaImpl.class.getName());

  private static final int IS_VALID_TIMEOUT = 3000;

  private Set<String> readDbUrls;
  private Set<String> writeDbUrls;
  private String username;
  private String password;

  private int maximumReadConnections;
  private int maximumWriteConnections;

  private transient final Set<Connection> readConnectionPool;
  private transient final Set<Connection> writeConnectionPool;

  private boolean cacheWriteConnections;
  private boolean cacheReadConnections;

  public JdbcPoolReadReplicaImpl() {
    readConnectionPool = new HashSet<>();
    writeConnectionPool = new HashSet<>();

    cacheWriteConnections = true;
    cacheReadConnections = true;
  }

  private Connection tryExistingConnections(Iterator<Connection> connItr) {
    while (connItr.hasNext()) {
      Connection conn = connItr.next();
      connItr.remove();

      //Test if connection is still alive.
      try {
        if (conn.isValid(IS_VALID_TIMEOUT)) {
          conn.rollback();
          return conn;
        } else {
          l.info("Connection was no longer valid. Trying another.");
        }
      } catch (Exception e) {
        l.info("Connection could not be re-used!");
        e.printStackTrace();
      }
    }

    //No suitable connection was found
    return null;
  }

  @Override
  public void closeAll() throws SQLException {
    for (Connection conn : readConnectionPool) {
      conn.close();
    }
    for (Connection conn : writeConnectionPool) {
      conn.close();
    }
  }

  @Override
  public Connection getReadOnlyConnection()
      throws SQLException {
    Connection conn;
    //Attempt to obtain an existing connection
    synchronized (readConnectionPool) {
      Iterator<Connection> connItr = readConnectionPool.iterator();
      conn = tryExistingConnections(connItr);
    }

    //Create a new connection instead
    if (conn == null) {
      List<String> urls = new ArrayList<>(readDbUrls);
      if (urls.isEmpty()) {
        throw new SQLException("No DB URLs were set!");
      }
      Collections.shuffle(urls);
      String url = urls.iterator().next();
      conn = JdbcUtils.obtainConnection(url, username, password);
      conn.setReadOnly(true);
    }
    return new ReusableConnection(this, conn);
  }

  @Override
  public Connection getWritableConnection()
      throws SQLException {
    Connection conn;

    //Attempt to obtain an existing connection
    synchronized (writeConnectionPool) {
      Iterator<Connection> connItr = writeConnectionPool.iterator();
      conn = tryExistingConnections(connItr);
    }

    //Create a new connection instead
    if (conn == null) {
      Set<String> urls = new HashSet<>(writeDbUrls);
      if (urls.size() != 1) {
        throw new SQLException("Exactly one writable database URL must set! (Found " + urls.size() + ")");
      }
      String url = urls.iterator().next();
      conn = JdbcUtils.obtainConnection(url, username, password);
    }
    return  new ReusableConnection(this, conn);
  }

  @Override
  public void releaseConnection(ReusableConnection conn)
      throws SQLException {
    //Put this connection back into the pool
    if (conn.isClosed() || !conn.isValid(IS_VALID_TIMEOUT)) {
      return;
    }

    if (conn.isReadOnly() && cacheReadConnections) {
      synchronized (readConnectionPool) {
        readConnectionPool.add(conn);
      }
    } else if (!conn.isReadOnly() && cacheWriteConnections) {
      synchronized (writeConnectionPool) {
        writeConnectionPool.add(conn);
      }
    }
  }

  @Override
  public void setReadDbUrls(Set<String> readDbUrls) {
    this.readDbUrls = readDbUrls;
  }

  @Override
  public Set<String> getReadDbUrls() {
    return readDbUrls;
  }

  @Override
  public void setWriteDbUrls(Set<String> writeDbUrls) {
    this.writeDbUrls = writeDbUrls;
  }

  @Override
  public Set<String> getWriteDbUrls() {
    return writeDbUrls;
  }

  @Override
  public void setUsername(String username) {
    this.username = username;
  }

  @Override
  public String getUsername() {
    return username;
  }

  @Override
  public void setPassword(String password) {
    this.password = password;
  }

  @Override
  public String getPassword() {
    return password;
  }

  @Override
  public void setMaximumReadConnections(int maxConns) {
    this.maximumReadConnections = maxConns;
  }

  @Override
  public int getMaximumReadConnections() {
    return maximumReadConnections;
  }

  @Override
  public void setMaximumWriteConnections(int maxConns) {
    this.maximumWriteConnections = maxConns;
  }

  @Override
  public int getMaximumWriteConnection() {
    return maximumWriteConnections;
  }

  @Override
  public boolean isCacheWriteConnections() {
    return cacheWriteConnections;
  }

  @Override
  public void setCacheWriteConnections(boolean cacheWriteConnections) {
    this.cacheWriteConnections = cacheWriteConnections;
  }

  @Override
  public boolean isCacheReadConnections() {
    return cacheReadConnections;
  }

  @Override
  public void setCacheReadConnections(boolean cacheReadConnections) {
    this.cacheReadConnections = cacheReadConnections;
  }
}
