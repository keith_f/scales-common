/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * File created: 16-Nov-2011, 15:28:15
 */
package com.scalesinformatics.jdbcutils;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Set;

/**
 * A JDBC connection pool that is intended to support multiple database servers.
 * Implementations of this interface may support different database configurations,
 * such as read-replicas, round-robin writing to shards, etc.
 * 
 * To open a connection, call either 'getReadOnlyConnection' or 'getWritableConnection'.
 * To release a connection back to the pool, simply call the Connection.close() 
 * method. If the implementation supports connection caching, it will be
 * available for future re-use. 
 * 
 * TODO more methods for: maximum local and maximum distributed connections. Or
 * alternatively, maximum connections per server node, allowing client threads
 * to scale as number of servers grows.
 * 
 * TODO exceptions for get*Connection when we're blocked waiting for a free
 * slot for too long.
 * 
 * TODO get/set methods for timeout for waiting for a connection (max block time)
 * 
 * @author Keith Flanagan
 */
public interface DistributedJdbcPool
{
  public void closeAll() throws SQLException;


  public boolean isCacheWriteConnections();

  public void setCacheWriteConnections(boolean poolWriteConnections);

  public boolean isCacheReadConnections();

  public void setCacheReadConnections(boolean poolReadConnections);

  /**
   * Returns a Connection for read purposes only.
   * 
   * @return a read-only Connection
   * @throws SQLException 
   */
  public Connection getReadOnlyConnection()
      throws SQLException;
  
  /**
   * Returns a Connection capable of both read and write operations.
   * 
   * @return a Connection capable of both read and write operations.
   * @throws SQLException 
   */
  public Connection getWritableConnection()
      throws SQLException;
  
  /**
   * Called by a ReusableConnection when it is no longer required by user code
   * (i.e., the user has called close()).
   * @param conn
   * @throws SQLException 
   */
  public void releaseConnection(ReusableConnection conn)
      throws SQLException;
  
  /**
   * Sets a list of databases that can be used for read-only operations.
   * @param readDbUrls 
   */
  public void setReadDbUrls(Set<String> readDbUrls);
  public Set<String> getReadDbUrls();
  
  /**
   * Sets a list of databases that can be used for read/write operations.
   * @param writeDbUrls 
   */
  public void setWriteDbUrls(Set<String> writeDbUrls);
  public Set<String> getWriteDbUrls();
  
  public void setUsername(String username);
  public String getUsername();
  
  public void setPassword(String password);
  public String getPassword();
  
  
  /**
   * Sets an upper limit on the total number of concurrent read connections.
   * A limit of -1 indicates 'no limit'.
   * 
   * @param maxConns the maximum number of connections, or a negative value for
   * no limit.
   */
  public void setMaximumReadConnections(int maxConns);
  public int getMaximumReadConnections();

  /**
   * Sets an upper limit on the total number of concurrent write connections.
   * A limit of -1 indicates 'no limit'.
   * 
   * @param maxConns the maximum number of connections, or a negative value for
   * no limit.
   */
  public void setMaximumWriteConnections(int maxConns);
  public int getMaximumWriteConnection();
  

}
