/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * File created: 16-Nov-2011, 15:15:58
 */
package com.scalesinformatics.jdbcutils;

import java.io.*;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utility class providing methods for connecting to JDBC databases.
 * 
 * @author Keith Flanagan
 */
public class JdbcUtils
{
  private static final Logger logger =
    Logger.getLogger(JdbcUtils.class.getName());

  /*
   * Shouldn't need to load JDBC drivers explicitly anymore since JDBC 4.0:
   * "Any JDBC 4.0 drivers that are found in your class path are automatically 
   * loaded. (However, you must manually load any drivers prior to JDBC 4.0 
   * with the method Class.forName.)"
   * http://download.oracle.com/javase/tutorial/jdbc/basics/connecting.html
   * 
   */
  private static final String PG_JDBC_DRIVER_NAME = "org.postgresql.Driver";
  private static final String MYSQL_JDBC_DRIVER_NAME = "com.mysql.jdbc.Driver";
  
  private static Class JDBC_DRIVER = null;

  /**
   * Connects to a JDBC database
   * @param driver eg: org.postgresql.Driver
   * @param url eg: <code>jdbc:postgresql://localhost:5432/database_name</code> 
   * or <code>jdbc:mysql://localhost:3306/database_name</code>
   * @param user
   * @param pass
   * @return
   * @throws java.sql.SQLException
   * @throws java.lang.ClassNotFoundException
   */
  public static Connection obtainConnection(
      String url, String user, String pass)
      throws SQLException//, ClassNotFoundException
  {
//    if (JDBC_DRIVER == null)
//    {
//      System.out.println("Attempting to load driver: "+JDBC_DRIVER_NAME);
//      JDBC_DRIVER = Class.forName(JDBC_DRIVER_NAME);
//    }
    Properties props = new Properties();
    props.setProperty("user", user);
    props.setProperty("password", pass);
//    logger.log(Level.INFO, "Attempting to connect to database: {0}", url);
    
    Connection conn = DriverManager.getConnection(url, props);
    conn.setAutoCommit(false);
//    conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
//    conn.rollback();
    
//    logger.log(Level.INFO, "Established a connection to database: {0}", url);
    return conn;
  }
  
public static void silentRollback(Connection conn)
  {
    if (conn == null)
    {
      return;
    }
    try
    {
      conn.rollback();
    }
    catch(SQLException e)
    {
      logger.log(Level.INFO, "Error rolling back connection (but not "
          + "much we can do...): {0}", e.getMessage());
    }
  }

  public static void silentClose(Connection conn)
  {
    if (conn == null)
    {
      return;
    }
    try
    {
      silentRollback(conn);
      conn.close();
    }
    catch(SQLException e)
    {
      logger.log(Level.INFO, "Error closing connection (but not much "
          + "we can do...): {0}", e.getMessage());
    }
  }

  public static void silentClose(ResultSet rs)
  {
    if (rs == null)
    {
      return;
    }
    try
    {
      rs.close();
    }
    catch(SQLException e)
    {
      logger.log(Level.INFO, "Error closing ResultSet (but not "
          + "much we can do...): {0}", e.getMessage());
    }
  }

  public static void silentClose(Statement stmt)
  {
    if (stmt == null)
    {
      return;
    }
    try
    {
      stmt.close();
    }
    catch(SQLException e)
    {
      logger.log(Level.INFO, "Error closing Statement (but not much "
          + "we can do...): {0}", e.getMessage());
    }
  }

  public static void silentClose(PreparedStatement stmt)
  {
    if (stmt == null)
    {
      return;
    }
    try
    {
      stmt.close();
    }
    catch(SQLException e)
    {
      logger.log(Level.INFO, "Error closing PreparedStatement (but not much "
          + "we can do...): {0}", e.getMessage());
    }
  }
  
  public static void uploadSchema(Connection conn, InputStream schema)
      throws IOException, SQLException
  {
    BufferedReader reader = new BufferedReader(new InputStreamReader(schema));
    uploadSchema(conn, reader);
  }

  /**
   * Uploads an entire database schema. If tables already exist, an exception
   * will be thrown.
   *
   * @param schema a string - may contain several semicolon and /linebreak
   * separated statements
   * @param conn
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   */
  public static void uploadSchema(Connection conn, String schema)
      throws IOException, SQLException
  {
    BufferedReader reader = new BufferedReader(new InputStreamReader(
        new ByteArrayInputStream(schema.getBytes())));
    uploadSchema(conn, reader);
  }

  public static void uploadSchema(Connection conn, BufferedReader schema)
      throws IOException, SQLException
  {
    //Split schema into statements
    List<String> statements = new LinkedList<String>();
    String line;
    StringBuilder st = new StringBuilder();
    while ((line = schema.readLine()) != null)
    {
      if (line.startsWith("--"))
      {
        continue;
      }
      st.append(line);
      if (line.contains(";"))
      {
        statements.add(st.toString());
        st = new StringBuilder();
      }
    }

    //Execute each statement in order
    conn.setAutoCommit(true);
    for (String sql : statements)
    {
      Statement stmt = conn.createStatement();
      logger.log(Level.INFO, "Executing:\n{0}", sql);
      stmt.execute(sql);
    }
    conn.setAutoCommit(false);
    conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
  }

  public static void reportEmbeddedSQLException(SQLException e)
  {
    if (e.getNextException() != null)
    {
      System.out.println("**********************************************");
      System.out.println("'Next' SQL exception: ");
      e.getNextException().printStackTrace();
      System.out.println("**********************************************");
    }
  }
  
}
