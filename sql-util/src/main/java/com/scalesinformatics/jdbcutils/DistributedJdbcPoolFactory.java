/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * File created: 17-Nov-2011, 16:31:56
 */
package com.scalesinformatics.jdbcutils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author Keith Flanagan
 */
public class DistributedJdbcPoolFactory
{
  /**
   * Creates a connection pool that is intended to communicate with a set of
   * SQL servers arranged as a single master with multiple replicated slaves.
   * In this scenario, the master node is capable of read/write operations, but
   * the slaves are read-only and may slightly lag behind the master.
   * 
   * Note, if there is a single database server (i.e., non-replicated, then 
   * this URL needs to be present both in the set of read and write URLs.
   * 
   * @return a connection pool
   */
  public static JdbcPoolReadReplicaImpl createSingleWriteMultipleReplicaPool(
      DatabaseConfiguration config)
  {
    JdbcPoolReadReplicaImpl pool = new JdbcPoolReadReplicaImpl();
    
    pool.setUsername(config.getUsername());
    pool.setPassword(config.getPassword());
    pool.setReadDbUrls(config.getReadUrls());
    pool.setWriteDbUrls(config.getWriteUrls());
    
    return pool;
  }
  
  public static DatabaseConfiguration readConfigurationFromPropertyResource(
      ClassLoader cl, String resourcePath) throws IOException, DatabaseConfigurationException
  {
    Properties props = new Properties();
    InputStream is = cl.getResourceAsStream(resourcePath);
    if (is == null)
    {
      throw new DatabaseConfigurationException(
          "Could not find the resource: "+resourcePath);
    }
    props.load(is);
    
    return readConfigurationFromProperties(props);
  }
  
  public static DatabaseConfiguration readConfigurationFromPropertyResource(
      InputStream is) 
      throws IOException, DatabaseConfigurationException
  {
    Properties props = new Properties();
    if (is == null)
    {
      throw new DatabaseConfigurationException(
          "Could not open the resource from the specified InputStream");
    }
    props.load(is);
    
    return readConfigurationFromProperties(props);
  }
  
  public static DatabaseConfiguration readConfigurationFromProperties(
      Properties props)
      throws DatabaseConfigurationException
  {
    DatabaseConfiguration config = new DatabaseConfiguration();
    
//    Properties props = new Properties();
    for (String propName : props.stringPropertyNames())
    {
      String value = props.getProperty(propName);
      
      if (propName.equals("username"))
      {
        config.setUsername(value);
      }
      else if (propName.equals("password"))
      {
        config.setPassword(value);
      }
      else if (propName.startsWith("db_url_writable"))
      {
        config.getWriteUrls().add(value);
      }
      else if (propName.startsWith("db_url_readonly"))
      {
        config.getReadUrls().add(value);
      }
    }
    
    if (config.getUsername() == null)
    {
      throw new DatabaseConfigurationException("Username was not set!");
    }
    if (config.getPassword() == null)
    {
      throw new DatabaseConfigurationException("Password was not set!");
    }
    return config;
  }
}


