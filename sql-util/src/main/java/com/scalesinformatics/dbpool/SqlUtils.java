/*
 * Copyright 2013 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.dbpool;

/**
 * @author Keith Flanagan
 */
public class SqlUtils {
  public static String createPostgresUrl(String hostname, String databaseName) {
    return "jdbc:postgresql://"+hostname+"/"+databaseName;
  }

  public static String createPostgresUrl(String hostname, int port, String databaseName) {
    return "jdbc:postgresql://"+hostname+":"+port+"/"+databaseName;
  }

  public static String createPostgresUrl(String hostname, String port, String databaseName) {
    return "jdbc:postgresql://"+hostname+":"+port+"/"+databaseName;
  }

  public static String createMysqlUrl(String hostname, String databaseName) {
    return "jdbc:mysql://"+hostname+"/"+databaseName;
  }

  public static String createMysqlUrl(String hostname, int port, String databaseName) {
    return "jdbc:mysql://"+hostname+":"+port+"/"+databaseName;
  }

  public static String createMysqlUrl(String hostname, String port, String databaseName) {
    return "jdbc:mysql://"+hostname+":"+port+"/"+databaseName;
  }
}
