/*
 * Copyright 2013 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.dbpool;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Logger;

/**
 * @author Keith Flanagan
 */
public class HikariSqlPool implements DistributedJdbcPool {
  private static final Logger logger = Logger.getLogger(HikariSqlPool.class.getName());

  private static final int DEFAULT_MIN_IDLE = 2;
  //private static final int DEFAULT_MAX_POOL_SIZE = DEFAULT_MIN_POOL_SIZE + Math.min(60, Runtime.getRuntime().availableProcessors());
  private static final int DEFAULT_MAX_POOL_SIZE = 2;

  private final Random random = new Random();

  private final String username;
  private final String password;
  private final int minIdle;
  private final int maxPoolSize;

  public static final int SQL_ERR_SERIALIZATION_FAILURE = 40001;


  private final List<HikariDataSource> writable;
  private final List<HikariDataSource> readOnly;

  public static HikariSqlPool make(String username, String password, String dbUrl) {
    return make(username, password, dbUrl, DEFAULT_MIN_IDLE, DEFAULT_MAX_POOL_SIZE);
  }

  public static HikariSqlPool make(String username, String password, String dbUrl, int minIdleThreads, int maxThreads) {
    Set<String> writeUrls = new HashSet<>();
    writeUrls.add(dbUrl);

    Set<String> readUrls = new HashSet<>();
    readUrls.add(dbUrl);

    return new HikariSqlPool(username, password, writeUrls, readUrls, minIdleThreads, maxThreads);
  }

  public HikariSqlPool(String username, String password, Set<String> writeDbUrls, Set<String> readDbUrls,
                       int minIdleThreads, int maxThreads) {
    this.username = username;
    this.password = password;
    writable = new ArrayList<>(writeDbUrls.size());
    readOnly = new ArrayList<>(readDbUrls.size());

    this.minIdle = minIdleThreads;
    this.maxPoolSize = maxThreads;

    for (String writeUrl : writeDbUrls) {
      HikariConfig config = new HikariConfig();
      //config.setJdbcUrl("jdbc:mysql://localhost:3306/simpsons");

      // See https://github.com/brettwooldridge/HikariCP

      config.setJdbcUrl(writeUrl);
      config.setUsername(username);
      config.setPassword(password);
      config.setMinimumIdle(minIdle);
      config.setMaximumPoolSize(maxPoolSize);
      config.setAutoCommit(false);
//      config.setTransactionIsolation("TRANSACTION_SERIALIZABLE");
//      config.setTransactionIsolation("TRANSACTION_REPEATABLE_READ");
      config.setTransactionIsolation("TRANSACTION_READ_COMMITTED");

      /*
       * This property controls the maximum amount of time that a connection is allowed to sit idle in the pool.
       * Whether a connection is retired as idle or not is subject to a maximum variation of +30 seconds, and average
       * variation of +15 seconds. A connection will never be retired as idle before this timeout. A value of 0 means
       * that idle connections are never removed from the pool. Default: 600000 (10 minutes)
       */
      config.setIdleTimeout(30000); // 0.5 minute
      config.setMaxLifetime(5 * 60000); // 5 minutes (excludes in-use connections that have not been returned to the pool)
      // The amount of time that we are willing to wait for a connection
      config.setConnectionTimeout(30000); // 30 seconds (default)

      config.addDataSourceProperty("cachePrepStmts", "true");
      config.addDataSourceProperty("prepStmtCacheSize", "250");
      config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");

      writable.add(new HikariDataSource(config));
    }

    for (String readUrl : readDbUrls) {
      HikariConfig config = new HikariConfig();
      //config.setJdbcUrl("jdbc:mysql://localhost:3306/simpsons");
      config.setJdbcUrl(readUrl);
      config.setUsername(username);
      config.setPassword(password);
      config.setMinimumIdle(minIdle);
      config.setMaximumPoolSize(maxPoolSize);
      config.setAutoCommit(false);
//      config.setTransactionIsolation("TRANSACTION_SERIALIZABLE");
//      config.setTransactionIsolation("TRANSACTION_REPEATABLE_READ");
      config.setTransactionIsolation("TRANSACTION_READ_COMMITTED");
      config.setReadOnly(true);

      /*
       * This property controls the maximum amount of time that a connection is allowed to sit idle in the pool.
       * Whether a connection is retired as idle or not is subject to a maximum variation of +30 seconds, and average
       * variation of +15 seconds. A connection will never be retired as idle before this timeout. A value of 0 means
       * that idle connections are never removed from the pool. Default: 600000 (10 minutes)
       */
      config.setIdleTimeout(30000); // 0.5 minute
      config.setMaxLifetime(5 * 60000); // 5 minutes (excludes in-use connections that have not been returned to the pool)
      // The amount of time that we are willing to wait for a connection
      config.setConnectionTimeout(30000); // 30 seconds (default)

      config.addDataSourceProperty("cachePrepStmts", "true");
      config.addDataSourceProperty("prepStmtCacheSize", "250");
      config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");

      readOnly.add(new HikariDataSource(config));
    }
  }

  @Override
  public synchronized Connection getReadOnlyConnection() throws SQLException {
//    Connection conn;
//    if (!readOnly.isEmpty()) {
//      conn = readOnly.get(random.nextInt(readOnly.size())).getConnection();
//    } else {
//      conn = writable.get(random.nextInt(writable.size())).getConnection();
//    }
    Connection conn = readOnly.get(random.nextInt(readOnly.size())).getConnection();
//    conn.setReadOnly(true);
//    conn.setAutoCommit(false);
    //conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
    conn.rollback();  // Needed in case this is a 'recycled' connection; need to update transaction's view
    return conn;
  }

  @Override
  public synchronized Connection getWritableConnection() throws SQLException {
    Connection conn = writable.get(random.nextInt(writable.size())).getConnection();
//    conn.setReadOnly(false);
//    conn.setAutoCommit(false);
    //conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
    conn.rollback();
    return conn;  // Needed in case this is a 'recycled' connection; need to update transaction's view
  }

  @Override
  public void close() {
    for (HikariDataSource ds : readOnly) {
      ds.close();
    }
    for (HikariDataSource ds : writable) {
      ds.close();
    }
  }

  public static void performCommit(Connection conn, CommitFunction function) throws DatabaseException {
    boolean serializationRetry = true;
    while (serializationRetry) {
      try {
        conn.rollback();
        function.call();
        conn.commit();
        serializationRetry = false;
      } catch (SQLException e) {
        logger.warning("SQL Exception occurred with SQLState " + e.getSQLState() + " and Error Code: "+e.getErrorCode());
        int errCode = Integer.parseInt(e.getSQLState());
        if (errCode != SQL_ERR_SERIALIZATION_FAILURE) {
          throw new DatabaseException("Failed to perform database query", e);
        }
        logger.warning("Error code was: " + SQL_ERR_SERIALIZATION_FAILURE+ ", indicating a serialization failure. " +
            "The statements / transaction can probably be retried as-is (going to do that next).");
      } catch (Exception e) {
        throw new DatabaseException("Failed to perform database query", e);
      }
    }
  }

  public interface CommitFunction {
    void call() throws Exception;
  }

}
