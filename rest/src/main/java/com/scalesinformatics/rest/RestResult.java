/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.rest;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Keith Flanagan
 */
public abstract class RestResult implements Serializable {
  private static final String OK_MSG = "Ok";
  private boolean success;
  private Date started;
  private long processingTimeMs;
  private String message;

  public RestResult() {
    this.started = new Date();
  }

  public void markAsSucceeded() {
    success = true;
    processingTimeMs = System.currentTimeMillis() - started.getTime();
    this.message = OK_MSG;
  }

  public void markAsFailed(String message) {
    success = false;
    processingTimeMs = System.currentTimeMillis() - started.getTime();
    this.message = message;
  }

  @Override
  public String toString() {
    return "RestResult{" +
        "success=" + success +
        ", started=" + started +
        ", processingTimeMs=" + processingTimeMs +
        ", message='" + message + '\'' +
        '}';
  }

  public boolean isSuccess() {
    return success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }

  public Date getStarted() {
    return started;
  }

  public void setStarted(Date started) {
    this.started = started;
  }

  public long getProcessingTimeMs() {
    return processingTimeMs;
  }

  public void setProcessingTimeMs(long processingTimeMs) {
    this.processingTimeMs = processingTimeMs;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
