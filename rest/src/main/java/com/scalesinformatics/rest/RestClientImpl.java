/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scalesinformatics.util.BeanIO;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

/**
 * @author Keith Flanagan
 */
public class RestClientImpl implements RestClient {
  private static final Logger logger = Logger.getLogger(RestClientImpl.class.getName());
  //private static final ObjectMapper mapper = new ObjectMapper();
  private static final BeanIO beanIO = new BeanIO();

  private String authentication;

  private enum Method {
    GET,
    POST,
    PUT,
    DELETE,
    OPTIONS;
  }


  private class ResponseWrapper {
    int httpStatus;
    byte[] content;

    public ResponseWrapper(int httpStatus, byte[] content) {
      this.httpStatus = httpStatus;
      this.content = content;
    }
  }

  @Override
  public <T> RestResponse<T> get(URL url, TypeReference<T> returnType) throws RestException {
    //return parseJsonResponse(rawGet(url, "application/json"), returnType);
    return parseJsonResponse(rawSendReq(url, Method.GET, "application/json"), returnType);
  }

  @Override
  public <T> RestResponse<T> get(String url, TypeReference<T> returnType) throws RestException {
    return get(makeUrl(url), returnType);
  }

//  @Override
//  public <T> RestResponse<T> get(String url, Object contentBody, TypeReference<T> returnType) throws RestException {
//    try {
//    return parseJsonResponse(rawSendReq(makeUrl(url), Method.GET,
//        "application/json", mapper.writeValueAsBytes(contentBody),
//        "application/json"), returnType);
//    } catch (JsonProcessingException e) {
//      throw new RestException("Failed to serialize JSON request content", e);
//    }
//  }

  @Override
  public <T> RestResponse<T> post(String url, TypeReference<T> returnType) throws RestException {
    return parseJsonResponse(rawSendReq(makeUrl(url), Method.POST, "application/json"), returnType);
  }

  @Override
  public <T> RestResponse<T> post(URL url, Object contentBody, TypeReference<T> returnType) throws RestException {
    try {
//      ResponseWrapper response = rawSendReq(url, Method.POST, "application/json",
//          mapper.writeValueAsBytes(contentBody),  "application/json");
      ResponseWrapper response = rawSendReq(url, Method.POST, "application/json",
          beanIO.save(contentBody),  "application/json");

      return parseJsonResponse(response, returnType);
    } catch (Exception e) {
      throw new RestException("Operation failed", e);
    }
  }

  @Override
  public <T> RestResponse<T> post(String url, Object contentBody, TypeReference<T> returnType) throws RestException {
    return post(makeUrl(url), contentBody, returnType);
  }

  @Override
  public <T> RestResponse<T> put(String url, TypeReference<T> returnType) throws RestException {
    return parseJsonResponse(rawSendReq(makeUrl(url), Method.PUT, "application/json"), returnType);
  }

  @Override
  public <T> RestResponse<T> put(URL url, Object contentBody, TypeReference<T> returnType) throws RestException {
    try {
//      return parseJsonResponse(rawSendReq(url, Method.PUT, "application/json",
//          mapper.writeValueAsBytes(contentBody), "application/json"), returnType);
      return parseJsonResponse(rawSendReq(url, Method.PUT, "application/json",
          beanIO.save(contentBody), "application/json"), returnType);
    } catch (Exception e) {
      throw new RestException("Operation failed", e);
    }
  }

  @Override
  public <T> RestResponse<T> put(String url, Object contentBody, TypeReference<T> returnType) throws RestException {
    return put(makeUrl(url), contentBody, returnType);
  }

  @Override
  public <T> RestResponse<T> delete(URL url, Object contentBody, TypeReference<T> returnType) throws RestException {
    try {
//      return parseJsonResponse(rawSendReq(url, Method.DELETE, "application/json",
//          mapper.writeValueAsBytes(contentBody),  "application/json"), returnType);
      return parseJsonResponse(rawSendReq(url, Method.DELETE, "application/json",
          beanIO.save(contentBody),  "application/json"), returnType);
    } catch (Exception e) {
      throw new RestException("Operation failed", e);
    }
  }

  @Override
  public <T> RestResponse<T> delete(String url, Object contentBody, TypeReference<T> returnType) throws RestException {
    return delete(makeUrl(url), contentBody, returnType);
  }

  private URL makeUrl(String url) throws RestException {
    try {
      return new URL(url);
    } catch (MalformedURLException e) {
      throw new RestException("Error creating URL from: "+url, e);
    }
  }

  private <T> RestResponse<T> parseJsonResponse(ResponseWrapper rawResponse, TypeReference<T> returnType)
          throws RestException {
    try {
      //T parsed = mapper.readValue(rawResponse.content, returnType);
      T parsed = beanIO.load(rawResponse.content, returnType);
      return new RestResponse<>(rawResponse.httpStatus, rawResponse.content, parsed);
    } catch (Exception e) {
      throw new RestException("Operation failed", e);
    }
  }


//  private ResponseWrapper rawGet(URL url, String acceptType) throws RestException {
//    HttpURLConnection conn = null;
//    try {
//      conn = (HttpURLConnection) url.openConnection();
//      conn.setRequestMethod("GET");
//      conn.setRequestProperty("Accept", acceptType);
//
//      int responseCode = conn.getResponseCode();
//      return new ResponseWrapper(responseCode, downloadFromServer(conn));
//    } catch (Exception e) {
//      if (conn != null) {
//        conn.disconnect();
//      }
//      throw new RestException("Connection failed", e);
//    }
//  }

  private ResponseWrapper rawSendReq(URL url, Method method, String acceptType)
      throws RestException {
    return rawSendReq(url, method, null, null, acceptType);
  }

  private ResponseWrapper rawSendReq(URL url, Method method, String contentType, byte[] content, String acceptType)
      throws RestException {
    HttpURLConnection conn = null;
    try {
      conn = (HttpURLConnection) url.openConnection();
      conn.setDoOutput(true);
      conn.setRequestMethod(method.name());
      conn.setRequestProperty("Accept", acceptType);
      if (authentication != null) {
        conn.setRequestProperty("Authorization", authentication);
      }
      //conn.setRequestProperty("X-FooToken", Integer.toString(content.length));

      if (contentType != null && content != null) {
        conn.setRequestProperty("Content-Type", contentType);
        conn.setRequestProperty("Content-Length", Integer.toString(content.length));
        uploadToServer(conn, content);
      }


      int responseCode = conn.getResponseCode();
      return new ResponseWrapper(responseCode, downloadFromServer(conn));
    } catch (Exception e) {
      if (conn != null) {
        conn.disconnect();
      }
      throw new RestException("Connection failed", e);
    }
  }

  private byte[] downloadFromServer(HttpURLConnection conn) throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream(4096);
    byte[] buffer = new byte[4096];
    try (InputStream is = conn.getInputStream()) {
      for (int read = is.read(buffer); read != -1; read = is.read(buffer)) {
        baos.write(buffer, 0, read);
      }

    }
    return baos.toByteArray();
  }

  private void uploadToServer(HttpURLConnection conn, byte[] content) throws IOException {
    try (OutputStream os = conn.getOutputStream()) {
      os.write(content);
      os.flush();
    }
  }

  @Override
  public String getAuthentication() {
    return authentication;
  }

  @Override
  public void setAuthentication(String authentication) {
    this.authentication = authentication;
  }
}
