/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.rest;

import com.fasterxml.jackson.core.type.TypeReference;

/**
 * Implements a set of commonly-required functionality at the layer up from that provided by
 * <code>RestClient</code>.
 *
 * A 'rich' REST client tends to expose domain-specific set of queries or operations, which are then mapped
 * onto multiple service endpoints. Each service endpoint provides the results for a given query.
 *
 * This class contains much of the boilerplate stuff that's required to interact with <code>RestClient</code>,
 * thereby enabling concise domain-specific clients to be written.
 *
 * @author Keith Flanagan
 */
abstract public class AbstractRichRestClient {
  private final RestClient restClient;

  public AbstractRichRestClient(RestClient restClient) {
    this.restClient = restClient;
  }

  /*
   * GET
   */

  protected <T> SingletonResult<T> callGetSingleton(
      String url, TypeReference<SingletonResult<T>> typeRef) throws RestException {
    try {
      RestClient.RestResponse<SingletonResult<T>> result = restClient.get(url, typeRef);
      return result.getContent();
    } catch (Exception e) {
      throw new RestException("Request failed", e);
    }
  }

  protected <T> ListResult<T> callGetList(
      String url, TypeReference<ListResult<T>> typeRef) throws RestException {
    try {
      RestClient.RestResponse<ListResult<T>> result = restClient.get(url, typeRef);
      return result.getContent();
    } catch (Exception e) {
      throw new RestException("Request failed", e);
    }
  }

  protected <T> SetResult<T> callGetSet(String url, TypeReference<SetResult<T>> typeRef) throws RestException {
    try {
      RestClient.RestResponse<SetResult<T>> result = restClient.get(url, typeRef);
      return result.getContent();
    } catch (Exception e) {
      throw new RestException("Request failed", e);
    }
  }

  protected <K, V> MapResult<K, V> callGetMap(String url, TypeReference<MapResult<K, V>> typeRef) throws RestException {
    try {
      RestClient.RestResponse<MapResult<K, V>> result = restClient.get(url, typeRef);
      return result.getContent();
    } catch (Exception e) {
      throw new RestException("Request failed", e);
    }
  }


  /*
   * PUT
   */
  protected <T> SingletonResult<T> callPutSingleton(
      String url, Object contentBody,  TypeReference<SingletonResult<T>> typeRef) throws RestException {
    try {
      RestClient.RestResponse<SingletonResult<T>> result = restClient.put(url, contentBody, typeRef);
      return result.getContent();
    } catch (Exception e) {
      throw new RestException("Request failed", e);
    }
  }

  protected <T> ListResult<T> callPutList(
      String url, Object contentBody, TypeReference<ListResult<T>> typeRef) throws RestException {
    try {
      RestClient.RestResponse<ListResult<T>> result = restClient.put(url, contentBody, typeRef);
      return result.getContent();
    } catch (Exception e) {
      throw new RestException("Request failed", e);
    }
  }

  protected <T> SetResult<T> callPutSet(String url, Object contentBody, TypeReference<SetResult<T>> typeRef) throws RestException {
    try {
      RestClient.RestResponse<SetResult<T>> result = restClient.put(url, contentBody, typeRef);
      return result.getContent();
    } catch (Exception e) {
      throw new RestException("Request failed", e);
    }
  }

  protected <K, V> MapResult<K, V> callPutMap(String url, Object contentBody, TypeReference<MapResult<K, V>> typeRef) throws RestException {
    try {
      RestClient.RestResponse<MapResult<K, V>> result = restClient.put(url, contentBody, typeRef);
      return result.getContent();
    } catch (Exception e) {
      throw new RestException("Request failed", e);
    }
  }



  /*
   * POST
   */
  protected <T> SingletonResult<T> callPostSingleton(
      String url, Object contentBody,  TypeReference<SingletonResult<T>> typeRef) throws RestException {
    try {
      RestClient.RestResponse<SingletonResult<T>> result = restClient.post(url, contentBody, typeRef);
      return result.getContent();
    } catch (Exception e) {
      throw new RestException("Request failed", e);
    }
  }

  protected <T> ListResult<T> callPostList(
      String url, Object contentBody, TypeReference<ListResult<T>> typeRef) throws RestException {
    try {
      RestClient.RestResponse<ListResult<T>> result = restClient.post(url, contentBody, typeRef);
      return result.getContent();
    } catch (Exception e) {
      throw new RestException("Request failed", e);
    }
  }

  protected <T> SetResult<T> callPostSet(String url, Object contentBody, TypeReference<SetResult<T>> typeRef) throws RestException {
    try {
      RestClient.RestResponse<SetResult<T>> result = restClient.post(url, contentBody, typeRef);
      return result.getContent();
    } catch (Exception e) {
      throw new RestException("Request failed", e);
    }
  }

  protected <K, V> MapResult<K, V> callPostMap(String url, Object contentBody, TypeReference<MapResult<K, V>> typeRef) throws RestException {
    try {
      RestClient.RestResponse<MapResult<K, V>> result = restClient.post(url, contentBody, typeRef);
      return result.getContent();
    } catch (Exception e) {
      throw new RestException("Request failed", e);
    }
  }


}
