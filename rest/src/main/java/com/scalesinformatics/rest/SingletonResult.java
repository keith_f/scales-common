/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.rest;

/**
 * @author Keith Flanagan
 */
public class SingletonResult<T> extends RestResult {
  T result;

  public SingletonResult() {
  }

  public SingletonResult(T result) {
    this.result = result;
  }

  @Override
  public String toString() {
    return "SingletonResult{" +
        "result=" + result +
        "} " + super.toString();
  }

  public T getResult() {
    return result;
  }

  public void setResult(T result) {
    this.result = result;
  }
}
