/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.scalesinformatics.rest;

import com.fasterxml.jackson.core.type.TypeReference;

import java.net.URL;

/**
 * A simple REST client that provides a means of automatically serializing and deserializing body content
 * (e.g. to/from JSON).
 *
 * TODO additional methods to support 'raw' gets/posts/puts/deletes (i.e. the raw, not deserialized data is returned)
 *
 * @author Keith Flanagan
 */
public interface RestClient {
  /**
   * Encapsulates the response of a REST call from a remote server.
   * Typically, 'conent' will be an implementation of RestResult.
   *
   * @param <T>
   */
  class RestResponse<T> {
    private final int httpStatus;
    private final byte[] rawContent;  // Raw message body
    private final T content;          // Deserialized object from rawContent

    public RestResponse(int httpStatus, byte[] rawContent, T content) {
      this.httpStatus = httpStatus;
      this.rawContent = rawContent;
      this.content = content;
    }

    public int getHttpStatus() {
      return httpStatus;
    }

    public byte[] getRawContent() {
      return rawContent;
    }

    public T getContent() {
      return content;
    }
  }

  /*
   * Get something ...
   */

  <T> RestResponse<T> get(URL url, TypeReference<T> returnType) throws RestException;
  <T> RestResponse<T> get(String url, TypeReference<T> returnType) throws RestException;
  //<T> RestResponse<T> get(String url, Object contentBody, TypeReference<T> returnType) throws RestException;


  /*
   * POST creates something ...
   */

  <T> RestResponse<T> post(String url, TypeReference<T> returnType) throws RestException;
  <T> RestResponse<T> post(URL url, Object contentBody, TypeReference<T> returnType) throws RestException;
  <T> RestResponse<T> post(String url, Object contentBody, TypeReference<T> returnType) throws RestException;


  /*
   * PUT updates something ...
   */
  <T> RestResponse<T> put(String url, TypeReference<T> returnType) throws RestException;
  <T> RestResponse<T> put(URL url, Object contentBody, TypeReference<T> returnType) throws RestException;
  <T> RestResponse<T> put(String url, Object contentBody, TypeReference<T> returnType) throws RestException;


  /*
   * DELETE destroys something ...
   */
  <T> RestResponse<T> delete(URL url, Object contentBody, TypeReference<T> returnType) throws RestException;
  <T> RestResponse<T> delete(String url, Object contentBody, TypeReference<T> returnType) throws RestException;


  /*
   * OPTIONS ... does something ...
   */






  String getAuthentication();
  void setAuthentication(String authentication);
}
